-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 11, 2017 at 11:00 PM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blueyed_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `users_role`
--

CREATE TABLE IF NOT EXISTS `users_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `users_role`
--

INSERT INTO `users_role` (`id`, `user_group_id`, `name`, `des`) VALUES
(1, 1, 'users_list', 'View Users List'),
(2, 1, 'edit_user', 'Edit User'),
(3, 1, 'view_user', 'View User'),
(4, 1, 'delete_user', 'Delete User'),
(5, 4, 'user_group_panel', 'User Group Panel View'),
(6, 2, 'candidate_list', 'Candidate List'),
(7, 2, 'view_candidate', 'View Candidate'),
(8, 2, 'edit_candidate', 'Edit Candidate'),
(9, 2, 'delete_candidate', 'Delete Candidate'),
(10, 3, 'recruiter_list', 'Recruiter List'),
(11, 3, 'view_recruiter', 'View Recruiter'),
(12, 3, 'edit_recruiter', 'Edit Recruiter'),
(13, 3, 'delete_recruiter', 'Delete Recruiter'),
(14, 1, 'create_user', 'Create User'),
(15, 2, 'create_candidate', 'Create Candidate'),
(16, 3, 'create_recruiter', 'Create Recruiter'),
(17, 5, 'display', 'Display of the Blog Management at Right Panel'),
(18, 5, 'add', 'Add new Blog'),
(19, 5, 'edit', 'Edit existing Blogs'),
(20, 5, 'delete', 'Delete existing Blog'),
(21, 6, 'display', 'Display of the Content Management at Right Panel'),
(22, 6, 'add', 'Add new Content'),
(23, 6, 'edit', 'Edit existing Content'),
(24, 6, 'delete', 'Delete existing Content'),
(25, 7, 'display', 'Display of the Template Management at Right Panel'),
(26, 7, 'add', 'Add new Template'),
(27, 7, 'edit', 'Edit existing Template'),
(28, 7, 'delete', 'Delete existing Template'),
(29, 8, 'display', 'Display of Role Management in the Right panel'),
(30, 8, 'add', 'Add new Roles'),
(31, 8, 'edit', 'Edit Existing Roles'),
(32, 8, 'delete', 'Delete Existing Roles'),
(33, 9, 'display', 'Display Assign Role at right Panel'),
(34, 9, 'add', 'Assign Roles'),
(35, 9, 'edit', 'Edit assigned Roles'),
(36, 9, 'delete', 'Delete assigned roles'),
(37, 10, 'view', 'Display Settings'),
(38, 11, 'add', 'Add new degrees '),
(39, 11, 'edit', 'Edit existing degrees'),
(40, 11, 'delete', 'Delete existing degrees'),
(41, 12, 'add', 'Add new questions'),
(42, 12, 'edit', 'Edit existing questions'),
(43, 12, 'delete', 'Delete existing questions'),
(44, 11, 'view', 'Display the Degree panel'),
(45, 12, 'view', 'Display the question panel'),
(46, 1, 'create', 'User Create'),
(47, 13, 'view', 'Display the College panel'),
(48, 13, 'add', 'Add new College'),
(49, 13, 'edit', 'Edit existing College'),
(50, 13, 'delete', 'Delete existing College'),
(51, 14, 'view', 'View Company List'),
(52, 14, 'add', 'Create Company Name'),
(53, 14, 'edit', 'Edit Company Name'),
(54, 14, 'delete', 'Delete Company Name'),
(55, 15, 'view', 'View Work Title List'),
(56, 15, 'add', 'Create Work Title'),
(57, 15, 'edit', 'Edit Work Title'),
(58, 15, 'delete', 'Delete Work Title'),
(59, 16, 'view', 'View UG Degree List'),
(60, 16, 'create', 'Create UG Degree'),
(61, 16, 'edit', 'Edit UG Degree'),
(62, 16, 'delete', 'Delete UG Degree'),
(63, 17, 'view', 'View UG college List'),
(64, 17, 'create', 'Create UG College'),
(65, 17, 'edit', 'Edit UG Collge'),
(66, 17, 'delete', 'Delete UG college'),
(67, 18, 'view', 'View HS School List'),
(68, 18, 'create', 'Create HS School'),
(69, 18, 'edit', 'Edit HS School'),
(70, 18, 'delete', 'Delete HS School'),
(71, 19, 'view', 'View Skills'),
(72, 19, 'create', 'Create Skills'),
(73, 19, 'edit', 'Edit Skills'),
(74, 19, 'delete', 'Delete Skills'),
(75, 20, 'view', 'View Graduation Major'),
(76, 20, 'create', 'Create Graduation Major'),
(77, 20, 'edit', 'Edit Graduation Major'),
(78, 20, 'delete', 'Delete Graduation Major'),
(79, 21, 'view', 'View Under Graduation Major'),
(80, 21, 'create', 'Create Under Graduation Major'),
(81, 21, 'edit', 'Edit Under Graduation Major'),
(82, 21, 'delete', 'Delete Under Graduation Major'),
(83, 22, 'view', 'View CTC Band'),
(84, 22, 'create ', 'Create CTC Band'),
(85, 22, 'edit', 'Edit CTC Band'),
(86, 22, 'delete ', 'Delete CTC Band'),
(87, 23, 'view', 'View Hs Stream'),
(88, 23, 'create', 'Create Hs stream'),
(89, 23, 'edit', 'Edit Hs Stream'),
(90, 23, 'delete', 'Delete Hs stream');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
