-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 29, 2017 at 10:11 PM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `microfinance_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demo_accounts`
--

CREATE TABLE IF NOT EXISTS `demo_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `loan_number` varchar(255) NOT NULL,
  `loan_amount` decimal(10,2) NOT NULL,
  `no_of_days` int(11) NOT NULL,
  `interest_rate` decimal(10,2) NOT NULL,
  `cheque_received_status` enum('Yes','No') NOT NULL DEFAULT 'No',
  `account_open_date` date NOT NULL,
  `account_close_date` date NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `demo_accounts`
--

INSERT INTO `demo_accounts` (`id`, `client_id`, `account_name`, `designation`, `loan_number`, `loan_amount`, `no_of_days`, `interest_rate`, `cheque_received_status`, `account_open_date`, `account_close_date`, `status`) VALUES
(16, 158, 'VIKRAM ENTERPRICE', 'Proprietor', '12345678', '75000.00', 200, '10.00', 'Yes', '2014-05-30', '2017-12-30', 'Yes'),
(17, 161, 'Digital Aptech Pvt Ltd', 'Director', '123456', '20000.00', 30, '5.00', 'No', '0000-00-00', '0000-00-00', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_admin`
--

CREATE TABLE IF NOT EXISTS `demo_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_type` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `demo_admin`
--

INSERT INTO `demo_admin` (`admin_id`, `admin_username`, `admin_password`, `admin_type`) VALUES
(1, 'master', 'd722dbcb93d6ca952b49928b37cac8e1', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `demo_area`
--

CREATE TABLE IF NOT EXISTS `demo_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(255) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `demo_area`
--

INSERT INTO `demo_area` (`id`, `area_name`, `status`) VALUES
(2, 'saltlake, sector 5', 'Yes'),
(3, 'Bidhanagar', 'Yes'),
(5, 'Taligange', 'Yes'),
(8, 'Behala Chowrasta', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_area_collection`
--

CREATE TABLE IF NOT EXISTS `demo_area_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_man_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `demo_area_collection`
--

INSERT INTO `demo_area_collection` (`id`, `collection_man_id`, `area_id`, `added_date`) VALUES
(3, 156, 2, '2017-10-25 03:48:29'),
(4, 170, 3, '2017-10-26 18:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `demo_banners`
--

CREATE TABLE IF NOT EXISTS `demo_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_title` varchar(255) NOT NULL,
  `banner_image` text NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `demo_banners`
--

INSERT INTO `demo_banners` (`id`, `banner_title`, `banner_image`, `status`) VALUES
(1, 'banner title 1', 'banners/img_1474302427.jpg', 'Yes'),
(2, 'test', 'banners/img_1507784294.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `demo_cheque_details`
--

CREATE TABLE IF NOT EXISTS `demo_cheque_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `cheque_amount` decimal(10,2) NOT NULL,
  `cheque_no` varchar(255) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `demo_cheque_details`
--

INSERT INTO `demo_cheque_details` (`id`, `client_id`, `account_id`, `cheque_amount`, `cheque_no`, `status`) VALUES
(9, 158, 16, '10000.00', '123456', 'Yes'),
(10, 158, 16, '20000.00', '23768', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_client_document`
--

CREATE TABLE IF NOT EXISTS `demo_client_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `purpose` varchar(255) NOT NULL,
  `document_value` decimal(10,2) NOT NULL,
  `doc_received_status` enum('Yes','No') NOT NULL,
  `received_by` varchar(255) NOT NULL,
  `received_date` date NOT NULL,
  `validity_status` enum('Yes','No') NOT NULL COMMENT 'Yes=Valid,No=Expired',
  `expiry_date` date NOT NULL,
  `return_status` enum('Yes','No') NOT NULL,
  `return_date` date NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `demo_client_document`
--

INSERT INTO `demo_client_document` (`id`, `client_id`, `account_id`, `purpose`, `document_value`, `doc_received_status`, `received_by`, `received_date`, `validity_status`, `expiry_date`, `return_status`, `return_date`, `status`) VALUES
(29, 161, 17, 'Short term', '0.00', 'Yes', 'Sukanta Mondal', '0000-00-00', 'No', '2023-05-08', 'No', '0000-00-00', 'Yes'),
(38, 158, 16, 'Short term', '75000.00', 'Yes', 'Samir Kumar', '0000-00-00', 'Yes', '2010-07-15', 'No', '0000-00-00', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_client_document_details`
--

CREATE TABLE IF NOT EXISTS `demo_client_document_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `client_document_id` int(11) NOT NULL,
  `document_type` varchar(255) NOT NULL,
  `document_path` text NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `demo_client_document_details`
--

INSERT INTO `demo_client_document_details` (`id`, `client_id`, `account_id`, `client_document_id`, `document_type`, `document_path`, `status`) VALUES
(85, 158, 16, 38, 'Hundi', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_cms`
--

CREATE TABLE IF NOT EXISTS `demo_cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_pagetitle` varchar(255) NOT NULL,
  `cms_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `demo_cms`
--

INSERT INTO `demo_cms` (`id`, `cms_pagetitle`, `cms_description`) VALUES
(1, 'HOME', ''),
(2, 'ABOUT US', '');

-- --------------------------------------------------------

--
-- Table structure for table `demo_draw_details`
--

CREATE TABLE IF NOT EXISTS `demo_draw_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `drawee_name` varchar(255) NOT NULL,
  `drawee_designation` varchar(255) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `demo_draw_details`
--

INSERT INTO `demo_draw_details` (`id`, `account_id`, `client_id`, `drawee_name`, `drawee_designation`, `status`) VALUES
(22, 17, 161, 'Surojit Roy', 'Proprietor', 'Yes'),
(31, 16, 158, 'Viram Mehra', 'Proprietor', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_groups`
--

CREATE TABLE IF NOT EXISTS `demo_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `bgcolor` char(7) NOT NULL DEFAULT '#607D8B',
  `flag` enum('0','1','2','3','5') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `demo_groups`
--

INSERT INTO `demo_groups` (`id`, `name`, `description`, `bgcolor`, `flag`) VALUES
(1, 'super_admin', 'Super Administrator', '#F44336', '0'),
(2, 'admin', 'Administrator', '#2196F3', '1'),
(3, 'agent', 'Blueyed Employee', '#607D8B', '2'),
(4, 'client', 'Candidate', '#e91e63', '3'),
(5, 'collection man', 'Recruiter', '#795548', '5');

-- --------------------------------------------------------

--
-- Table structure for table `demo_guarantor`
--

CREATE TABLE IF NOT EXISTS `demo_guarantor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `guarantor_name` varchar(255) NOT NULL,
  `guarantor_designation` varchar(255) NOT NULL,
  `blood_relation` varchar(255) NOT NULL,
  `guarantor_address` varchar(255) NOT NULL,
  `tel1` varchar(255) NOT NULL,
  `tel2` varchar(255) NOT NULL,
  `draw_id` int(11) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `demo_guarantor`
--

INSERT INTO `demo_guarantor` (`id`, `account_id`, `client_id`, `guarantor_name`, `guarantor_designation`, `blood_relation`, `guarantor_address`, `tel1`, `tel2`, `draw_id`, `status`) VALUES
(18, 161, 161, 'Partner 1', 'Proprietor', 'Father', 'DumDum', '0933987898', '0339876876', 0, 'Yes'),
(19, 161, 161, 'partner 2', 'Proprietor', 'Uncle', '', '', '', 0, 'Yes'),
(20, 161, 161, 'Sachine Kumar', 'Proprietor', 'Father', 'Bikash Bwaban,kolkata', '', '', 0, 'Yes'),
(21, 161, 161, 'test', 'Owner', '', '', '', '', 0, 'Yes'),
(23, 161, 161, 'Surojit Roy', 'Proprietor', '', '', '', '', 0, 'Yes'),
(34, 17, 161, 'Surojit Roy', 'Proprietor', 'Father', '', '', '', 0, 'Yes'),
(45, 16, 158, 'Parveen Mehra', 'Owner', 'Father', '', '', '', 0, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_roles`
--

CREATE TABLE IF NOT EXISTS `demo_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) NOT NULL,
  `role_des` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `roleID` varchar(255) NOT NULL,
  `usersID` int(11) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `demo_roles`
--

INSERT INTO `demo_roles` (`id`, `role_name`, `role_des`, `created_date`, `roleID`, `usersID`, `status`) VALUES
(1, 'Subscriber', 'subscriber', '2017-03-09 22:31:24', '6,7,38,39,40,44,41,42,43,45,47,48,49,50', 1, 'Yes'),
(2, 'Starter', '', '2017-03-07 00:00:44', '33,34,35,36,37,39,40,44,42,43,45', 1, 'Yes'),
(3, 'skills', 'view skills', '2017-03-27 23:21:57', '37,71,72,73,74', 1, 'Yes'),
(5, 'test role', 'description', '2017-10-13 01:00:36', '2,3,14,7,26,27', 1, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_role_features`
--

CREATE TABLE IF NOT EXISTS `demo_role_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_group_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `demo_role_features`
--

INSERT INTO `demo_role_features` (`id`, `role_group_id`, `name`, `des`) VALUES
(1, 1, 'users_list', 'View Users List'),
(2, 1, 'edit_user', 'Edit User'),
(3, 1, 'view_user', 'View User'),
(4, 1, 'delete_user', 'Delete User'),
(5, 4, 'user_group_panel', 'User Group Panel View'),
(6, 2, 'candidate_list', 'Candidate List'),
(7, 2, 'view_candidate', 'View Candidate'),
(8, 2, 'edit_candidate', 'Edit Candidate'),
(9, 2, 'delete_candidate', 'Delete Candidate'),
(10, 3, 'recruiter_list', 'Recruiter List'),
(11, 3, 'view_recruiter', 'View Recruiter'),
(12, 3, 'edit_recruiter', 'Edit Recruiter'),
(13, 3, 'delete_recruiter', 'Delete Recruiter'),
(14, 1, 'create_user', 'Create User'),
(15, 2, 'create_candidate', 'Create Candidate'),
(16, 3, 'create_recruiter', 'Create Recruiter'),
(17, 5, 'display', 'Display of the Blog Management at Right Panel'),
(18, 5, 'add', 'Add new Blog'),
(19, 5, 'edit', 'Edit existing Blogs'),
(20, 5, 'delete', 'Delete existing Blog'),
(21, 6, 'display', 'Display of the Content Management at Right Panel'),
(22, 6, 'add', 'Add new Content'),
(23, 6, 'edit', 'Edit existing Content'),
(24, 6, 'delete', 'Delete existing Content'),
(25, 7, 'display', 'Display of the Template Management at Right Panel'),
(26, 7, 'add', 'Add new Template'),
(27, 7, 'edit', 'Edit existing Template'),
(28, 7, 'delete', 'Delete existing Template'),
(29, 8, 'display', 'Display of Role Management in the Right panel'),
(30, 8, 'add', 'Add new Roles'),
(31, 8, 'edit', 'Edit Existing Roles'),
(32, 8, 'delete', 'Delete Existing Roles'),
(33, 9, 'display', 'Display Assign Role at right Panel'),
(34, 9, 'add', 'Assign Roles'),
(35, 9, 'edit', 'Edit assigned Roles'),
(36, 9, 'delete', 'Delete assigned roles'),
(37, 10, 'view', 'Display Settings'),
(38, 11, 'add', 'Add new degrees '),
(39, 11, 'edit', 'Edit existing degrees'),
(40, 11, 'delete', 'Delete existing degrees'),
(41, 12, 'add', 'Add new questions'),
(42, 12, 'edit', 'Edit existing questions'),
(43, 12, 'delete', 'Delete existing questions'),
(44, 11, 'view', 'Display the Degree panel'),
(45, 12, 'view', 'Display the question panel'),
(46, 1, 'create', 'User Create'),
(47, 13, 'view', 'Display the College panel'),
(48, 13, 'add', 'Add new College'),
(49, 13, 'edit', 'Edit existing College'),
(50, 13, 'delete', 'Delete existing College'),
(51, 14, 'view', 'View Company List'),
(52, 14, 'add', 'Create Company Name'),
(53, 14, 'edit', 'Edit Company Name'),
(54, 14, 'delete', 'Delete Company Name'),
(55, 15, 'view', 'View Work Title List'),
(56, 15, 'add', 'Create Work Title'),
(57, 15, 'edit', 'Edit Work Title'),
(58, 15, 'delete', 'Delete Work Title'),
(59, 16, 'view', 'View UG Degree List'),
(60, 16, 'create', 'Create UG Degree'),
(61, 16, 'edit', 'Edit UG Degree'),
(62, 16, 'delete', 'Delete UG Degree'),
(63, 17, 'view', 'View UG college List'),
(64, 17, 'create', 'Create UG College'),
(65, 17, 'edit', 'Edit UG Collge'),
(66, 17, 'delete', 'Delete UG college'),
(67, 18, 'view', 'View HS School List'),
(68, 18, 'create', 'Create HS School'),
(69, 18, 'edit', 'Edit HS School'),
(70, 18, 'delete', 'Delete HS School'),
(71, 19, 'view', 'View Skills'),
(72, 19, 'create', 'Create Skills'),
(73, 19, 'edit', 'Edit Skills'),
(74, 19, 'delete', 'Delete Skills'),
(75, 20, 'view', 'View Graduation Major'),
(76, 20, 'create', 'Create Graduation Major'),
(77, 20, 'edit', 'Edit Graduation Major'),
(78, 20, 'delete', 'Delete Graduation Major'),
(79, 21, 'view', 'View Under Graduation Major'),
(80, 21, 'create', 'Create Under Graduation Major'),
(81, 21, 'edit', 'Edit Under Graduation Major'),
(82, 21, 'delete', 'Delete Under Graduation Major'),
(83, 22, 'view', 'View CTC Band'),
(84, 22, 'create ', 'Create CTC Band'),
(85, 22, 'edit', 'Edit CTC Band'),
(86, 22, 'delete ', 'Delete CTC Band'),
(87, 23, 'view', 'View Hs Stream'),
(88, 23, 'create', 'Create Hs stream'),
(89, 23, 'edit', 'Edit Hs Stream'),
(90, 23, 'delete', 'Delete Hs stream');

-- --------------------------------------------------------

--
-- Table structure for table `demo_role_group`
--

CREATE TABLE IF NOT EXISTS `demo_role_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(200) NOT NULL,
  `code` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '0 => Not Deleted ; 1 => Deleted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `demo_role_group`
--

INSERT INTO `demo_role_group` (`id`, `group_name`, `code`, `is_deleted`) VALUES
(1, 'User', 'user', 0),
(2, 'Candidate', 'candidate', 0),
(3, 'Recruiter', 'recruiter', 0),
(4, 'Group Panel', 'group', 0),
(5, 'Blog Management', 'blog', 1),
(6, 'Content Management', 'content', 0),
(7, 'Template Management', 'template', 0),
(8, 'Role Management', 'role', 0),
(9, 'Assign Role Management', 'role_assign', 0),
(10, 'Candidate Setting', 'candidate_setting', 0),
(11, 'Degree', 'degree', 0),
(12, 'Question', 'question', 0),
(13, 'College', 'college', 0),
(14, 'Company', 'company', 0),
(15, 'Work Title', 'title', 0),
(16, 'UG Degree', 'ugdegree', 0),
(17, 'UG College', 'ugcollege', 0),
(18, 'School', 'school', 0),
(19, 'Skills', 'skills', 0),
(20, 'Graduation Major', 'graduation major', 0),
(21, 'Under Graduation Major', 'under graduation major', 0),
(22, 'CTC Band', 'ctc band', 0),
(23, 'Hs Stream', 'hs stream', 0);

-- --------------------------------------------------------

--
-- Table structure for table `demo_settings`
--

CREATE TABLE IF NOT EXISTS `demo_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_type` varchar(255) NOT NULL,
  `config_val` varchar(255) NOT NULL,
  `config_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `demo_settings`
--

INSERT INTO `demo_settings` (`id`, `config_type`, `config_val`, `config_title`) VALUES
(1, 'admin_email', 'no-reply@bnc.com', 'Admin Email'),
(2, 'facebook_link', 'https://www.facebook.com', 'facebook Link'),
(3, 'twitter_link', 'http://twitter.com', 'twitter Link'),
(4, 'youtube_link', 'http://youtube.com', 'youtube Link'),
(5, 'googleplus_link', 'http://google.com', 'googleplus Link'),
(6, 'linkedin_link', 'http://linkedin.com', 'linkedin Link'),
(7, 'banner_width', '1024', 'Banner width'),
(8, 'banner_height', '768', 'Banner height');

-- --------------------------------------------------------

--
-- Table structure for table `demo_users`
--

CREATE TABLE IF NOT EXISTS `demo_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `work_address` varchar(255) NOT NULL,
  `residence_address` varchar(255) NOT NULL,
  `work_telephone` varchar(255) NOT NULL,
  `mobile1` varchar(255) NOT NULL,
  `mobile2` varchar(255) NOT NULL,
  `introduced_by` int(11) NOT NULL,
  `collection_man_id` int(11) NOT NULL,
  `broker_ids` varchar(255) NOT NULL,
  `area_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=171 ;

--
-- Dumping data for table `demo_users`
--

INSERT INTO `demo_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `created_on`, `last_login`, `status`, `first_name`, `last_name`, `full_name`, `work_address`, `residence_address`, `work_telephone`, `mobile1`, `mobile2`, `introduced_by`, `collection_man_id`, `broker_ids`, `area_id`, `note`, `company`, `phone`, `group_id`) VALUES
(1, '127.0.0.1', 'admin', '25d55ad283aa400af464c76d713c07ad', '', 'admin@admin.com', 1268889823, 1507869177, 'Yes', 'Super Admin', '', '', '', '', '0', '', '', 0, 0, '0', 0, '', '', '0', 1),
(130, '', 'James', '8569bf26c19801add56c2ba9309d48bc', NULL, 'test@ascimatics.com', 2017, NULL, 'Yes', 'James', 'Thomas', 'James Thomas', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 2),
(131, '', NULL, 'adeb0135b4715e964536c4ac680d75f6', NULL, 'manoj.ascimatics@gmail.com', 2017, NULL, 'Yes', 'Manoj', 'Kumar', 'Manoj Kumar', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 3),
(132, '', 'Shuvojit', 'c12041273402bfa4e910bf34a92bf12c', NULL, 'shuvojit.sarker@ascimatics.com', 2017, NULL, '', 'Shuvojit', 'Sarker', 'Shuvojit Sarker', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 2),
(133, '', 'AbhishekS', '849a6afb19cc6304e29bab4a1ee1c313', NULL, 'abhishek.singh96ms@gmail.com', 2017, NULL, 'Yes', 'Abhishek', 'Kumar', 'Abhishek Kumar', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 5),
(134, '', NULL, '111e017c3fd610a9692589adfbdd4412', NULL, 'satyendra.abhishek83@gmail.com', 2017, NULL, '', 'Abhishek', 'Singh', 'Abhishek Singh', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 5),
(135, '', 'Sandipan', '62a370ad9789b6ed8b94343538b69ebb', NULL, 'sandipan.biswas@ascimatics.com', 2017, NULL, '', 'Sandipan', 'Biswas', 'Sandipan Biswas', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 2),
(136, '', 'Vinay', 'd0084beb1edf5080e212c9e0a14d4db3', NULL, 'vinay.sewal@gmail.com', 2017, NULL, 'Yes', 'Vinay', 'Sewal', 'Vinay Sewal', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 5),
(137, '', 'Sandipan', 'd5dd6b4435b74567116882cf43454cd7', NULL, 'sandipan.biswas@sworks.co.in', 2017, NULL, 'Yes', 'Sandipan', 'Biswas', 'Sandipan Biswas', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 2),
(138, '', 'Sandipan', 'efa078b95df0eb19eaa59b18f45885af', NULL, 'sandipan.biswas30@gmail.com', 2017, NULL, '', 'Sandipan', 'Biswas', 'Sandipan Biswas', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 3),
(139, '', NULL, 'adeb0135b4715e964536c4ac680d75f6', NULL, 'vinay@theblueyed.com', 2017, NULL, '', 'Vinay', 'Sewal', 'Vinay Sewal', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 5),
(140, '', 'Sreejit', 'de8701981cf393dfda59da33e31e4133', NULL, 'choudhurysreejit@gmail.com', 2017, NULL, 'Yes', 'Sreejit', 'Choudhury ', 'Sreejit Choudhury ', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 3),
(141, '', NULL, 'adeb0135b4715e964536c4ac680d75f6', NULL, 'sreejitascimatics@gmail.com', 2017, NULL, 'Yes', 'Sreejit', 'Emp', 'Sreejit Emp', '', '', '0', '', '', 0, 0, '0', 0, '', NULL, NULL, 3),
(158, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Viram Mehra', '10, ABC street,kolkata-20', '5, XYZ  Road,kolkata-16', '8767675678', '8787656789', '9878765645', 133, 0, '138,140', 2, '', NULL, NULL, 4),
(159, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Tushar Kumar', '', '', '9878908978', '9089786789', '9876098789', 0, 0, '', 0, '', NULL, NULL, 3),
(161, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Sreejit Roy', '', '', '8767567898', '9878670987', '9089786756', 158, 0, '159,165,167', 8, '', NULL, NULL, 4),
(162, '', NULL, '', NULL, '', 0, NULL, 'No', NULL, NULL, 'Partha Dutta', '', '', '', '', '', 0, 0, '', 0, '', NULL, NULL, 3),
(165, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Pritam Banerjee', '', '', '', '', '', 0, 0, '', 0, '', NULL, NULL, 3),
(166, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Kushal Ghosh', '', '', '', '', '', 0, 0, '', 0, '', NULL, NULL, 3),
(167, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Krishna yadav', '', '', '', '', '', 0, 0, '', 0, '', NULL, NULL, 3),
(168, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Sajal Seth', '', '', '9878786789', '9878675645', '8789800987', 0, 0, '', 0, '', NULL, NULL, 3),
(169, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Test', '', '', '', '', '', 0, 0, '', 0, '', NULL, NULL, 3),
(170, '', NULL, '', NULL, '', 0, NULL, 'Yes', NULL, NULL, 'Pradip Kar', '', '', '9089888880', '9089786754', '0989878678', 0, 0, '', 3, '', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `demo_users_role_assign`
--

CREATE TABLE IF NOT EXISTS `demo_users_role_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `demo_users_role_assign`
--

INSERT INTO `demo_users_role_assign` (`id`, `role_id`, `user_id`, `created_by`, `created_date`) VALUES
(1, 1, 130, 1, '2017-10-10 14:39:46'),
(2, 2, 133, 1, '2017-10-13 03:16:04'),
(3, 3, 136, 1, '2017-10-13 03:17:38'),
(4, 5, 131, 1, '2017-10-13 04:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `demo_user_types`
--

CREATE TABLE IF NOT EXISTS `demo_user_types` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `bgcolor` char(7) NOT NULL DEFAULT '#607D8B',
  `flag` enum('0','1','2','3','5') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `demo_user_types`
--

INSERT INTO `demo_user_types` (`id`, `name`, `description`, `bgcolor`, `flag`) VALUES
(1, 'super_admin', 'Super Administrator', '#F44336', '0'),
(2, 'admin', 'Administrator', '#2196F3', '1'),
(3, 'agent', 'Blueyed Employee', '#607D8B', '2'),
(4, 'candidate', 'Candidate', '#e91e63', '3'),
(5, 'recruiter', 'Recruiter', '#795548', '5');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
