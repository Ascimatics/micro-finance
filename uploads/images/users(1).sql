-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Oct 12, 2017 at 10:44 PM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blueyed_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(100) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `admin_approval_status` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=pending,1=approved,2=hold,3=reject',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `admin_approval_status`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', NULL, NULL, NULL, 'V.XWxrHNpe7y5K1SSxHv/u', 1268889823, 1507869177, 1, 'Admin', '', '', '0', '0'),
(130, '', 'James', '8569bf26c19801add56c2ba9309d48bc', NULL, 'test@ascimatics.com', 'blueyed150641279559ca08fbb1b16', NULL, NULL, NULL, 2017, NULL, 1, 'James', 'Thomas', NULL, NULL, '0'),
(131, '', NULL, 'adeb0135b4715e964536c4ac680d75f6', NULL, 'manoj.ascimatics@gmail.com', 'blueyed150641363159ca0c3f46200', NULL, NULL, NULL, 2017, NULL, 1, 'Manoj', 'Kumar', NULL, NULL, '0'),
(132, '', 'Shuvojit', 'c12041273402bfa4e910bf34a92bf12c', NULL, 'shuvojit.sarker@ascimatics.com', 'blueyed150642413259ca354424b37', NULL, NULL, NULL, 2017, NULL, 0, 'Shuvojit', 'Sarker', NULL, NULL, '0'),
(133, '', 'AbhishekS', '849a6afb19cc6304e29bab4a1ee1c313', NULL, 'abhishek.singh96ms@gmail.com', 'blueyed150700246559d308610c9fc', NULL, NULL, NULL, 2017, NULL, 1, 'Abhishek', 'Kumar', NULL, NULL, '0'),
(134, '', NULL, '111e017c3fd610a9692589adfbdd4412', NULL, 'satyendra.abhishek83@gmail.com', 'blueyed150700507759d31295ba950', NULL, NULL, NULL, 2017, NULL, 0, 'Abhishek', 'Singh', NULL, NULL, '0'),
(135, '', 'Sandipan', '62a370ad9789b6ed8b94343538b69ebb', NULL, 'sandipan.biswas@ascimatics.com', 'blueyed150701246059d32f6cf38a3', NULL, NULL, NULL, 2017, NULL, 0, 'Sandipan', 'Biswas', NULL, NULL, '0'),
(136, '', 'Vinay', 'd0084beb1edf5080e212c9e0a14d4db3', NULL, 'vinay.sewal@gmail.com', 'blueyed150701347259d333601d0b4', NULL, NULL, NULL, 2017, NULL, 1, 'Vinay', 'Sewal', NULL, NULL, '0'),
(137, '', 'Sandipan', 'd5dd6b4435b74567116882cf43454cd7', NULL, 'sandipan.biswas@sworks.co.in', 'blueyed150709696259d47982c8135', NULL, NULL, NULL, 2017, NULL, 1, 'Sandipan', 'Biswas', NULL, NULL, '0'),
(138, '', 'Sandipan', 'efa078b95df0eb19eaa59b18f45885af', NULL, 'sandipan.biswas30@gmail.com', 'blueyed150712234659d4dcaa3b9ff', NULL, NULL, NULL, 2017, NULL, 0, 'Sandipan', 'Biswas', NULL, NULL, '0'),
(139, '', NULL, 'adeb0135b4715e964536c4ac680d75f6', NULL, 'vinay@theblueyed.com', 'blueyed150719540559d5fa0d7a559', NULL, NULL, NULL, 2017, NULL, 0, 'Vinay', 'Sewal', NULL, NULL, '0'),
(140, '', 'Sreejit', 'de8701981cf393dfda59da33e31e4133', NULL, 'choudhurysreejit@gmail.com', 'blueyed150762356159dc82893a29a', NULL, NULL, NULL, 2017, NULL, 1, 'Sreejit', 'Choudhury ', NULL, NULL, '0'),
(141, '', NULL, 'adeb0135b4715e964536c4ac680d75f6', NULL, 'sreejitascimatics@gmail.com', 'blueyed150762975759dc9abd50fc5', NULL, NULL, NULL, 2017, NULL, 1, 'Sreejit', 'Emp', NULL, NULL, '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
