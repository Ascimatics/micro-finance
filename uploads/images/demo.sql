-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2017 at 11:54 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demo_admin`
--

CREATE TABLE `demo_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_admin`
--

INSERT INTO `demo_admin` (`admin_id`, `admin_username`, `admin_password`, `admin_type`) VALUES
(1, 'master', 'd722dbcb93d6ca952b49928b37cac8e1', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `demo_banners`
--

CREATE TABLE `demo_banners` (
  `id` int(11) NOT NULL,
  `banner_title` varchar(255) NOT NULL,
  `banner_image` text NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_banners`
--

INSERT INTO `demo_banners` (`id`, `banner_title`, `banner_image`, `status`) VALUES
(1, 'banner title 1', 'banners/img_1474302427.jpg', 'No'),
(2, 'test', 'banners/img_1507784294.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `demo_cms`
--

CREATE TABLE `demo_cms` (
  `id` int(11) NOT NULL,
  `cms_pagetitle` varchar(255) NOT NULL,
  `cms_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_cms`
--

INSERT INTO `demo_cms` (`id`, `cms_pagetitle`, `cms_description`) VALUES
(1, 'HOME', ''),
(2, 'ABOUT US', '');

-- --------------------------------------------------------

--
-- Table structure for table `demo_roles`
--

CREATE TABLE `demo_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(200) NOT NULL,
  `role_des` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `roleID` varchar(255) NOT NULL,
  `usersID` int(11) NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_roles`
--

INSERT INTO `demo_roles` (`id`, `role_name`, `role_des`, `created_date`, `roleID`, `usersID`, `status`) VALUES
(1, 'Subscriber', 'subscriber', '2017-03-09 22:31:24', '6,7,38,39,40,44,41,42,43,45,47,48,49,50', 1, 'Yes'),
(2, 'Starter', '', '2017-03-07 00:00:44', '33,34,35,36,37,39,40,44,42,43,45', 1, 'Yes'),
(3, 'skills', 'view skills', '2017-03-27 23:21:57', '37,71,72,73,74', 1, 'Yes'),
(5, 'test role', 'description', '2017-10-12 06:13:37', '2,3,14,7', 1, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `demo_role_features`
--

CREATE TABLE `demo_role_features` (
  `id` int(11) NOT NULL,
  `role_group_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `des` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_role_features`
--

INSERT INTO `demo_role_features` (`id`, `role_group_id`, `name`, `des`) VALUES
(1, 1, 'users_list', 'View Users List'),
(2, 1, 'edit_user', 'Edit User'),
(3, 1, 'view_user', 'View User'),
(4, 1, 'delete_user', 'Delete User'),
(5, 4, 'user_group_panel', 'User Group Panel View'),
(6, 2, 'candidate_list', 'Candidate List'),
(7, 2, 'view_candidate', 'View Candidate'),
(8, 2, 'edit_candidate', 'Edit Candidate'),
(9, 2, 'delete_candidate', 'Delete Candidate'),
(10, 3, 'recruiter_list', 'Recruiter List'),
(11, 3, 'view_recruiter', 'View Recruiter'),
(12, 3, 'edit_recruiter', 'Edit Recruiter'),
(13, 3, 'delete_recruiter', 'Delete Recruiter'),
(14, 1, 'create_user', 'Create User'),
(15, 2, 'create_candidate', 'Create Candidate'),
(16, 3, 'create_recruiter', 'Create Recruiter'),
(17, 5, 'display', 'Display of the Blog Management at Right Panel'),
(18, 5, 'add', 'Add new Blog'),
(19, 5, 'edit', 'Edit existing Blogs'),
(20, 5, 'delete', 'Delete existing Blog'),
(21, 6, 'display', 'Display of the Content Management at Right Panel'),
(22, 6, 'add', 'Add new Content'),
(23, 6, 'edit', 'Edit existing Content'),
(24, 6, 'delete', 'Delete existing Content'),
(25, 7, 'display', 'Display of the Template Management at Right Panel'),
(26, 7, 'add', 'Add new Template'),
(27, 7, 'edit', 'Edit existing Template'),
(28, 7, 'delete', 'Delete existing Template'),
(29, 8, 'display', 'Display of Role Management in the Right panel'),
(30, 8, 'add', 'Add new Roles'),
(31, 8, 'edit', 'Edit Existing Roles'),
(32, 8, 'delete', 'Delete Existing Roles'),
(33, 9, 'display', 'Display Assign Role at right Panel'),
(34, 9, 'add', 'Assign Roles'),
(35, 9, 'edit', 'Edit assigned Roles'),
(36, 9, 'delete', 'Delete assigned roles'),
(37, 10, 'view', 'Display Settings'),
(38, 11, 'add', 'Add new degrees '),
(39, 11, 'edit', 'Edit existing degrees'),
(40, 11, 'delete', 'Delete existing degrees'),
(41, 12, 'add', 'Add new questions'),
(42, 12, 'edit', 'Edit existing questions'),
(43, 12, 'delete', 'Delete existing questions'),
(44, 11, 'view', 'Display the Degree panel'),
(45, 12, 'view', 'Display the question panel'),
(46, 1, 'create', 'User Create'),
(47, 13, 'view', 'Display the College panel'),
(48, 13, 'add', 'Add new College'),
(49, 13, 'edit', 'Edit existing College'),
(50, 13, 'delete', 'Delete existing College'),
(51, 14, 'view', 'View Company List'),
(52, 14, 'add', 'Create Company Name'),
(53, 14, 'edit', 'Edit Company Name'),
(54, 14, 'delete', 'Delete Company Name'),
(55, 15, 'view', 'View Work Title List'),
(56, 15, 'add', 'Create Work Title'),
(57, 15, 'edit', 'Edit Work Title'),
(58, 15, 'delete', 'Delete Work Title'),
(59, 16, 'view', 'View UG Degree List'),
(60, 16, 'create', 'Create UG Degree'),
(61, 16, 'edit', 'Edit UG Degree'),
(62, 16, 'delete', 'Delete UG Degree'),
(63, 17, 'view', 'View UG college List'),
(64, 17, 'create', 'Create UG College'),
(65, 17, 'edit', 'Edit UG Collge'),
(66, 17, 'delete', 'Delete UG college'),
(67, 18, 'view', 'View HS School List'),
(68, 18, 'create', 'Create HS School'),
(69, 18, 'edit', 'Edit HS School'),
(70, 18, 'delete', 'Delete HS School'),
(71, 19, 'view', 'View Skills'),
(72, 19, 'create', 'Create Skills'),
(73, 19, 'edit', 'Edit Skills'),
(74, 19, 'delete', 'Delete Skills'),
(75, 20, 'view', 'View Graduation Major'),
(76, 20, 'create', 'Create Graduation Major'),
(77, 20, 'edit', 'Edit Graduation Major'),
(78, 20, 'delete', 'Delete Graduation Major'),
(79, 21, 'view', 'View Under Graduation Major'),
(80, 21, 'create', 'Create Under Graduation Major'),
(81, 21, 'edit', 'Edit Under Graduation Major'),
(82, 21, 'delete', 'Delete Under Graduation Major'),
(83, 22, 'view', 'View CTC Band'),
(84, 22, 'create ', 'Create CTC Band'),
(85, 22, 'edit', 'Edit CTC Band'),
(86, 22, 'delete ', 'Delete CTC Band'),
(87, 23, 'view', 'View Hs Stream'),
(88, 23, 'create', 'Create Hs stream'),
(89, 23, 'edit', 'Edit Hs Stream'),
(90, 23, 'delete', 'Delete Hs stream');

-- --------------------------------------------------------

--
-- Table structure for table `demo_role_group`
--

CREATE TABLE `demo_role_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `code` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '0 => Not Deleted ; 1 => Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_role_group`
--

INSERT INTO `demo_role_group` (`id`, `group_name`, `code`, `is_deleted`) VALUES
(1, 'User', 'user', 0),
(2, 'Candidate', 'candidate', 0),
(3, 'Recruiter', 'recruiter', 0),
(4, 'Group Panel', 'group', 0),
(5, 'Blog Management', 'blog', 1),
(6, 'Content Management', 'content', 0),
(7, 'Template Management', 'template', 0),
(8, 'Role Management', 'role', 0),
(9, 'Assign Role Management', 'role_assign', 0),
(10, 'Candidate Setting', 'candidate_setting', 0),
(11, 'Degree', 'degree', 0),
(12, 'Question', 'question', 0),
(13, 'College', 'college', 0),
(14, 'Company', 'company', 0),
(15, 'Work Title', 'title', 0),
(16, 'UG Degree', 'ugdegree', 0),
(17, 'UG College', 'ugcollege', 0),
(18, 'School', 'school', 0),
(19, 'Skills', 'skills', 0),
(20, 'Graduation Major', 'graduation major', 0),
(21, 'Under Graduation Major', 'under graduation major', 0),
(22, 'CTC Band', 'ctc band', 0),
(23, 'Hs Stream', 'hs stream', 0);

-- --------------------------------------------------------

--
-- Table structure for table `demo_settings`
--

CREATE TABLE `demo_settings` (
  `id` int(11) NOT NULL,
  `config_type` varchar(255) NOT NULL,
  `config_val` varchar(255) NOT NULL,
  `config_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_settings`
--

INSERT INTO `demo_settings` (`id`, `config_type`, `config_val`, `config_title`) VALUES
(1, 'admin_email', 'no-reply@bnc.com', 'Admin Email'),
(2, 'facebook_link', 'https://www.facebook.com', 'facebook Link'),
(3, 'twitter_link', 'http://twitter.com', 'twitter Link'),
(4, 'youtube_link', 'http://youtube.com', 'youtube Link'),
(5, 'googleplus_link', 'http://google.com', 'googleplus Link'),
(6, 'linkedin_link', 'http://linkedin.com', 'linkedin Link'),
(7, 'banner_width', '1024', 'Banner width'),
(8, 'banner_height', '768', 'Banner height');

-- --------------------------------------------------------

--
-- Table structure for table `demo_user_types`
--

CREATE TABLE `demo_user_types` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `bgcolor` char(7) NOT NULL DEFAULT '#607D8B',
  `flag` enum('0','1','2','3','5') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `demo_user_types`
--

INSERT INTO `demo_user_types` (`id`, `name`, `description`, `bgcolor`, `flag`) VALUES
(1, 'super_admin', 'Super Administrator', '#F44336', '0'),
(2, 'admin', 'Administrator', '#2196F3', '1'),
(3, 'agent', 'Blueyed Employee', '#607D8B', '2'),
(4, 'candidate', 'Candidate', '#e91e63', '3'),
(5, 'recruiter', 'Recruiter', '#795548', '5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `demo_admin`
--
ALTER TABLE `demo_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `demo_banners`
--
ALTER TABLE `demo_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_cms`
--
ALTER TABLE `demo_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_roles`
--
ALTER TABLE `demo_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_role_features`
--
ALTER TABLE `demo_role_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_role_group`
--
ALTER TABLE `demo_role_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_settings`
--
ALTER TABLE `demo_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_user_types`
--
ALTER TABLE `demo_user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `demo_admin`
--
ALTER TABLE `demo_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `demo_banners`
--
ALTER TABLE `demo_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `demo_cms`
--
ALTER TABLE `demo_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `demo_roles`
--
ALTER TABLE `demo_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `demo_role_features`
--
ALTER TABLE `demo_role_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `demo_role_group`
--
ALTER TABLE `demo_role_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `demo_settings`
--
ALTER TABLE `demo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `demo_user_types`
--
ALTER TABLE `demo_user_types`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
