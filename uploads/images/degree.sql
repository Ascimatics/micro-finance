-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 21, 2017 at 01:55 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blueyed_staging`
--

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE IF NOT EXISTS `degree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `degree_name` varchar(200) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `degree`
--

INSERT INTO `degree` (`id`, `degree_name`, `status`, `created_date`) VALUES
(7, 'B Tech', '1', '2017-02-27 09:48:55'),
(8, 'M Tech', '1', '2017-02-27 09:49:02'),
(9, 'Science', '1', '2017-02-27 10:52:12'),
(10, 'Commerce', '1', '2017-02-27 10:52:19'),
(11, 'Arts', '1', '2017-02-27 10:52:26'),
(12, 'MCA', '1', '2017-02-27 10:52:32'),
(13, 'BCA', '1', '2017-02-27 10:52:39'),
(14, '10th', '1', '2017-02-27 10:52:48'),
(15, '12th', '1', '2017-02-27 10:52:54'),
(16, 'Pharma', '1', '2017-02-27 10:53:07'),
(17, 'Test', '1', '2017-03-20 14:11:28'),
(18, 'fvgvngh', '1', '2017-03-23 13:50:23'),
(19, 'cghgf', '1', '2017-03-23 13:50:27'),
(20, 'xfgfdg', '1', '2017-03-23 13:50:32'),
(21, 'd', '1', '2017-03-23 13:50:36'),
(22, 'ggggggggggggggggggg', '1', '2017-03-23 13:50:39'),
(23, 'gbggggggggggg', '1', '2017-03-23 13:50:50'),
(25, 'rg', '1', '2017-03-23 13:51:03'),
(26, 'Other Grad Degree', '1', '2017-04-11 10:48:25'),
(27, 'hfggsdgfdg', '1', '2017-04-28 09:32:24'),
(28, 'My Post Graduation Degree (Other)', '1', '2017-05-02 05:49:39'),
(33, 'MBA Other Degree', '1', '2017-05-09 12:50:21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
