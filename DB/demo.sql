-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2016 at 04:45 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(11) NOT NULL,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demo_admin`
--

CREATE TABLE `demo_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_admin`
--

INSERT INTO `demo_admin` (`admin_id`, `admin_username`, `admin_password`, `admin_type`) VALUES
(1, 'master', 'd722dbcb93d6ca952b49928b37cac8e1', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `demo_banners`
--

CREATE TABLE `demo_banners` (
  `id` int(11) NOT NULL,
  `banner_title` varchar(255) NOT NULL,
  `banner_image` text NOT NULL,
  `status` enum('Yes','No') NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_banners`
--

INSERT INTO `demo_banners` (`id`, `banner_title`, `banner_image`, `status`) VALUES
(1, 'banner title 1', 'banners/img_1474302427.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `demo_cms`
--

CREATE TABLE `demo_cms` (
  `id` int(11) NOT NULL,
  `cms_pagetitle` varchar(255) NOT NULL,
  `cms_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_cms`
--

INSERT INTO `demo_cms` (`id`, `cms_pagetitle`, `cms_description`) VALUES
(1, 'HOME', ''),
(2, 'ABOUT US', '');

-- --------------------------------------------------------

--
-- Table structure for table `demo_settings`
--

CREATE TABLE `demo_settings` (
  `id` int(11) NOT NULL,
  `config_type` varchar(255) NOT NULL,
  `config_val` varchar(255) NOT NULL,
  `config_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demo_settings`
--

INSERT INTO `demo_settings` (`id`, `config_type`, `config_val`, `config_title`) VALUES
(1, 'admin_email', 'no-reply@bnc.com', 'Admin Email'),
(2, 'facebook_link', 'https://www.facebook.com', 'facebook Link'),
(3, 'twitter_link', 'http://twitter.com', 'twitter Link'),
(4, 'youtube_link', 'http://youtube.com', 'youtube Link'),
(5, 'googleplus_link', 'http://google.com', 'googleplus Link'),
(6, 'linkedin_link', 'http://linkedin.com', 'linkedin Link'),
(7, 'banner_width', '1024', 'Banner width'),
(8, 'banner_height', '768', 'Banner height');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `demo_admin`
--
ALTER TABLE `demo_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `demo_banners`
--
ALTER TABLE `demo_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_cms`
--
ALTER TABLE `demo_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_settings`
--
ALTER TABLE `demo_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `demo_admin`
--
ALTER TABLE `demo_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `demo_banners`
--
ALTER TABLE `demo_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `demo_cms`
--
ALTER TABLE `demo_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `demo_settings`
--
ALTER TABLE `demo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
