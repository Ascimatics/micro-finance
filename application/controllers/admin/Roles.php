<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Roles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
        $this->load->helper("custom_helper");
        $this->load->model("admin/role_model");
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'roles');    
		    
        $this->load->view('admin/role',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/


    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

     	$this->data['role_types']   = $this->role_model->get_roles();		
		
        $this->load->view('admin/addRole',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {
          
	
		// field name, error message, validation rules
	  	$this->form_validation->set_rules('role_name', 'role name', 'trim|required');

		 
		if($this->form_validation->run() == FALSE){
					redirect('admin/roles');
		}
		else{ 			
			 
					$role_name=$this->input->post('role_name');

					$role_des=$this->input->post('role_des');

					$roleID = implode(',',$this->input->post('roleID'));
					
					$created_date = date('Y-m-d h:i:s');
			
					$usersID = $this->session->userdata('id');
						
					$data=array('role_name'=>$role_name,'role_des'=>$role_des,'created_date'=>$created_date,'roleID'=>$roleID,'usersID'=>$usersID);
						
					$this->general_model->add(TABLE_PREFIX.'roles',$data); 

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/roles');				
			 	
			}		 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_data(TABLE_PREFIX.'roles',$where);     

        $this->data['role_types']   = $this->role_model->get_roles();
		
		$this->data['role']   = $this->role_model->grab_role($id);	     
            
        $this->load->view('admin/editRole',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {

    	// field name, error message, validation rules
	  	$this->form_validation->set_rules('role_name', 'role name', 'trim|required');

		 
		if($this->form_validation->run() == FALSE){

					$this->session->set_flashdata('message', 'error|Error in updating data.');
					
					redirect('admin/roles');
		}
		else{ 			
				
					$role_name=$this->input->post('role_name');

					$role_des=$this->input->post('role_des');

					$roleID = implode(',',$this->input->post('roleID'));
					
					$created_date = date('Y-m-d h:i:s');
			
					$usersID = $this->session->userdata('id');
					

					$data = array('role_name'=>$role_name,'role_des'=>$role_des,'created_date'=>$created_date,'roleID'=>$roleID,'usersID'=>$usersID);

					$where = array('id'	=>	$id);

	    			$this->general_model->update(TABLE_PREFIX.'roles',$data,$where);

	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/roles');
			 			
			 						
						
			}

    }

    /****************************  END UPDATE DATA ****************************/




    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$current_status = $status == 'true' ? 'Yes' : 'No';
			
			$data = array('status'=>$current_status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'banners',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/

	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function roleassign() {

        $this->data['viewData'] = $this->role_model->get_assigned_roles();    
		    
        $this->load->view('admin/roleassign',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function createRoleAssign() {	

     	$this->data['user_groups']   = $this->general_model->get_all('demo_groups');	

     	$this->data['roles']   = $this->general_model->get_all('demo_roles');	
		
        $this->load->view('admin/ajax/addRoleAssign',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/

    /****************************  START FETCH USER BY GROUP ******************/

     public function fetchUserByGroup() {	

     	$group_id = $this->input->post('group_id');	

     	$this->data['userlist'] = $this->general_model->get_data('demo_users',array('group_id'=>$group_id));
		
        $this->load->view('admin/ajax/userByGroup',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/

    /****************************  START INSERT FORM DATA ********************/
    public function submitAssignRoleData() {
          
	
		// field name, error message, validation rules
		$this->form_validation->set_rules('user_id', 'Username', 'trim|required');
	  	$this->form_validation->set_rules('role_id', 'role name', 'trim|required');

		 
		if($this->form_validation->run() == FALSE){
					redirect('admin/roles/roleassign');
		}
		else{ 			
			 
					$user_id=$this->input->post('user_id');

					$role_id=$this->input->post('role_id');
					
					$created_date = date('Y-m-d h:i:s');
			
					$created_by = $this->session->userdata('id');
						
					$data=array('user_id'=>$user_id,'role_id'=>$role_id,'created_date'=>$created_date,'created_by'=>$created_by);
						
					$this->general_model->add(TABLE_PREFIX.'users_role_assign',$data); 

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/roles/roleassign');				
			 	
			}		 
    }

    /****************************  END INSERT FORM DATA ************************/


     /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editRoleAssign() {
        
        $id = $this->input->post('id');

        $where=array('demo_users_role_assign.id' => $id);
        
        $this->data['roleassign']= $this->role_model->get_assigned_roles($where); 

        $this->data['userlist'] = $this->general_model->get_data('demo_users',array('group_id'=>$this->data['roleassign']->group_id));

        $this->data['user_groups']   = $this->general_model->get_all(TABLE_PREFIX.'groups');	

     	$this->data['roles'] = $this->general_model->get_all(TABLE_PREFIX.'roles');	 
     
            
        $this->load->view('admin/ajax/editRoleAssign',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/

	/****************************  INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

	private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FILE_UPLOAD_PATH.'banners/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = 'img_'.time();
		// $config['max_size']     = '100';
		// $config['max_width'] = '1024';
		// $config['max_height'] = '768';

        return $config;
    }
    
    /****************************  END INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

}

/* End of file banners.php */
/* Location: ./application/controllers/admin/banners.php */