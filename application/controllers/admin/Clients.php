<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }

        $this->load->model('admin/client_model');
        $this->load->library('Ajax_pagination');
        if($this->session->has_userdata('perPage')){
			$this->perPage = $this->session->userdata('perPage');
        }else{
        	$this->perPage = 10;
        }
        
    }

    function setPerPage(){
    	$perPage = $this->input->post('perPage');
    	$this->session->set_userdata('perPage', $perPage);
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

    	$data = array();
        
        //total rows count
        $totalRec = count($this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'4')));
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/clients/ajaxClientData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'4'),array('limit'=>$this->perPage));

        //$this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/client',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN AJAX PAGINATION ******************/

    function ajaxClientData($page=''){
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'4'))); 
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/clients/ajaxClientData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'4'),array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('admin/ajax/ajax_client_data', $this->data, false);
    }
	
	/****************************  END AJAX PAGINATION ******************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

     	$this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
		
		$this->data['firm_types']   = $this->general_model->get_data(TABLE_PREFIX.'firm_type',array('status'=>'Yes'));

		$this->data['concern_designation']   = $this->general_model->get_data(TABLE_PREFIX.'concern_designation',array('status'=>'Yes'));

     	$this->data['broker_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'3'));		
     	$this->data['collection_man_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'5'));

     	//$this->data['area_list']   = $this->general_model->get_data(TABLE_PREFIX.'area');
			
        $this->load->view('admin/addClient',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {


    				/*------Insert Client information-------*/

					$broker_ids = $this->input->post('broker_ids');
				
					if (in_array('new', $broker_ids)) {
						$data=array(	
							'full_name'=>$this->input->post('broker_full_name'),
							'work_address'=>$this->input->post('broker_work_address'),
							'residence_address'=>$this->input->post('broker_residence_address'),
							'work_telephone'=>$this->input->post('broker_work_telephone'),
							'mobile1'=>$this->input->post('broker_mobile1'),
							'mobile2'=>$this->input->post('broker_mobile2'),
							'group_id'=>'3'
						);
							
						$broker_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 

						array_push($broker_ids,$broker_id);
					}	


					$firm_type_id = $this->input->post('firm_type_id');
					
					if($firm_type_id=='new'){
							
						$data=array(
						'type'=>$this->input->post('firm_type')
						);
						
						$firm_type_id = $this->general_model->add(TABLE_PREFIX.'firm_type',$data); 
					}



					$data=array(	
						'full_name'=>$this->input->post('full_name'),
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'introduced_by'=>$this->input->post('introduced_by'),
						'introduce_phone'=>$this->input->post('introduce_contact'),
						'broker_ids'=>implode(",",$broker_ids),
						'firm_type_id'=>$firm_type_id,
						'note'=>$this->input->post('note'),
						'group_id'=>'4'
					);
						
					$client_id = $this->general_model->add(TABLE_PREFIX.'users',$data);
					
					$client_phone = $this->input->post('phone');
					foreach ($client_phone as $key => $value) {
						$data=array(	
							'phone'=>$value,
							'client_id'=>$client_id
						);
						$this->general_model->add(TABLE_PREFIX.'client_phone',$data); 
					}

					/*------Insert Loan drawee information-------*/

					$concern_name = $this->input->post('concern_name');
					$concern_designation_id = $this->input->post('concern_designation_id');

					foreach ($concern_designation_id as $key => $value) {
						
						if($value='new'){
							
							$data=array(	
								'designation'=>$this->input->post('concern_designation')
							);
							
							$value = $this->general_model->add(TABLE_PREFIX.'concern_designation',$data); 
							$concern_name[$key]=$this->input->post('concern_name_text');
						}
							$data=array(	
								'name'=>$concern_name[$key],
								'designation_id'=>$value,
								'client_id'=>$client_id,
								'firm_type_id'=>$firm_type_id
							);
							$this->general_model->add(TABLE_PREFIX.'concern_people',$data); 
					}

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/clients');				
			 	
				 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_row(TABLE_PREFIX.'users',$where);  
		
		$this->data['firm_types']   = $this->general_model->get_data(TABLE_PREFIX.'firm_type',array('status'=>'Yes'));

        $this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
		
		$this->data['phone_list']   = $this->general_model->get_data(TABLE_PREFIX.'client_phone',array('client_id'=>$id)); 

        $this->data['broker_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'3'));		
     	$this->data['collection_man_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'5'));

     	$this->data['firm_types']   = $this->general_model->get_data(TABLE_PREFIX.'firm_type',array('status'=>'Yes'));

		$this->data['concern_designation']   = $this->general_model->get_data(TABLE_PREFIX.'concern_designation',array('status'=>'Yes'));

		$this->data['concern_people']   = $this->general_model->get_data(TABLE_PREFIX.'concern_people',array('client_id'=>$id));
            
        $this->load->view('admin/editClient',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {		

					/*------Update Client information-------*/


					$broker_ids = $this->input->post('broker_ids');
				
					if (in_array('new', $broker_ids)) {
						$data=array(	
							'full_name'=>$this->input->post('broker_full_name'),
							'work_address'=>$this->input->post('broker_work_address'),
							'residence_address'=>$this->input->post('broker_residence_address'),
							'work_telephone'=>$this->input->post('broker_work_telephone'),
							'mobile1'=>$this->input->post('broker_mobile1'),
							'mobile2'=>$this->input->post('broker_mobile2'),
							'group_id'=>'3'
						);
							
						$broker_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 
						array_push($broker_ids,$broker_id);
					}

					$firm_type_id = $this->input->post('firm_type_id');
          
					if($firm_type_id=='new'){
					  
					$data=array(
					'type'=>$this->input->post('firm_type')
					);

					$firm_type_id = $this->general_model->add(TABLE_PREFIX.'firm_type',$data); 
					}

					$data=array(	
						'full_name'=>$this->input->post('full_name'),
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'introduced_by'=>$this->input->post('introduced_by'),
            			'introduce_phone'=>$this->input->post('introduce_contact'),
						'broker_ids'=>implode(",",$broker_ids),
						'firm_type_id'=>$firm_type_id,
            			'note'=>$this->input->post('note')
					);
						
					$this->general_model->update(TABLE_PREFIX.'users',$data,array('id'=>$id)); 
					
					$this->general_model->delete(TABLE_PREFIX.'client_phone',array('client_id'=>$id),'single');
					$client_phone = $this->input->post('phone');
					foreach ($client_phone as $key => $value) {
						$data=array(	
							'phone'=>$value,
							'client_id'=>$id
						);
						$this->general_model->add(TABLE_PREFIX.'client_phone',$data); 
					}

					/*------Insert Account drawee information-------*/
					

			$this->general_model->delete(TABLE_PREFIX.'concern_people',array('client_id'=>$id),'single');

			$concern_name = $this->input->post('concern_name');
          	$concern_designation_id = $this->input->post('concern_designation_id');

          foreach ($concern_designation_id as $key => $value) {
            
            if($value=='new'){
              
              $data=array(  
                'designation'=>$this->input->post('concern_designation')
              );
              
              $value = $this->general_model->add(TABLE_PREFIX.'concern_designation',$data); 
              $concern_name[$key]=$this->input->post('concern_name_text');
            }
              $data=array(  
                'name'=>$concern_name[$key],
                'designation_id'=>$value,
                'client_id'=>$id,
                'firm_type_id'=>$firm_type_id
              );
              $this->general_model->add(TABLE_PREFIX.'concern_people',$data); 
          }
					

	    	$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			redirect('admin/clients');		

    }

    /****************************  END UPDATE DATA ****************************/

    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$data = array('status'=>$status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'users',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/


    /****************************  START ADDING MORE DOCUMENT FIELDS **************************/
	
	public function addMoreDocument(){
	
		$this->load->view('admin/ajax/addMoreDocument');
		
	}

	public function addMoreGuarantor(){
	
		$this->load->view('admin/ajax/addMoreGuarantor');
		
	}

	public function addMoreBroker(){
	
		$this->load->view('admin/ajax/addMoreBroker');
		
	}

	public function addMoreIntroduced(){
	
		$this->load->view('admin/ajax/addMoreIntroduced');
		
	}

	public function addCheque(){
	
		$this->load->view('admin/ajax/addCheque');
		
	}

	public function addMoreCheque(){
	
		$this->load->view('admin/ajax/addMoreCheque');
		
	}

	/****************************  END MORE DOCUMENT FIELDS ***************************/

	/****************************  INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

	private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FILE_UPLOAD_PATH.'banners/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|docx|doc|txt';
		$config['file_name'] = 'img_'.time();
		// $config['max_size']     = '100';
		// $config['max_width'] = '1024';
		// $config['max_height'] = '768';

        return $config;
    }
    
    /****************************  END INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

}

/* End of file Clients.php */
/* Location: ./application/controllers/admin/Clients.php */