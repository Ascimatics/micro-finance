<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Broker extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }

        $this->load->library('Ajax_pagination');
        if($this->session->has_userdata('perPage')){
			$this->perPage = $this->session->userdata('perPage');
        }else{
        	$this->perPage = 10;
        }
        
    }

    function setPerPage(){
    	$perPage = $this->input->post('perPage');
    	$this->session->set_userdata('perPage', $perPage);
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

    	$data = array();
        
        //total rows count
        $totalRec = count($this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'3')));
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/broker/ajaxBrokerData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'3'),array('limit'=>$this->perPage));

        //$this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/broker',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN AJAX PAGINATION ******************/

    function ajaxBrokerData($page=''){
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'3'))); 
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/broker/ajaxBrokerData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'users',array('group_id'=>'3'),array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('admin/ajax/ajax_broker_data', $this->data, false);
    }
	
	/****************************  END AJAX PAGINATION ******************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

     	//$this->data['area_list']   = $this->general_model->get_data(TABLE_PREFIX.'area');
			
        $this->load->view('admin/addBroker');
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {

    				/*------Insert Client information-------*/

					$data=array(	
						'full_name'=>$this->input->post('full_name'),
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'work_telephone'=>$this->input->post('work_telephone'),
						'mobile1'=>$this->input->post('mobile1'),
						'mobile2'=>$this->input->post('mobile2'),
						'group_id'=>'3'
					);
						
					$client_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/broker');				
			 	
			//}		 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_row(TABLE_PREFIX.'users',$where);   
	        
        $this->load->view('admin/editBroker',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {		
				
					/*------Update Client information-------*/

					$data=array(	
						'full_name'=>$this->input->post('full_name'),
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'work_telephone'=>$this->input->post('work_telephone'),
						'mobile1'=>$this->input->post('mobile1'),
						'mobile2'=>$this->input->post('mobile2'),
					);
						
					$this->general_model->update(TABLE_PREFIX.'users',$data,array('id'=>$id)); 
					

	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/broker');		

    }

    /****************************  END UPDATE DATA ****************************/

    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$data = array('status'=>$status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'users',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/

}

/* End of file Clients.php */
/* Location: ./application/controllers/admin/Clients.php */