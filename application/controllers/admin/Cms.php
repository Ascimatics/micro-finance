<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    /**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'cms');    
 
        $this->load->view('admin/cms',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_data(TABLE_PREFIX.'cms',$where);          
            
        $this->load->view('admin/managecms',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/

    /****************************  START UPDATE DATA *************************/ 
    
    public function updateData($id) {
          
        // field name, error message, validation rules
        $this->form_validation->set_rules('cms_pagetitle', 'Page title', 'trim|required');

        if ($this->form_validation->run() == FALSE) {        

            $this->session->set_flashdata('message', 'error|Error in updating data.');
             
            redirect('admin/cms');
        } 
        else 
        {
            $data=$this->input->post();
            
            $where = array('id' =>  $id);
         
            $this->general_model->update(TABLE_PREFIX.'cms',$data,$where); 

            $this->session->set_flashdata('message', 'success|Data updated successfully.');
             
            redirect('admin/cms');
        } 
    }

    /****************************  END UPDATE DATA ****************************/
    

}