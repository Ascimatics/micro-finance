<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
        $this->load->helper("custom_helper");
        $this->load->model("admin/role_model");
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

        $this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/client',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/


    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

     	$this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
     	$this->data['broker_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'3'));		
     	$this->data['collection_man_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'5'));		
		
        $this->load->view('admin/addClient',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {

    				/*------Insert Client information-------*/

					$data=array(	
						'first_name'=>$this->input->post('client_name'),
						'last_name'=>'',
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'work_telephone'=>$this->input->post('work_telephone'),
						'mobile1'=>$this->input->post('mobile1'),
						'mobile2'=>$this->input->post('mobile2'),
						'introduced_by'=>$this->input->post('introduced_by'),
						'collection_man_id'=>$this->input->post('collection_man_id'),
						'area_id'=>$this->input->post('area_id'),
						'broker_ids'=>implode(",",$this->input->post('broker_ids')),
						'note'=>$this->input->post('note'),
						'group_id'=>'4'
					);
						
					$client_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 

					/*------Insert Client account information-------*/

					$data=array(	
							'client_id'=>$client_id,
							'loan_number'=>$this->input->post('loan_number'),
							'loan_amount '=>$this->input->post('loan_amount'),
							'no_of_days'=>$this->input->post('no_of_days'),
							'interest_rate'=>$this->input->post('interest_rate')
					);

					$account_id = $this->general_model->add(TABLE_PREFIX.'accounts',$data); 

					/*------Insert Account drawee information-------*/

					$drawee_name = $this->input->post('drawee_name');
					$drawee_designation = $this->input->post('drawee_designation');

					foreach ($drawee_name as $key => $value) {
						$data=array(	
							'drawee_name'=>$value,
							'drawee_designation'=>$drawee_designation[$key],
							'account_id'=>$account_id
						);
						$this->general_model->add(TABLE_PREFIX.'draw_details',$data); 
					}

					/*------Insert Account guarantor information-------*/

					$guarantor_name = $this->input->post('guarantor_name');
					$guarantor_designation = $this->input->post('guarantor_designation');
					$blood_relation = $this->input->post('blood_relation');
					$guarantor_address = $this->input->post('guarantor_address');
					$tel1 = $this->input->post('tel1');
					$tel2 = $this->input->post('tel2');

					foreach ($guarantor_name as $key => $value) {
						$data=array(	
							'guarantor_name'=>$value,
							'guarantor_designation'=>$guarantor_designation[$key],
							'blood_relation'=>$blood_relation[$key],
							'guarantor_address'=>$guarantor_address[$key],
							'tel1'=>$tel1[$key],
							'tel2'=>$tel2[$key],
							'account_id'=>$account_id
						);
						$this->general_model->add(TABLE_PREFIX.'guarantor',$data); 
					}

					/*------Insert Client Document information-------*/

					$document_type = $this->input->post('document_type');
					//$doc_received_status = $this->input->post('doc_received_status');
					$purpose = $this->input->post('purpose');
					$recived_by = $this->input->post('recived_by');
					//$validity_status = $this->input->post('validity_status');
					$expiry_date = $this->input->post('expiry_date');
					//$return_status = $this->input->post('return_status');
					$return_date = $this->input->post('return_date');

					foreach ($document_type as $key => $value) {
						$data=array(	
							'client_id'=>$client_id,
							'document_type'=>$value,
							'purpose'=>$purpose[$key],
						
							'recived_by'=>$recived_by[$key],
					
							'expiry_date'=>strtotime($expiry_date[$key]),
			
							'return_date'=>strtotime($return_date[$key])
						);
						$this->general_model->add(TABLE_PREFIX.'client_document',$data); 
					}

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/clients');				
			 	
			//}		 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_row(TABLE_PREFIX.'users',$where);   
        $this->data['account_data']= $this->general_model->get_row(TABLE_PREFIX.'accounts',array('client_id'=>$id));   

        $this->data['drawee_list']   = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('account_id'=>$this->data['account_data']->id));

        $this->data['guarantor_list']   = $this->general_model->get_data(TABLE_PREFIX.'guarantor',array('account_id'=>$this->data['account_data']->id));

        $this->data['document_list']   = $this->general_model->get_data(TABLE_PREFIX.'client_document',array('client_id'=>$id));

     	$this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
     	$this->data['broker_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'3'));		
     	$this->data['collection_man_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'5'));	    
            
        $this->load->view('admin/editClient',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {		
				
					/*------Update Client information-------*/

					$data=array(	
						'first_name'=>$this->input->post('client_name'),
						'last_name'=>'',
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'work_telephone'=>$this->input->post('work_telephone'),
						'mobile1'=>$this->input->post('mobile1'),
						'mobile2'=>$this->input->post('mobile2'),
						'introduced_by'=>$this->input->post('introduced_by'),
						'collection_man_id'=>$this->input->post('collection_man_id'),
						'area_id'=>$this->input->post('area_id'),
						'broker_ids'=>implode(",",$this->input->post('broker_ids')),
						'note'=>$this->input->post('note')
					);
						
					$this->general_model->update(TABLE_PREFIX.'users',$data,array('id'=>$id)); 

					/*------Update Client account information-------*/
					 

					$data=array(	
							'loan_number'=>$this->input->post('loan_number'),
							'loan_amount '=>$this->input->post('loan_amount'),
							'no_of_days'=>$this->input->post('no_of_days'),
							'interest_rate'=>$this->input->post('interest_rate')
					);

					$this->general_model->update(TABLE_PREFIX.'accounts',$data,array('client_id'=>$id)); 

					/*------Insert Account drawee information-------*/
					$this->data['account_data']= $this->general_model->get_row(TABLE_PREFIX.'accounts',array('client_id'=>$id));

			$this->general_model->delete(TABLE_PREFIX.'draw_details',array('account_id'=>$this->data['account_data']->id),'all');

					$drawee_name = $this->input->post('drawee_name');
					$drawee_designation = $this->input->post('drawee_designation');

					foreach ($drawee_name as $key => $value) {
						$data=array(	
							'drawee_name'=>$value,
							'drawee_designation'=>$drawee_designation[$key],
							'account_id'=>$account_id
						);
						$this->general_model->add(TABLE_PREFIX.'draw_details',$data); 
					}

					/*------Insert Account guarantor information-------*/
					$this->general_model->delete(TABLE_PREFIX.'guarantor',array('account_id'=>$this->data['account_data']->id),'all');

					$guarantor_name = $this->input->post('guarantor_name');
					$guarantor_designation = $this->input->post('guarantor_designation');
					$blood_relation = $this->input->post('blood_relation');
					$guarantor_address = $this->input->post('guarantor_address');
					$tel1 = $this->input->post('tel1');
					$tel2 = $this->input->post('tel2');

					foreach ($guarantor_name as $key => $value) {
						$data=array(	
							'guarantor_name'=>$value,
							'guarantor_designation'=>$guarantor_designation[$key],
							'blood_relation'=>$blood_relation[$key],
							'guarantor_address'=>$guarantor_address[$key],
							'tel1'=>$tel1[$key],
							'tel2'=>$tel2[$key],
							'account_id'=>$account_id
						);
						$this->general_model->add(TABLE_PREFIX.'guarantor',$data); 
					}

					/*------Insert Client Document information-------*/

					$this->general_model->delete(TABLE_PREFIX.'client_document',array('client_id'=>$id),'all');

					$document_type = $this->input->post('document_type');
					//$doc_received_status = $this->input->post('doc_received_status');
					$purpose = $this->input->post('purpose');
					$recived_by = $this->input->post('recived_by');
					//$validity_status = $this->input->post('validity_status');
					$expiry_date = $this->input->post('expiry_date');
					//$return_status = $this->input->post('return_status');
					$return_date = $this->input->post('return_date');

					foreach ($document_type as $key => $value) {
						$data=array(	
							'client_id'=>$client_id,
							'document_type'=>$value,
							'purpose'=>$purpose[$key],
						
							'recived_by'=>$recived_by[$key],
					
							'expiry_date'=>strtotime($expiry_date[$key]),
			
							'return_date'=>strtotime($return_date[$key])
						);
						$this->general_model->add(TABLE_PREFIX.'client_document',$data); 
					}
					

	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/clients');
			 			
			 						
						
			

    }

    /****************************  END UPDATE DATA ****************************/




    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$current_status = $status == 'true' ? 'Yes' : 'No';
			
			$data = array('status'=>$current_status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'banners',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/

	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function roleassign() {

        $this->data['viewData'] = $this->role_model->get_assigned_roles();    
		    
        $this->load->view('admin/roleassign',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function createRoleAssign() {	

     	$this->data['user_groups']   = $this->general_model->get_all('demo_groups');	

     	$this->data['roles']   = $this->general_model->get_all('demo_roles');	
		
        $this->load->view('admin/ajax/addRoleAssign',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/

    /****************************  START FETCH USER BY GROUP ******************/

     public function fetchUserByGroup() {	

     	$group_id = $this->input->post('group_id');	

     	$this->data['userlist'] = $this->general_model->get_data('demo_users',array('group_id'=>$group_id));
		
        $this->load->view('admin/ajax/userByGroup',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/

    /****************************  START INSERT FORM DATA ********************/
    public function submitAssignRoleData() {
          
	
		// field name, error message, validation rules
		$this->form_validation->set_rules('user_id', 'Username', 'trim|required');
	  	$this->form_validation->set_rules('role_id', 'role name', 'trim|required');

		 
		if($this->form_validation->run() == FALSE){
					redirect('admin/roles/roleassign');
		}
		else{ 			
			 
					$user_id=$this->input->post('user_id');

					$role_id=$this->input->post('role_id');
					
					$created_date = date('Y-m-d h:i:s');
			
					$created_by = $this->session->userdata('id');
						
					$data=array('user_id'=>$user_id,'role_id'=>$role_id,'created_date'=>$created_date,'created_by'=>$created_by);
						
					$this->general_model->add(TABLE_PREFIX.'users_role_assign',$data); 

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/roles/roleassign');				
			 	
			}		 
    }

    /****************************  END INSERT FORM DATA ************************/


     /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editRoleAssign() {
        
        $id = $this->input->post('id');

        $where=array('demo_users_role_assign.id' => $id);
        
        $this->data['roleassign']= $this->role_model->get_assigned_roles($where); 

        $this->data['userlist'] = $this->general_model->get_data('demo_users',array('group_id'=>$this->data['roleassign']->group_id));

        $this->data['user_groups']   = $this->general_model->get_all(TABLE_PREFIX.'groups');	

     	$this->data['roles'] = $this->general_model->get_all(TABLE_PREFIX.'roles');	 
     
            
        $this->load->view('admin/ajax/editRoleAssign',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/

	/****************************  INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

	private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FILE_UPLOAD_PATH.'banners/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = 'img_'.time();
		// $config['max_size']     = '100';
		// $config['max_width'] = '1024';
		// $config['max_height'] = '768';

        return $config;
    }
    
    /****************************  END INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

}

/* End of file banners.php */
/* Location: ./application/controllers/admin/banners.php */