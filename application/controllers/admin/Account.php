<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }

        $this->load->model('admin/account_model');
        $this->load->library('Ajax_pagination');
        $this->load->helper('custom_helper');
        if($this->session->has_userdata('perPage')){
			$this->perPage = $this->session->userdata('perPage');
        }else{
        	$this->perPage = 10;
        }
        
    }

    function setPerPage(){
    	$perPage = $this->input->post('perPage');
    	$this->session->set_userdata('perPage', $perPage);
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

    	$data = array();
        
        //total rows count
        $totalRec = count($this->account_model->get_all());
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/clients/ajaxAccountData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->account_model->get_all(array('limit'=>$this->perPage));

        //$this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/account',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN AJAX PAGINATION ******************/

    function ajaxAccountData($page=''){
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->account_model->get_all()); 
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/account/ajaxAccountData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->account_model->get_all(array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('admin/ajax/ajax_account_data', $this->data, false);
    }
	
	/****************************  END AJAX PAGINATION ******************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

		$this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
			
        $this->load->view('admin/addAccount',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/

    /****************************  START FETCHING CLIENT DATA **************************/
	
	public function fetchClientAccounts(){
	
		$id = $this->input->post('client_id');			

		$this->data['area_list']   = $this->general_model->get_data(TABLE_PREFIX.'area');

        $this->load->view('admin/ajax/fetchClientAccounts',$this->data);
		
	}

	/****************************  END FETCHING CLIENT DATA ***************************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {

    				$client_id = $this->input->post('client_id');
    				$client_data = $this->general_model->get_row(TABLE_PREFIX.'users',array('id'=>$client_id)); 
    				$client_document_data = $this->general_model->get_row(TABLE_PREFIX.'client_document',array('client_id'=>$client_id),'desc'); 
    				
    				$total_accounts   = $this->general_model->get_num_rows(TABLE_PREFIX.'accounts',array('client_id'=>$client_id));
    				$account_name = $client_data->full_name.($total_accounts+1);
    				$area_id = $this->input->post('area_id');
					
					if($area_id=='new'){
						$data=array(	
							'area_name'=>$this->input->post('area_name')
						);
						
						$area_id = $this->general_model->add(TABLE_PREFIX.'area',$data); 
					}

    				/*------Insert Client account information-------*/
					
					$document_data =$this->input->post('document_data');

					foreach ($document_data as $key => $value) {
					
						$data=array(	
								'account_name'=>$account_name,
								'client_id'=>$client_id,
								'document_id'=>$value,
								'area_id'=>$area_id,
								'loan_amount '=>$this->input->post('loan_amount'),
								'account_duration'=>$this->input->post('account_duration'),
								'start_date'=>$this->input->post('start_date'),
								'end_date'=>$this->input->post('end_date'),
								'interest_rate'=>$this->input->post('interest_rate'),
								'installment_amount'=>$this->input->post('installment_amount')
						);

						$account_id = $this->general_model->add(TABLE_PREFIX.'accounts',$data); 
					
					}

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/account');				
	 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $this->data['client_id']= $id;
        $this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));

        $this->data['drawee_list']   = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('client_id'=>$id));

		$this->data['document_list']   = $this->general_model->get_data(TABLE_PREFIX.'client_document',array('client_id'=>$id));
    
    	$this->data['total_accounts']   = $this->general_model->get_num_rows(TABLE_PREFIX.'accounts',array('client_id'=>$id));

    	$this->data['area_list']   = $this->general_model->get_data(TABLE_PREFIX.'area');
            
        $this->load->view('admin/editAccount',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData()
    {		
					$client_id = $this->input->post('client_id');	

    				$account_data = $this->general_model->get_row(TABLE_PREFIX.'accounts',array('client_id'=>$client_id),'desc'); 

    				$area_id = $this->input->post('area_id');
					
					if($area_id=='new'){
						$data=array(	
							'area_name'=>$this->input->post('area_name')
						);
						
						$area_id = $this->general_model->add(TABLE_PREFIX.'area',$data); 
					}

    				/*------Update Client account information-------*/

					$data=array(	
							'area_id'=>$area_id,
							'loan_number'=>$this->input->post('loan_number'),
							'loan_amount '=>$this->input->post('loan_amount'),
							'no_of_days'=>$this->input->post('no_of_days'),
							'account_open_date'=>$this->input->post('account_open_date'),
							'account_close_date'=>$this->input->post('account_close_date'),
							'interest_rate'=>$this->input->post('interest_rate')
					);

					$this->general_model->update(TABLE_PREFIX.'accounts',$data,array('id'=>$account_data->id)); 


	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/account');		

    }

    /****************************  END UPDATE DATA ****************************/

    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHECKING CLIENT DOCUMENTS **************************/
	
	public function checkDocuments(){
	
		$client_id = $this->input->post('client_id');	
		$this->data['loan_amount'] = $this->input->post('loan_amount');			

		$this->data['document_list']   = $this->general_model->get_data(TABLE_PREFIX.'client_document',array('client_id'=>$client_id,'status'=>'Yes'));

        $this->load->view('admin/ajax/fetchAccountNotify',$this->data);
		
	}

	/****************************  END CHECKING CLIENT DOCUMENTS ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$current_status = $status == 'true' ? 'Yes' : 'No';
			
			$data = array('status'=>$current_status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'users',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/

	public function addNewAccount(){
	
		$this->load->view('admin/ajax/addNewAccount');
		
	}

}

/* End of file Clients.php */
/* Location: ./application/controllers/admin/Clients.php */