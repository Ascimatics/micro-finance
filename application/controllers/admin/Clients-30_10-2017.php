<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }

        $this->load->model('admin/client_model');
        $this->load->library('Ajax_pagination');
        if($this->session->has_userdata('perPage')){
			$this->perPage = $this->session->userdata('perPage');
        }else{
        	$this->perPage = 10;
        }
        
    }

    function setPerPage(){
    	$perPage = $this->input->post('perPage');
    	$this->session->set_userdata('perPage', $perPage);
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

    	$data = array();
        
        //total rows count
        $totalRec = count($this->client_model->get_all());
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/clients/ajaxClientData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->client_model->get_all(array('limit'=>$this->perPage));

        //$this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/client',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN AJAX PAGINATION ******************/

    function ajaxClientData($page=''){
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->client_model->get_all()); 
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/clients/ajaxClientData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->client_model->get_all(array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('admin/ajax/ajax_client_data', $this->data, false);
    }
	
	/****************************  END AJAX PAGINATION ******************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

     	$this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));

     	$this->data['broker_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'3'));		
     	$this->data['collection_man_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'5'));

     	$this->data['area_list']   = $this->general_model->get_data(TABLE_PREFIX.'area');
			
        $this->load->view('admin/addClient',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {


    				/*------Insert Client information-------*/
					$area_id = $this->input->post('area_id');
					
					if($area_id=='new'){
						$data=array(	
							'area_name'=>$this->input->post('area_name')
						);
						
						$area_id = $this->general_model->add(TABLE_PREFIX.'area',$data); 
					}

					$broker_ids = $this->input->post('broker_ids');
				
					if (in_array('new', $broker_ids)) {
						$data=array(	
							'full_name'=>$this->input->post('broker_full_name'),
							'work_address'=>$this->input->post('broker_work_address'),
							'residence_address'=>$this->input->post('broker_residence_address'),
							'work_telephone'=>$this->input->post('broker_work_telephone'),
							'mobile1'=>$this->input->post('broker_mobile1'),
							'mobile2'=>$this->input->post('broker_mobile2'),
							'group_id'=>'3'
						);
							
						$broker_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 

						array_push($broker_ids,$broker_id);
					}	

					$data=array(	
						'full_name'=>$this->input->post('full_name'),
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'work_telephone'=>$this->input->post('work_telephone'),
						'mobile1'=>$this->input->post('mobile1'),
						'mobile2'=>$this->input->post('mobile2'),
						'introduced_by'=>$this->input->post('introduced_by'),
						'area_id'=>$area_id,
						'broker_ids'=>implode(",",$broker_ids),
						'group_id'=>'4'
					);
						
					$client_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 

					/*------Insert Client account information-------*/

					$data=array(	
							'client_id'=>$client_id,
							'account_name'=>$this->input->post('account_name'),
							'designation'=>$this->input->post('account_designation'),
							'cheque_received_status'=>$this->input->post('cheque_received_status'),
							'loan_number'=>$this->input->post('loan_number'),
							'loan_amount '=>$this->input->post('loan_amount'),
							'no_of_days'=>$this->input->post('no_of_days'),
							'account_open_date'=>$this->input->post('account_open_date'),
							'account_close_date'=>$this->input->post('account_close_date'),
							'interest_rate'=>$this->input->post('interest_rate')
					);

					$account_id = $this->general_model->add(TABLE_PREFIX.'accounts',$data); 

					/*------Insert Cheque information-------*/

					$cheque_received_status = $this->input->post('cheque_received_status');

					if($cheque_received_status=='Yes'){

						$cheque_no = $this->input->post('cheque_no');
						$cheque_amount = $this->input->post('cheque_amount');

						foreach ($cheque_amount as $key => $value) {
							$data=array(	
								'cheque_amount'=>$value,
								'cheque_no'=>$cheque_no[$key],
								'account_id'=>$account_id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'cheque_details',$data); 
						}

					}

					/*------Insert Loan drawee information-------*/

					$drawee_name = $this->input->post('drawee_name');
					$drawee_designation = $this->input->post('drawee_designation');

					foreach ($drawee_name as $key => $value) {
						$data=array(	
							'drawee_name'=>$value,
							'drawee_designation'=>$drawee_designation[$key],
							'account_id'=>$account_id,
							'client_id'=>$client_id
						);
						$this->general_model->add(TABLE_PREFIX.'draw_details',$data); 
					}


					/*------Insert Loan guarantor information-------*/

					if($this->input->post('same_as_draw')=='yes'){

						$draw_details = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('client_id'=>$client_id,'account_id'=>$account_id));
						
						foreach ($draw_details as $value) {
							$data=array(	
								'guarantor_name'=>$value->drawee_name,
								'guarantor_designation'=>$value->drawee_designation,
								'draw_id'=>$value->id,
								'account_id'=>$account_id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor',$data);
						}
					}else{

						$guarantor_name = $this->input->post('guarantor_name');
						$guarantor_designation = $this->input->post('guarantor_designation');
						$blood_relation = $this->input->post('blood_relation');
						$guarantor_address = $this->input->post('guarantor_address');
						$tel1 = $this->input->post('tel1');
						$tel2 = $this->input->post('tel2');

						foreach ($guarantor_name as $key => $value) {
							$data=array(	
								'guarantor_name'=>$value,
								'guarantor_designation'=>$guarantor_designation[$key],
								'blood_relation'=>$blood_relation[$key],
								'guarantor_address'=>$guarantor_address[$key],
								'tel1'=>$tel1[$key],
								'tel2'=>$tel2[$key],
								'draw_id'=>0,
								'account_id'=>$account_id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor',$data); 
						} 
					}

				/*------Insert Client Document information-------*/



					$document_type = $this->input->post('document_type');
					

					$purpose = $this->input->post('purpose');
					$document_value = $this->input->post('document_value');
					$doc_received_status = $this->input->post('doc_received_status');
					$received_by = $this->input->post('received_by');
					$validity_status = $this->input->post('validity_status');
					$expiry_date = $this->input->post('expiry_date');
					$return_status = $this->input->post('return_status');
					$return_date = $this->input->post('return_date');

					if($doc_received_status!='Yes'){
							$doc_received_status="No";
							$received_by="";
						}

						if($validity_status!='Yes'){
							$validity_status="No";
						}

						if($return_status!='Yes'){
							$return_status="No";
							$return_date="";
						}
						
					

					$data=array(	
								'client_id'=>$id,
								'account_id'=>$this->data['account_data']->id,
								'purpose'=>$purpose,
								'document_value'=>$document_value,
								'doc_received_status'=>$doc_received_status,
								'received_by'=>$received_by,
								'validity_status'=>$validity_status,
								'expiry_date'=>$expiry_date,
								'return_status'=>$return_status,
								'return_date'=>$return_date
								);
	
					$document_id = $this->general_model->add(TABLE_PREFIX.'client_document',$data); 

					
					

					foreach ($document_type as $key => $value) {

						$filepath="";
						//Get the temp file path
					  	$tmpFilePath = $_FILES['client_document']['tmp_name'][$key];

					  	//Make sure we have a filepath
					  	if ($tmpFilePath != ""){
					    
					    	//Setup our new file path
					    	$newFilePath = $_SERVER['DOCUMENT_ROOT'].'/microfinance/uploads/images/' . $_FILES['client_document']['name'][$key];

					    	//Upload the file into the temp dir
					    	move_uploaded_file($tmpFilePath, $newFilePath);
					    	$filepath = 'uploads/images/'.$_FILES['client_document']['name'][$key];
					    }

						

						$data=array(	
							'client_id'=>$client_id,
							'client_document_id'=>$document_id,
							'account_id'=>$account_id,
							'document_type'=>$value,
							'document_path'=>$filepath
						);
						$this->general_model->add(TABLE_PREFIX.'client_document_details',$data); 
					}

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/clients');				
			 	
				 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_row(TABLE_PREFIX.'users',$where);  
        $this->data['account_data']= $this->general_model->get_row(TABLE_PREFIX.'accounts',array('client_id'=>$id)); 

        $this->data['cheque_details']   = $this->general_model->get_data(TABLE_PREFIX.'cheque_details',array('client_id'=>$id,'account_id'=>$this->data['account_data']->id)); 

        $this->data['area_list']   = $this->general_model->get_data(TABLE_PREFIX.'area');

        $this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));

        $this->data['broker_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'3'));		
     	$this->data['collection_man_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'5'));

     	$this->data['drawee_list']   = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('account_id'=>$this->data['account_data']->id));

        $this->data['guarantor_list']   = $this->general_model->get_data(TABLE_PREFIX.'guarantor',array('account_id'=>$this->data['account_data']->id));

        $this->data['document_list']   = $this->general_model->get_row(TABLE_PREFIX.'client_document',array('client_id'=>$id,'account_id'=>$this->data['account_data']->id));

        $this->data['document_details_list']   = $this->general_model->get_data(TABLE_PREFIX.'client_document_details',array('client_id'=>$id,'account_id'=>$this->data['account_data']->id));
    
            
        $this->load->view('admin/editClient',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {		

					/*------Update Client information-------*/

					$area_id = $this->input->post('area_id');
					
					if($area_id=='new'){
						$data=array(	
							'area_name'=>$this->input->post('area_name')
						);
						
						$area_id = $this->general_model->add(TABLE_PREFIX.'area',$data); 
					}

					$broker_ids = $this->input->post('broker_ids');
				
					if (in_array('new', $broker_ids)) {
						$data=array(	
							'full_name'=>$this->input->post('broker_full_name'),
							'work_address'=>$this->input->post('broker_work_address'),
							'residence_address'=>$this->input->post('broker_residence_address'),
							'work_telephone'=>$this->input->post('broker_work_telephone'),
							'mobile1'=>$this->input->post('broker_mobile1'),
							'mobile2'=>$this->input->post('broker_mobile2'),
							'group_id'=>'3'
						);
							
						$broker_id = $this->general_model->add(TABLE_PREFIX.'users',$data); 
						array_push($broker_ids,$broker_id);
					}

					$data=array(	
						'full_name'=>$this->input->post('full_name'),
						'work_address'=>$this->input->post('work_address'),
						'residence_address'=>$this->input->post('residence_address'),
						'work_telephone'=>$this->input->post('work_telephone'),
						'mobile1'=>$this->input->post('mobile1'),
						'mobile2'=>$this->input->post('mobile2'),
						'introduced_by'=>$this->input->post('introduced_by'),
						'area_id'=>$area_id,
						'broker_ids'=>implode(",",$broker_ids)
					);
						
					$this->general_model->update(TABLE_PREFIX.'users',$data,array('id'=>$id)); 

					/*------Update Client account information-------*/
					 
					$data=array(	
							'account_name'=>$this->input->post('account_name'),
							'cheque_received_status'=>$this->input->post('cheque_received_status'),
							'designation'=>$this->input->post('account_designation'),
							'loan_number'=>$this->input->post('loan_number'),
							'loan_amount '=>$this->input->post('loan_amount'),
							'no_of_days'=>$this->input->post('no_of_days'),
							'account_open_date'=>$this->input->post('account_open_date'),
							'account_close_date'=>$this->input->post('account_close_date'),
							'interest_rate'=>$this->input->post('interest_rate')
					);

					$this->general_model->update(TABLE_PREFIX.'accounts',$data,array('client_id'=>$id)); 

					$this->data['account_data']= $this->general_model->get_row(TABLE_PREFIX.'accounts',array('client_id'=>$id));


					/*------Insert Cheque information-------*/
					
					$cheque_received_status = $this->input->post('cheque_received_status');

					if($cheque_received_status=='Yes'){
						
						$this->general_model->delete(TABLE_PREFIX.'cheque_details',array('account_id'=>$this->data['account_data']->id,'client_id'=>$id),'single');

						$cheque_no = $this->input->post('cheque_no');
						$cheque_amount = $this->input->post('cheque_amount');

						foreach ($cheque_amount as $key => $value) {
							$data=array(	
								'cheque_amount'=>$value,
								'cheque_no'=>$cheque_no[$key],
								'account_id'=>$this->data['account_data']->id,
								'client_id'=>$id
							);
							$this->general_model->add(TABLE_PREFIX.'cheque_details',$data); 
						}

					}


					/*------Insert Account drawee information-------*/
					

			$this->general_model->delete(TABLE_PREFIX.'draw_details',array('account_id'=>$this->data['account_data']->id),'single');

					$drawee_name = $this->input->post('drawee_name');
					$drawee_designation = $this->input->post('drawee_designation');

					foreach ($drawee_name as $key => $value) {
						$data=array(	
							'drawee_name'=>$value,
							'drawee_designation'=>$drawee_designation[$key],
							'account_id'=>$this->data['account_data']->id,
							'client_id'=>$id
						);
						$this->general_model->add(TABLE_PREFIX.'draw_details',$data); 
					}
					
					
				
					/*------Insert Account guarantor information-------*/
					$this->general_model->delete(TABLE_PREFIX.'guarantor',array('account_id'=>$this->data['account_data']->id),'single');

					if($this->input->post('same_as_draw')=='yes'){
						
						$draw_details = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('client_id'=>$id,'account_id'=>$this->data['account_data']->id));
						
						foreach ($draw_details as $value) {
							$data=array(	
								'guarantor_name'=>$value->drawee_name,
								'guarantor_designation'=>$value->drawee_designation,
								'draw_id'=>$value->id,
								'account_id'=>$this->data['account_data']->id,
								'client_id'=>$id
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor',$data);
						}
					}else{

					$guarantor_name = $this->input->post('guarantor_name');
					$guarantor_designation = $this->input->post('guarantor_designation');
					$blood_relation = $this->input->post('blood_relation');
					$guarantor_address = $this->input->post('guarantor_address');
					$tel1 = $this->input->post('tel1');
					$tel2 = $this->input->post('tel2');

					foreach ($guarantor_name as $key => $value) {
						$data=array(	
							'guarantor_name'=>$value,
							'guarantor_designation'=>$guarantor_designation[$key],
							'blood_relation'=>$blood_relation[$key],
							'guarantor_address'=>$guarantor_address[$key],
							'tel1'=>$tel1[$key],
							'tel2'=>$tel2[$key],
							'draw_id'=>0,
							'account_id'=>$this->data['account_data']->id,
							'client_id'=>$id
						);
						$this->general_model->add(TABLE_PREFIX.'guarantor',$data); 
					} 
				}

				/*------Insert Client Document information-------*/

					$this->general_model->delete(TABLE_PREFIX.'client_document',array('client_id'=>$id),'single');

					$document_type = $this->input->post('document_type');
					$purpose = $this->input->post('purpose');
					$document_value = $this->input->post('document_value');
					$doc_received_status = $this->input->post('doc_received_status');
					$received_by = $this->input->post('received_by');
					$validity_status = $this->input->post('validity_status');
					$expiry_date = $this->input->post('expiry_date');
					$return_status = $this->input->post('return_status');
					$return_date = $this->input->post('return_date');

					

						if($doc_received_status!='Yes'){
							$doc_received_status="No";
							$received_by="";
						}

						if($validity_status!='Yes'){
							$validity_status="No";
						}

						if($return_status!='Yes'){
							$return_status="No";
							$return_date="";
						}
						
					

					$data=array(	
								'client_id'=>$id,
								'account_id'=>$this->data['account_data']->id,
								'purpose'=>$purpose,
								'document_value'=>$document_value,
								'doc_received_status'=>$doc_received_status,
								'received_by'=>$received_by,
								'validity_status'=>$validity_status,
								'expiry_date'=>$expiry_date,
								'return_status'=>$return_status,
								'return_date'=>$return_date
								);
	
					$document_id = $this->general_model->add(TABLE_PREFIX.'client_document',$data); 

					$this->general_model->delete(TABLE_PREFIX.'client_document_details',array('client_id'=>$id),'single');
					
					foreach ($document_type as $key => $value) {
						
						//Get the temp file path
					  	$tmpFilePath = $_FILES['client_document']['tmp_name'][$key];

						//Make sure we have a filepath
					  	if ($tmpFilePath != ""){
					    
					    	//Setup our new file path
					    	$newFilePath = $_SERVER['DOCUMENT_ROOT'].'/microfinance/uploads/images/' . $_FILES['client_document']['name'][$key];

					    	//Upload the file into the temp dir
					    	move_uploaded_file($tmpFilePath, $newFilePath);
					    	$filepath = 'uploads/images/'.$_FILES['client_document']['name'][$key];

						    	$data=array(	
								'client_id'=>$id,
								'client_document_id'=>$document_id,
								'account_id'=>$this->data['account_data']->id,
								'document_type'=>$value,
								'document_path'=>$filepath
								);

					  		}else{

						  		$data=array(	
								'client_id'=>$id,
								'client_document_id'=>$document_id,
								'account_id'=>$this->data['account_data']->id,
								'document_type'=>$value
								);
					  	}

						
						$this->general_model->add(TABLE_PREFIX.'client_document_details',$data); 
					}
					

	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/clients');		

    }

    /****************************  END UPDATE DATA ****************************/

    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$data = array('status'=>$status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'users',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/


    /****************************  START ADDING MORE DOCUMENT FIELDS **************************/
	
	public function addMoreDocument(){
	
		$this->load->view('admin/ajax/addMoreDocument');
		
	}

	public function addMoreGuarantor(){
	
		$this->load->view('admin/ajax/addMoreGuarantor');
		
	}

	public function addMoreBroker(){
	
		$this->load->view('admin/ajax/addMoreBroker');
		
	}

	public function addCheque(){
	
		$this->load->view('admin/ajax/addCheque');
		
	}

	public function addMoreCheque(){
	
		$this->load->view('admin/ajax/addMoreCheque');
		
	}

	/****************************  END MORE DOCUMENT FIELDS ***************************/

	/****************************  INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

	private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FILE_UPLOAD_PATH.'banners/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|docx|doc|txt';
		$config['file_name'] = 'img_'.time();
		// $config['max_size']     = '100';
		// $config['max_width'] = '1024';
		// $config['max_height'] = '768';

        return $config;
    }
    
    /****************************  END INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

}

/* End of file Clients.php */
/* Location: ./application/controllers/admin/Clients.php */