<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Documents extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }

        $this->load->model('admin/client_model');
        $this->load->model('admin/document_model');
        $this->load->library('Ajax_pagination');
        $this->load->helper('custom_helper');
        if($this->session->has_userdata('perPage')){
			$this->perPage = $this->session->userdata('perPage');
        }else{
        	$this->perPage = 10;
        }
        
    }

    function setPerPage(){
    	$perPage = $this->input->post('perPage');
    	$this->session->set_userdata('perPage', $perPage);
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

    	$data = array();
        
        //total rows count
        $totalRec = count($this->document_model->get_all());
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/document/ajaxDocumentData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->document_model->get_all(array('limit'=>$this->perPage));

        //$this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/document',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN AJAX PAGINATION ******************/

    function ajaxDocumentData($page=''){
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->document_model->get_all()); 
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/clients/ajaxClientData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->document_model->get_all(array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('admin/ajax/ajax_document_data', $this->data, false);
    }
	
	/****************************  END AJAX PAGINATION ******************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	

     	$this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
			
        $this->load->view('admin/addDocument',$this->data);
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/

    /****************************  START FETCHING CLIENT DATA **************************/
	
	public function fetchClientDocuments(){
	
		$id = $this->input->post('client_id');			

		$this->data['drawee_list']   = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('client_id'=>$id));

		$this->data['document_list']   = $this->general_model->get_data(TABLE_PREFIX.'client_document',array('client_id'=>$id));

		$this->data['concern_designation']   = $this->general_model->get_data(TABLE_PREFIX.'concern_designation',array('status'=>'Yes'));

		$this->data['firm_types']   = $this->general_model->get_data(TABLE_PREFIX.'firm_type',array('status'=>'Yes'));

        $this->load->view('admin/ajax/fetchClientDocuments',$this->data);
		
	}

	/****************************  END FETCHING CLIENT DATA ***************************/

	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {

						
					$client_id = $this->input->post('client_id');

					/*------Insert Client Document information-------*/

					$document_type = $this->input->post('document_type');
					$purpose = $this->input->post('purpose');
					$document_value = $this->input->post('document_value');
					$doc_received_status = $this->input->post('doc_received_status');
					$cheque_received_status = $this->input->post('cheque_received_status');
					$expiry_date = $this->input->post('expiry_date');


						$filepath="";
						//Get the temp file path
					  	$tmpFilePath = $_FILES['client_document']['tmp_name'];

					  	//Make sure we have a filepath
					  	if ($tmpFilePath != ""){
					    
					    	//Setup our new file path
					    	$newFilePath = $_SERVER['DOCUMENT_ROOT'].'/microfinance/uploads/images/' . $_FILES['client_document']['name'];

					    	//Upload the file into the temp dir
					    	move_uploaded_file($tmpFilePath, $newFilePath);
					    	$filepath = 'uploads/images/'.$_FILES['client_document']['name'];
					    }


						if($cheque_received_status!='Yes'){
							$cheque_received_status="No";
						}

						if($doc_received_status!='Yes'){
							$doc_received_status="No";
						}

					$data=array(	
								'client_id'=>$client_id,
								'purpose'=>$purpose,
								'document_value'=>$document_value,
								'cheque_received_status'=>$cheque_received_status,
								'doc_received_status'=>$doc_received_status,
								'expiry_date'=>$expiry_date,
								'document_path'=>$filepath,
								'document_type'=>$document_type
								);
	
					$document_id = $this->general_model->add(TABLE_PREFIX.'client_document',$data); 


						$drawee_designation = $this->input->post('drawee_designation');
						$drawee_name = $this->input->post('drawee_name');
						
						foreach ($drawee_designation as $key=>$value) {
							$data=array(	
								'drawee_name'=>$drawee_name[$key],
								'drawee_designation'=>$value,
								'document_id'=>$document_id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'draw_details',$data);
						}

					/*------Insert Loan guarantor information-------*/

					if($this->input->post('same_as_draw')=='yes'){
						
						$data=array(
								'document_id'=>$document_id,
								'client_id'=>$client_id,
								'same_as_draw'=>'Yes'
						);

						$guarantor_id = $this->general_model->add(TABLE_PREFIX.'guarantor',$data);

						$draw_details = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('document_id'=>$document_id));

						foreach ($draw_details as $value) {
							$data=array(	
								'guarantor_name'=>$value->drawee_name,
								'guarantor_designation'=>$value->drawee_designation,
								'draw_id'=>$value->id,
								'client_id'=>$client_id,
								'guarantor_id'=>$guarantor_id
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor_list',$data);
						}
					}else{

						$guarantor_name = $this->input->post('guarantor_name');
						$guarantor_designation = $this->input->post('guarantor_designation');
						$blood_relation = $this->input->post('blood_relation');
						$guarantor_address = $this->input->post('guarantor_address');
						$guarantor_type = $this->input->post('guarantor_type');
						$firm_title = $this->input->post('firm_title');
						$firm_type_id = $this->input->post('firm_type_id');
						$tel1 = $this->input->post('tel1');
						$tel2 = $this->input->post('tel2');

						$data=array(
								'guarantor_type'=>$guarantor_type,
								'document_id'=>$document_id,
								'client_id'=>$client_id,
								'same_as_draw'=>'No',
								'firm_title'=>$firm_title,
								'firm_type_id'=>$firm_type_id
						);

						$guarantor_id = $this->general_model->add(TABLE_PREFIX.'guarantor',$data);

						foreach ($guarantor_name as $key => $value) {
							$data=array(	
								'guarantor_name'=>$value,
								'guarantor_designation'=>$guarantor_designation[$key],
								'blood_relation'=>$blood_relation[$key],
								'guarantor_address'=>$guarantor_address[$key],
								'tel1'=>$tel1[$key],
								'tel2'=>$tel2[$key],
								'guarantor_id'=>$guarantor_id,
								'client_id'=>$client_id
								
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor_list',$data); 
						} 
					}


					/*------Insert Cheque information-------*/

					if($cheque_received_status=='Yes'){

						$cheque_amount = $this->input->post('cheque_amount');

						foreach ($cheque_amount as $key => $value) {
							$data=array(	
								'cheque_amount'=>$value,
								'document_id'=>$document_id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'cheque_details',$data); 
						}

					}


					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/documents');				
			 	
				 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $this->data['document_id']= $id;
        $this->data['client_list']   = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));
    
            
        $this->load->view('admin/editDocument',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


	/****************************  START EDIT CLIENT DATA **************************/
	
	public function editClientDocuments(){
	
		$client_id = $this->input->post('client_id');	

		$id = $this->input->post('document_id');		

		$this->data['drawee_list']   = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('document_id'=>$id));

		$this->data['firm_types']   = $this->general_model->get_data(TABLE_PREFIX.'firm_type',array('status'=>'Yes'));

		$this->data['guarantor_details']   = $this->general_model->get_row(TABLE_PREFIX.'guarantor',array('document_id'=>$id));

		$this->data['concern_designation']   = $this->general_model->get_data(TABLE_PREFIX.'concern_designation',array('status'=>'Yes'));

		$this->data['document_details']   = $this->general_model->get_row(TABLE_PREFIX.'client_document',array('id'=>$id));

		$this->data['cheque_details'] = $this->general_model->get_data(TABLE_PREFIX."cheque_details",array('document_id'=>$id));

        $this->load->view('admin/ajax/editClientDocuments',$this->data);
		
	}

	/****************************  END EDIT CLIENT DATA ***************************/

    /****************************  START UPDATE DATA *************************/

   public function updateData($id) {
					
					$client_id = $this->input->post('client_id');

					/*------Update Client Document information-------*/
					
					
					$document_type = $this->input->post('document_type');
					$purpose = $this->input->post('purpose');
					$document_value = $this->input->post('document_value');
					$doc_received_status = $this->input->post('doc_received_status');
					$cheque_received_status = $this->input->post('cheque_received_status');
					$expiry_date = $this->input->post('expiry_date');


						$filepath="";
						//Get the temp file path
					  	$tmpFilePath = $_FILES['client_document']['tmp_name'];

					  	//Make sure we have a filepath
					  	if ($tmpFilePath != ""){
					    
					    	//Setup our new file path
					    	$newFilePath = $_SERVER['DOCUMENT_ROOT'].'/microfinance/uploads/images/' . $_FILES['client_document']['name'];

					    	//Upload the file into the temp dir
					    	move_uploaded_file($tmpFilePath, $newFilePath);
					    	$filepath = 'uploads/images/'.$_FILES['client_document']['name'];
					    }


						if($cheque_received_status!='Yes'){
							$cheque_received_status="No";
						}

						if($doc_received_status!='Yes'){
							$doc_received_status="No";
						}

						$data=array(	
								'purpose'=>$purpose,
								'document_value'=>$document_value,
								'cheque_received_status'=>$cheque_received_status,
								'doc_received_status'=>$doc_received_status,
								'expiry_date'=>$expiry_date,
								'document_path'=>$filepath,
								'document_type'=>$document_type
								);
	
					$document_id = $this->general_model->update(TABLE_PREFIX.'client_document',$data,array('id'=>$id)); 

					$this->general_model->delete(TABLE_PREFIX.'draw_details',array('document_id'=>$id),'single');

					$drawee_designation = $this->input->post('drawee_designation');
						$drawee_name = $this->input->post('drawee_name');
						
						foreach ($drawee_designation as $key=>$value) {
							$data=array(	
								'drawee_name'=>$drawee_name[$key],
								'drawee_designation'=>$value,
								'document_id'=>$id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'draw_details',$data);
						}

					/*------Insert Account guarantor information-------*/
					

					$guarantor_details   = $this->general_model->get_row(TABLE_PREFIX.'guarantor',array('document_id'=>$id));

					$this->general_model->delete(TABLE_PREFIX.'guarantor_list',array('guarantor_id'=>$guarantor_details->id),'single');

					if($this->input->post('same_as_draw')=='yes'){
						
						$data=array(
								'document_id'=>$id,
								'client_id'=>$client_id,
								'same_as_draw'=>'Yes',
								'firm_title'=>'',
								'guarantor_type'=>'',
								'firm_type_id'=>0
						);

						$guarantor_id = $this->general_model->update(TABLE_PREFIX.'guarantor',$data,array('id'=>$guarantor_details->id));

						$draw_details = $this->general_model->get_data(TABLE_PREFIX.'draw_details',array('document_id'=>$id));

						foreach ($draw_details as $value) {
							$data=array(	
								'guarantor_name'=>$value->drawee_name,
								'guarantor_designation'=>$value->drawee_designation,
								'draw_id'=>$value->id,
								'client_id'=>$client_id,
								'guarantor_id'=>$guarantor_details->id
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor_list',$data);
						}
					}else{

						$guarantor_name = $this->input->post('guarantor_name');
						$guarantor_designation = $this->input->post('guarantor_designation');
						$blood_relation = $this->input->post('blood_relation');
						$guarantor_address = $this->input->post('guarantor_address');
						$guarantor_type = $this->input->post('guarantor_type');
						$firm_title = $this->input->post('firm_title');
						$firm_type_id = $this->input->post('firm_type_id');
						$tel1 = $this->input->post('tel1');
						$tel2 = $this->input->post('tel2');

						$data=array(
								'guarantor_type'=>$guarantor_type,
								'document_id'=>$id,
								'client_id'=>$client_id,
								'same_as_draw'=>'No',
								'firm_title'=>$firm_title,
								'firm_type_id'=>$firm_type_id
						);

						$this->general_model->update(TABLE_PREFIX.'guarantor',$data,array('id'=>$guarantor_details->id));

						foreach ($guarantor_name as $key => $value) {
							$data=array(	
								'guarantor_name'=>$value,
								'guarantor_designation'=>$guarantor_designation[$key],
								'blood_relation'=>$blood_relation[$key],
								'guarantor_address'=>$guarantor_address[$key],
								'tel1'=>$tel1[$key],
								'tel2'=>$tel2[$key],
								'guarantor_id'=>$guarantor_details->id,
								'client_id'=>$client_id
								
							);
							$this->general_model->add(TABLE_PREFIX.'guarantor_list',$data); 
						} 
					}

					/*------Insert Cheque information-------*/
					
					$cheque_received_status = $this->input->post('cheque_received_status');

					if($cheque_received_status=='Yes'){
						
						$this->general_model->delete(TABLE_PREFIX.'cheque_details',array('document_id'=>$id),'single');

						$cheque_amount = $this->input->post('cheque_amount');

						foreach ($cheque_amount as $key => $value) {
							$data=array(	
								'cheque_amount'=>$value,
								'document_id'=>$client_document_data->id,
								'client_id'=>$client_id
							);
							$this->general_model->add(TABLE_PREFIX.'cheque_details',$data); 
						}

					}


					$this->session->set_flashdata('message', 'success|Data updated successfully.');
			
			 		redirect('admin/documents');				
			 	
				 
    }

    /****************************  END UPDATE DATA ****************************/

    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$data = array('status'=>$status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'users',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/


    /****************************  START ADDING MORE DOCUMENT FIELDS **************************/
	
	public function addMoreDocument(){
	
		$this->load->view('admin/ajax/addMoreDocument');
		
	}

	public function addMoreGuarantor(){
	
		$this->load->view('admin/ajax/addMoreGuarantor');
		
	}

	public function addGuarantor(){
	
		$this->load->view('admin/ajax/addGuarantor');
		
	}


	public function addMoreBroker(){
	
		$this->load->view('admin/ajax/addMoreBroker');
		
	}

	public function addCheque(){
	
		$this->load->view('admin/ajax/addCheque');
		
	}

	public function addMoreCheque(){
	
		$this->load->view('admin/ajax/addMoreCheque');
		
	}

	public function addNewDocument(){
	
		$this->load->view('admin/ajax/addNewDocument');
		
	}

	/****************************  END MORE DOCUMENT FIELDS ***************************/

	/****************************  INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

	private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FILE_UPLOAD_PATH.'banners/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|docx|doc|txt';
		$config['file_name'] = 'img_'.time();
		// $config['max_size']     = '100';
		// $config['max_width'] = '1024';
		// $config['max_height'] = '768';

        return $config;
    }
    
    /****************************  END INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

}

/* End of file Clients.php */
/* Location: ./application/controllers/admin/Clients.php */