<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tools extends CI_Controller {

    public function __construct() {
        parent::__construct();
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    /**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {
        
        $this->load->view('admin/changepass');
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN EDIT PASSWORD WITH DATA *************/

    public function changePassword() {
        
        $this->load->view('admin/changepass');
    }

    /****************************  END OPEN EDIT PASSWORD WITH DATA ***************/

    /****************************  START UPDATE PASSWORD *************************/

    public function updatePassword() {

        // field name, error message, validation rules
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('confirm_password', 'Repeat Password', 'trim|required|matches[new_password]');

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('message', 'error|Error in updating data.');

            redirect('admin/tools/changePassword');
        } 
        else 
        {
            $old_password=$this->input->post('old_password');

            $result = $this->general_model->get_num_rows(TABLE_PREFIX.'users',array('password'=>md5($old_password)));

            if($result)
            {
                $new_password=$this->input->post('confirm_password');
                
                $data=array('password'=>md5($new_password));

                $where=array('password'=>md5($old_password));
              
                $this->general_model->update(TABLE_PREFIX.'users',$data,$where); 

                $this->session->set_flashdata('message', 'success|Password updated successfully');          
            }
            else
            {
                $this->session->set_flashdata('message', 'error|Wrong Old Password');
            }

            redirect('admin/tools/changePassword');
        }
    }

    /****************************  END UPDATE PASSWORD ****************************/


    /****************************  START OPEN EDIT SETTINGS WITH DATA *************/

    public function changeSettings() {
        
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'settings');
        $this->load->view('admin/settings',$this->data);
    }

    /****************************  END OPEN EDIT SETTINGS WITH DATA ***************/

    /****************************  START UPDATE SETTINGS *************************/

    public function updateSettings() {
                
        $settings=$this->input->post();

        foreach ($settings as $key => $value) {
                    
            $where=array('config_type'=>$key);

            $data=array('config_val'=>$value);
              
            $this->general_model->update(TABLE_PREFIX.'settings',$data,$where); 
        }



        $this->session->set_flashdata('message', 'success|Settings updated successfully');          

        redirect('admin/tools/changeSettings');
    }

    /****************************  END UPDATE SETTINGS ****************************/


}
