<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banners extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
        $this->load->helper("custom_helper");
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'banners');    
		    
        $this->load->view('admin/banner',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/


    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {			
		
        $this->load->view('admin/ajax/addBanner');
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {
          
	
		// field name, error message, validation rules
	  	$this->form_validation->set_rules('banner_title', 'banner Title', 'trim|required');

		 
		if($this->form_validation->run() == FALSE){
					redirect('admin/banners');
		}
		else{ 			
				$this->load->library('upload');

                $this->upload->initialize($this->set_upload_options());					
				
				if (!$this->upload->do_upload('banner_image')){
						
					$error_msg = $this->upload->display_errors(); // uploading failed. $error will holds the errors.
						
					$this->session->set_flashdata('message', 'error|'.$error_msg); 

					redirect('admin/banners');
						
				}
				else{
						
					$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

					resize_image(FILE_UPLOAD_PATH.'banners/'.$upload_data['file_name'],FILE_UPLOAD_PATH.'banners/thumb','200','200');//RESIZE uploded image by calling custom helper function

					$file_path = 'banners/'.$upload_data['file_name'];
			 
					$banner_title=$this->input->post('banner_title');
						
					$data=array('banner_title'=>$banner_title,'banner_image'=>$file_path);
						
					$this->general_model->add(TABLE_PREFIX.'banners',$data); 

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/banners');				
			 	}
			}		 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData() {

		$where = array('id'=>$this->input->post('id'));

        $this->data['data'] = $this->general_model->get_data(TABLE_PREFIX.'banners',$where);    
		    
        $result = $this->load->view('admin/ajax/editBanner',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {

    	// field name, error message, validation rules
	  	$this->form_validation->set_rules('banner_title', 'banner Title', 'trim|required');

		 
		if($this->form_validation->run() == FALSE){

					$this->session->set_flashdata('message', 'error|Error in updating data.');
					
					redirect('admin/banners');
		}
		else{ 			
				$this->load->library('upload');

                $this->upload->initialize($this->set_upload_options());					
				
				if (!$this->upload->do_upload('banner_image')){
						
					$error_msg = $this->upload->display_errors(); // uploading failed. $error will holds the errors.
						
					$this->session->set_flashdata('message', 'error|'.$error_msg); 
						
					$file_path = '';

					$data = array('banner_title' =>	$this->input->post('banner_title'));

					$where = array('id'	=>	$id);

	    			$this->general_model->update(TABLE_PREFIX.'banners',$data,$where);
			
			 		redirect('admin/banners');
						
					}
					else{
						
					$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
						
					resize_image(FILE_UPLOAD_PATH.'banners/'.$upload_data['file_name'],FILE_UPLOAD_PATH.'banners/thumb','200','200');//RESIZE uploded image by calling custom helper function

					@unlink(FILE_UPLOAD_PATH.$this->input->post('old_file'));

					$file_path = 'banners/'.$upload_data['file_name'];

					$data = array('banner_title'	=>	$this->input->post('banner_title'),
									  'banner_image'	=>	$file_path);

					$where = array('id'	=>	$id);

	    			$this->general_model->update(TABLE_PREFIX.'banners',$data,$where);

	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/banners');
			 			
			 			}					
						
			}

    }

    /****************************  END UPDATE DATA ****************************/


    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$current_status = $status == 'true' ? 'Yes' : 'No';
			
			$data = array('status'=>$current_status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'banners',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/

	/****************************  INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

	private function set_upload_options()
    {   
        //upload an image options
        $config = array();
        $config['upload_path'] = FILE_UPLOAD_PATH.'banners/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = 'img_'.time();
		// $config['max_size']     = '100';
		// $config['max_width'] = '1024';
		// $config['max_height'] = '768';

        return $config;
    }
    
    /****************************  END INITIALIZE CONFIG DATA FOR FILE UPLOAD ***************************/

}

/* End of file banners.php */
/* Location: ./application/controllers/admin/banners.php */