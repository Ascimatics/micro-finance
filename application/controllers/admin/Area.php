<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Area extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
        $this->load->model('admin/area_model');
        $this->load->library('Ajax_pagination');
        if($this->session->has_userdata('perPage')){
			$this->perPage = $this->session->userdata('perPage');
        }else{
        	$this->perPage = 10;
        }
        
    }

    function setPerPage(){
    	$perPage = $this->input->post('perPage');
    	$this->session->set_userdata('perPage', $perPage);
    }

 	/**************************  START FETCH OR VIEW FORM DATA ***************/

    public function index() {

    	$data = array();
        
        //total rows count
        $totalRec = count($this->general_model->get_all(TABLE_PREFIX.'area'));
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/area/ajaxAreaData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'area',array(),array('limit'=>$this->perPage));

        //$this->data['viewData'] = $this->general_model->get_data(TABLE_PREFIX.'users',array('group_id'=>'4'));    
		    
        $this->load->view('admin/area',$this->data);
    }

    /****************************  END FETCH OR VIEW FORM DATA ***************/

    /****************************  START OPEN AJAX PAGINATION ******************/

    function ajaxAreaData($page=''){
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //total rows count
        $totalRec = count($this->general_model->get_all(TABLE_PREFIX.'area')); 
        
        //pagination configuration
        $config['target']      = '#client';
        $config['base_url']    = base_url().'admin/area/ajaxAreaData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $this->data['viewData'] = $this->general_model->get_all(TABLE_PREFIX.'area',array(),array('start'=>$offset,'limit'=>$this->perPage));
        
        //load the view
        $this->load->view('admin/ajax/ajax_area_data', $this->data, false);
    }
	
	/****************************  END AJAX PAGINATION ******************/

    /****************************  START OPEN ADD FORM FILE ******************/

     public function addData() {	
			
        $this->load->view('admin/addArea');
	
    }

    /****************************  END OPEN ADD FORM FILE ********************/
	
   
    /****************************  START INSERT FORM DATA ********************/
    public function submitData() {

    				/*------Insert Client information-------*/

					$data=array(	
						'area_name'=>$this->input->post('area_name')
					);
						
					$area_id = $this->general_model->add(TABLE_PREFIX.'area',$data); 

					$this->session->set_flashdata('message', 'success|Data inserted successfully.');
			
			 		redirect('admin/area');				
			 	
			//}		 
    }

    /****************************  END INSERT FORM DATA ************************/


    /****************************  START OPEN EDIT FORM WITH DATA *************/

    public function editData($id) {
        
        $where=array('id' => $id);
        
        $this->data['data']= $this->general_model->get_row(TABLE_PREFIX.'area',$where);   

        $this->load->view('admin/editArea',$this->data);
    }

    /****************************  END OPEN EDIT FORM WITH DATA ***************/


    /****************************  START UPDATE DATA *************************/

    public function updateData($id)
    {		
				
					/*------Update Client information-------*/

					$data=array(	
						'area_name'=>$this->input->post('area_name')
					);
						
					$this->general_model->update(TABLE_PREFIX.'area',$data,array('id'=>$id)); 
					

	    			$this->session->set_flashdata('message', 'success|Data Updated successfully.');
			
			 		redirect('admin/area');		

    }

    /****************************  END UPDATE DATA ****************************/


    /****************************  START SEARCH DATA **************************/
    
    public function getSearchResult(){
    
        $searchStr = $this->input->post('searchStr');
        
        $this->data['viewData'] =$this->area_model->getSearchResult($searchStr); 

        $this->load->view('admin/ajax/ajax_area_data',$this->data);
        
    }

    /****************************  END SEARCH DATA ***************************/

    /****************************  START DELETE DATA **************************/
	
	public function deleteData(){
	
		$id = $this->input->post('id');
		
		$mode = $this->input->post('mode');
		
		if($mode=='single'){
		
		$where=array('id'=>$id);

		}
		else{
		
		$where=explode(",",$id);
				
		}
		
		$this->general_model->delete(TABLE_PREFIX.'banners',$where,$mode); 

		$this->session->set_flashdata('message', 'success|Data deleted successfully.');
		
	}

	/****************************  END DELETE DATA ***************************/

	/****************************  START CHANGE STATUS ***************************/
	
	public function changeStatus(){
	
			$status = $this->input->post('stat');
			$id = $mode=$this->input->post('id');
			
			$data = array('status'=>$status);

			$where = array('id'=>$id);
			
			$this->general_model->update(TABLE_PREFIX.'area',$data,$where);
	
	}

	/****************************  END CHANGE STATUS ***************************/

}

/* End of file Clients.php */
/* Location: ./application/controllers/admin/Clients.php */