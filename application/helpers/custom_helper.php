<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('resize_image'))
{
	function resize_image($source_file, $thumb_path,$width, $height) {

		$ci=& get_instance();

	    $ci->load->library('image_lib');

	    $config['image_library'] = 'gd2';
	    $config['source_image'] = $source_file;
	    $config['maintain_ratio'] = TRUE;
	    $config['create_thumb'] = TRUE;
	    $config['new_image'] = $thumb_path;
	    $config['width'] = $width;
	    $config['quality'] = 100;
	    $config['height'] = $height;

	    $ci->image_lib->initialize($config);
	    $ci->image_lib->resize();

	}
}

if (!function_exists('get_data'))
{
	    function get_data($table = '',$where = array(),$fieldname = '*')
        {
        	$ci=& get_instance();

	    	$ci->load->database('microfinance_db');

            $ci->db->select($fieldname);
            $ci->db->where($where);
            $q = $ci->db->get($table);
            if($q->num_rows() > 0)
            {
                return $q->result();
            }
            return array();
        }
}

if (!function_exists('get_row'))
{
	    function get_row($table,$where = array())
        {
        	$ci=& get_instance();

	    	$ci->load->database('microfinance_db');
			$ci->db->where($where);
            $q = $ci->db->get($table);
            if($q->num_rows() > 0)
            {
                return $q->row();
            }
            return array();
        }
}

if (!function_exists('available_amount'))
{
        function available_amount($table,$where = array())
        {
            $ci=& get_instance();

            $ci->load->database('microfinance_db');
            $ci->db->where($where);
            $q = $ci->db->get($table);
            if($q->num_rows() > 0)
            {
                return $q->row();
            }
            return array();
        }
}



