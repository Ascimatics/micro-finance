<?php class Account_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }


        // Return all records in the table
        public function get_all($params = array())
        {

                $this->db->select('demo_users.full_name,demo_accounts.*,demo_client_document.document_value')
                ->from('demo_accounts')
                ->join('demo_users','demo_users.id = demo_accounts.client_id','left')
                ->join('demo_client_document','demo_users.id = demo_client_document.client_id','left');

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            return ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
    
}
?>
