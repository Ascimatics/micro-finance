    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
     
    class General_model extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
        }
     
        // Return all records in the table
        public function get_all($table,$where = array(),$params = array())
        {
            $this->db->where($where);
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get($table);
            
            

            return ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
     
        // Return only one row
        public function get_row($table,$where = array(),$order_by='asc')
        {
            $this->db->where($where);
            $this->db->order_by('id',$order_by);
            $q = $this->db->get($table);
            if($q->num_rows() > 0)
            {
                return $q->row();
            }

            return false;
        }
     
        // Return one only field value
        public function get_data($table = '',$where = array(),$fieldname = '*')
        {
            $this->db->select($fieldname);
            $this->db->where($where);
            $q = $this->db->get($table);
            if($q->num_rows() > 0)
            {
                return $q->result();
            }
            return array();
        }

        // Return number of rows
        public function get_num_rows($table = '',$where = array(),$fieldname = '*')
        {
            $this->db->select($fieldname);
            $this->db->where($where);
            $q = $this->db->get($table);
            return $q->num_rows();
        }
     
        // Insert into table
        public function add($table,$data)
        {
            if($this->db->insert($table, $data)){
                return $this->db->insert_id();
            }
        }
     
        // Update data to table
        public function update($table,$data=array(),$where=array())
        {
            $this->db->where($where);
            $q = $this->db->update($table, $data);
            return $q;
        }
     
        // Delete record from table
        public function delete($table,$where,$mode){

            if($mode=='single'){
                $this->db->delete($table, $where); 
            }else{
                 $this->db->where_in('id', $where);
                $this->db->delete($table);
            
            }
        }
     
        // Check whether a value has duplicates in the database
        public function has_duplicate($value, $tabletocheck, $fieldtocheck)
        {
            $this->db->select($fieldtocheck);
            $this->db->where($fieldtocheck,$value);
            $result = $this->db->get($tabletocheck);
     
            if($result->num_rows() > 0) {
                return true;
            }
            else {
                return false;
            }
        }
     
        // Check whether the field has any reference from other table
        // Normally to check before delete a value that is a foreign key in another table
        public function has_child($value, $tabletocheck, $fieldtocheck)
        {
            $this->db->select($fieldtocheck);
            $this->db->where($fieldtocheck,$value);
            $result = $this->db->get($tabletocheck);
     
            if($result->num_rows() > 0) {
                return true;
            }
            else {
                return false;
            }
        }
     
        // Return an array to use as reference or dropdown selection
        public function get_ref($table,$key,$value,$dropdown=false)
        {
            $this->db->from($table);
            $this->db->order_by($value);
            $result = $this->db->get();
     
            $array = array();
            if ($dropdown)
                $array = array("" => "Please Select");
     
            if($result->num_rows() > 0) {
                foreach($result->result_array() as $row) {
                $array[$row[$key]] = $row[$value];
                }
            }
            return $array;
        }

    }