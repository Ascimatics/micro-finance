<?php class Document_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }


        // Return all records in the table
        public function get_all($params = array())
        {
            $this->db->select('demo_users.full_name,demo_client_document.id,demo_client_document.document_value,demo_client_document.purpose,demo_client_document.status')
                ->from('demo_client_document')
                ->join('demo_users','demo_users.id = demo_client_document.client_id','left')
                ->where('demo_users.group_id','4')
                ->group_by('demo_client_document.id','desc');

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            return ($query->num_rows() > 0)?$query->result_array():FALSE;
        }

        // Return all records in the table
        public function getDocumentsByClient($client_id)
        {
            $this->db->select('demo_client_document.*,demo_client_document_details.*,demo_guarantor.*,demo_cheque_details.*')
                ->from('demo_client_document')
                ->join('demo_client_document_details', 'demo_client_document.id = demo_client_document_details.document_id','left')
                ->join('demo_guarantor', 'demo_client_document.id = demo_guarantor.document_id','left')
                ->join('demo_cheque_details', 'demo_client_document.id = demo_cheque_details.document_id','left')
                ->where(array('demo_client_document.client_id'=>$client_id));

            $query = $this->db->get();

            return ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
    
}
?>
