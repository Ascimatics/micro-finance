<?php class CollectionMan_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        // Return all records in the table
        public function getSearchResult($searchStr)
        {
            $query =$this->db->select('demo_users.*,demo_area.area_name')
                ->from('demo_users')
                ->join('demo_area', 'demo_users.area_id = demo_area.id')
                ->where('demo_users.group_id','5')
                ->where("demo_area.area_name LIKE '%$searchStr%' or demo_users.full_name LIKE '%$searchStr%'")
                ->get();

            return ($query->num_rows() > 0)?$query->result_array():array();
        }

        // Return all records in the table
        public function get_all($params = array())
        {
            $this->db->select('demo_users.*,demo_area.area_name')
                ->from('demo_users')
                ->join('demo_area', 'demo_users.area_id = demo_area.id')
                ->where('demo_users.group_id','5');

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            return ($query->num_rows() > 0)?$query->result_array():FALSE;
        }

        function get_assigned_roles($where=array()) {
            $this->db->select('demo_users.*,demo_roles.*,demo_users_role_assign.id as assign_id,demo_users_role_assign.role_id,demo_users_role_assign.user_id')
                ->from('demo_users_role_assign')
                ->join('demo_users', 'demo_users.id = demo_users_role_assign.user_id')
                ->join('demo_roles', 'demo_roles.id = demo_users_role_assign.role_id')
                ->where($where);
                
                $query = $this->db->get ();
                
                if($query->num_rows() > 0)
                {
                    if(!empty($where)){
                        return $query->row();
                    }
                    return $query->result();
                }
                return array();
        }       
}
?>
