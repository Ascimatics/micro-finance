<?php class Role_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_roles()
        {
            $result_roles_group = $this->db->get_where(TABLE_PREFIX.'role_group',array('is_deleted'=>0))->result_array();
            for($i=0;$i<count($result_roles_group);$i++)
            {
                $query_roles = $this->db->get_where(TABLE_PREFIX.'role_features',array('role_group_id' => $result_roles_group[$i]['id']));
                if($query_roles->num_rows()>0)
                {
                    $result_roles = $query_roles->result_array();
                }
                $result_roles_group[$i]['groups_role'] = $result_roles;
            }
            
            return $result_roles_group;
        }

        public function grab_role($id)
        {
            $query_roles = $this->db->get_where(TABLE_PREFIX.'roles',array('id' => $id))->result_array();
            return $query_roles;
        }

        function get_assigned_roles($where=array()) {
            $this->db->select('demo_users.*,demo_roles.*,demo_users_role_assign.id as assign_id,demo_users_role_assign.role_id,demo_users_role_assign.user_id')
                ->from('demo_users_role_assign')
                ->join('demo_users', 'demo_users.id = demo_users_role_assign.user_id')
                ->join('demo_roles', 'demo_roles.id = demo_users_role_assign.role_id')
                ->where($where);
                
                $query = $this->db->get ();
                
                if($query->num_rows() > 0)
                {
                    if(!empty($where)){
                        return $query->row();
                    }
                    return $query->result();
                }
                return array();
        }       
}
?>
