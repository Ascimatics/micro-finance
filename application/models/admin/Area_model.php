<?php class Area_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }


        // Return all records in the table
        public function getSearchResult($searchStr)
        {
            $query =$this->db->select('demo_area.*')
                ->from('demo_area')
                ->where("area_name LIKE '%$searchStr%'")
                ->get();

            return ($query->num_rows() > 0)?$query->result_array():array();
        }
       
}
?>
