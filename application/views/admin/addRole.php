<?php $this->load->view("admin/header");
$page_id=$this->uri->segment(4);
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/roles"><i class="fa fa-dashboard"></i> Manage Role</a></li>
        <li class="active">create role</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Edit CMS</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form name="changepassword" id="main" class="form-horizontal" action="<?php echo base_url()?>admin/roles/submitData" method="post" novalidate>
              <div class="box-body">
                <div class="form-group <?php if(form_error("role_name")!=''){ ?>has-error<?php }?>">
                  <label for="role_name" class="col-sm-2 control-label">Role Name</label>

                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="role_name" id="role_name" value="" placeholder="">
                    <span class="col-sm-5 messages"> <?php echo form_error("role_name");?></span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_des" class="col-sm-2 control-label">Role Description</label>

                  <div class="col-sm-6"> 
                    <textarea name="role_des" id="role_des" rows="10" cols="80"></textarea>
                    <span class="col-sm-5 messages"> <?php echo form_error("role_des");?></span>
                  </div>
                </div>
                 <?php
                    if(!empty($role_types))
                    {
                    ?>
                      <div class="form-group">
                          <label for="roleID_sec" class="col-sm-2 control-label">Role Types</label>
                          <div class="col-sm-10">
                              <div id="accordion-resizer" class="ui-widget-content">
                                  <div id="accordion">  
                                  <?php 
                                  foreach ($role_types as $types) {
                                  ?>
                                      <h3><?php echo $types['group_name']?></h3>
                                      <div>
                                      <ul>
                                        <li><input type="checkbox" ><b>Select All</b>
                                              <ul id="roleID_sec">
                                              <?php
                                                  foreach($types['groups_role'] as $roles)
                                                  {
                                                  ?>
                                                  <li><input type="checkbox" value="<?php echo $roles['id']?>" name="roleID[]" ><strong><?php echo $roles['name']?></strong> <span style=" margin-left: 50px;"><?php echo $roles['des']?></span></li>
                                                  <?php   
                                                  }
                                              ?>
                                              </ul>
                                          </li>
                                      </ul>    
                                      </div> 
                                  <?php 
                                  }
                                  ?>
                                   </div>
                              </div>    
                          </div>
                      </div>
                    <?php 
                    }
                    ?>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
  <script type="text/javascript"> 
  $(function () { 
    $("input[type='checkbox']").change(function () { 
      $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
    }); 
  }); 
  </script>
<!-- CK Editor -->
<!-- <script src="<?php //echo ADMIN_ASSETS_PATH;?>plugins/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('cms_description');
    //bootstrap WYSIHTML5 - text editor
    //$(".textarea").wysihtml5();
  });
</script> -->

<script type="text/javascript">
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      // These are the constraints used to validate the form
      var constraints = {
        role_name: {
          presence: true,
        }
      };

      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#main");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        // validate the form aainst the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
          showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it 
        $("#main").submit();
      }
    })();
  </script>

