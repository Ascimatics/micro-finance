<?php $this->load->view("admin/header");
$page_id=$this->uri->segment(4);
?> 
<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Account
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/account"><i class="fa fa-dashboard"></i> Manage Account</a></li>
        <li class="active">edit account</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Edit CMS</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form name="changepassword" id="main" class="form-horizontal" action="<?php echo base_url()?>admin/account/updateData" method="post" enctype="multipart/form-data" novalidate>
              <div class="box-body">
                <div class="form-group">
                  <label for="area_id" class="col-sm-2 control-label">Choose Client :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="client_id" name="client_id" onchange="fetch_client_accounts(this.value)">
                    <option value="">Select</option>
                      <?php foreach ($client_list as $key => $value) { ?>
                        <option value="<?php echo $value->id;?>" <?php if($value->id==$client_id){?> selected<?php }?>><?php echo $value->account_name;?></option>
                      <?php }?>
                    
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                
                <div id="client_div">
                  <h3 class="box-title">Draw:</h3>
                  <div id="file_div">
                  <?php 
                  foreach ($drawee_list as $key => $value) { ?>
                     <div class="row form-group">
                        <label for="drawee_designation" class="col-sm-2 control-label">Designation :</label>
                        <div class="col-xs-3">
                           <select class="form-control select2" disabled="true">
                              <option value="">Select</option>
                              <option value="Proprietor" <?php if($value->drawee_designation=='Proprietor'){?> selected<?php }?>>Proprietor</option>
                              <option value="Director" <?php if($value->drawee_designation=='Director'){?> selected<?php }?>>Director</option>
                              <option value="Partner" <?php if($value->drawee_designation=='Partner'){?> selected<?php }?>>Partner</option>
                           </select>
                           <span class="col-sm-12 messages"></span>
                        </div>
                        <label for="drawee_name" class="col-sm-2 control-label">Name :</label>
                        <div class="col-xs-3">
                           <input class="form-control" placeholder="" type="text" id="drawee_name" disabled="true" name="drawee_name[]" value="<?php echo $value->drawee_name;?>">
                           <span class="col-sm-12 messages"></span>
                        </div>
                     </div>
                     <?php  }?>
                  </div>
                  <hr>
                  <?php 

                     $count=1;
                     foreach ($document_list as $key => $document) { 
                     
                       $accountlist   = get_data(TABLE_PREFIX.'accounts',array('document_id'=>$document->id));

                       ?>

                  <h3 class="box-title">Document Details:</h3>
                  <div>
                     <?php 
                     $document_details_list = get_data(TABLE_PREFIX."client_document_details",array('document_id'=>$document->id));
                        foreach ($document_details_list as $key => $value) { ?>
                     <div>
                        <div class="form-group">
                           <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
                           <div class="col-sm-6">
                              <select class="form-control select2" disabled="true">
                                 <option value="Hundi" <?php if($value->document_type=='Hundi'){?> selected<?php  }?>>Hundi</option>
                                 <option value="Property papers" <?php if($value->document_type=='Property papers'){?> selected<?php  }?>>Property papers</option>
                                 <option value="Agreement" <?php if($value->document_type=='Agreement'){?> selected<?php  }?>>Agreement</option>
                                 <option value="Commodity" <?php if($value->document_type=='Commodity'){?> selected<?php  }?>>Commodity</option>
                              </select>
                              <span class="col-sm-12 messages"></span>
                           </div>
                        </div>
                     </div>
                     <?php  }?>
                  </div>
                  <div class="form-group">
                     <label for="document_value" class="col-sm-2 control-label">Document value :</label>
                     <div class="col-sm-6"> 
                        <input type="text" class="form-control" disabled="true" value="<?php echo $document->document_value;?>" placeholder="">
                        <span class="col-sm-12 messages"></span>
                     </div>
                  </div>

                <?php 
                if(!empty($accountlist)){ 
                  foreach ($accountlist as $key => $account_data) { 
                ?>
                <h3 class="box-title">Account Details:</h3>
                <div class="form-group">
                  <label for="area_id" class="col-sm-2 control-label">Choose Area :</label>
                  <div class="col-sm-4"> 
                    <select class="form-control select2" <?php if($count==$total_accounts){?> id="area_id" name="area_id" onchange="add_new_area(this.value)" <?php }else{?> disabled="true" <?php }?> >
                    <option value="">Select</option>
                      <?php foreach ($area_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>" <?php if($value->id==$account_data->area_id){?> selected<?php }?>><?php echo $value->area_name;?></option>
                      <?php }?>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <?php if($count==$total_accounts){?>
                <div id="new_area_div"></div>
                <?php }?>
                <div class="form-group">
                  <label for="loan_number" class="col-sm-2 control-label">Loan No :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" <?php if($count==$total_accounts){?> name="loan_number" id="loan_number" <?php }else{?> disabled="true" <?php }?> value="<?php echo $account_data->loan_number;?>" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="loan_amount" class="col-sm-2 control-label">Loan Amount :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" <?php if($count==$total_accounts){?> name="loan_amount" id="loan_amount" <?php }else{?> disabled="true" <?php }?> value="<?php echo $account_data->loan_amount;?>" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="interest_rate" class="col-sm-2 control-label">Rate of Interest :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" <?php if($count==$total_accounts){?> name="interest_rate" id="interest_rate" <?php }else{?> disabled="true" <?php }?> value="<?php echo $account_data->interest_rate;?>" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_open_date" class="col-sm-2 control-label">Installment start Date :</label>
                  <div class="col-xs-3">
                     <input type="text" class="form-control datepick" <?php if($count==$total_accounts){?> name="account_open_date" id="account_open_date" <?php }else{?> disabled="true" <?php }?> value="<?php echo $account_data->account_open_date;?>" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_close_date" class="col-sm-2 control-label">Installment close Date :</label>
                  <div class="col-xs-3">
                     <input type="text" class="form-control datepick" <?php if($count==$total_accounts){?> name="account_close_date" id="account_close_date" <?php }else{?> disabled="true" <?php }?> value="<?php echo $account_data->account_close_date;?>" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_of_days" class="col-sm-2 control-label">No of Days :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" <?php if($count==$total_accounts){?> name="no_of_days" id="no_of_days" <?php }else{?> disabled="true" <?php }?> value="<?php echo $account_data->no_of_days;?>" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <?php $count++; }}?> <hr><?php } ?>
                </div> 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>
<script type="text/javascript">
  function fetch_client_accounts(client_id){

    $.post("<?php echo base_url()?>admin/account/fetchClientAccounts",{'client_id':client_id},function(data){
      $("#client_div").html(data);
    });  
  }
</script>
<script type="text/javascript">
  function add_new_area(val){
    if(val=='new'){
      $('#new_area_div').html('<div class="form-group"><label for="area_name" class="col-sm-2 control-label">Area Name :</label><div class="col-sm-4"> <input type="text" class="form-control" name="area_name" id="area_name" value="" placeholder=""><span class="col-sm-12 messages"></span></div></div>');
    }else{
      $('#new_area_div').html('');
    }
  }
</script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
  <script type="text/javascript"> 
  $(function () { 
    $("input[type='checkbox']").change(function () { 
      $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
    }); 
  }); 
  </script>

<script type="text/javascript">
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      // These are the constraints used to validate the form
      var constraints = {
          client_id: {
          presence: true,
        },
      };

      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#main");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        // validate the form aainst the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
          showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name],textarea[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it 
        $("#main").submit();
      }
    })();
  </script>

