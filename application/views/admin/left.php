<?php $first_segment = $this->uri->segment(2);

$second_segment = $this->uri->segment(3);
?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo ADMIN_ASSETS_PATH;?>image/no-user.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username');?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
<!--       <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <li class="<?php if($first_segment=='dashboard'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='dashboard'){ ?>active<?php }?>"><a href="<?php echo base_url();?>admin/"><i class="fa fa-circle-o"></i> Dashboard </a></li>
            <!-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li> -->
          </ul>
        </li>

        <li class="<?php if($first_segment=='area'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Area</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='area'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/area"><i class="fa fa-circle-o"></i> Manage Area</a></li>
          </ul>
        </li>
        <li class="<?php if($first_segment=='collectionMan'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Collection Man</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='collectionMan'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/collectionMan"><i class="fa fa-circle-o"></i> Manage CM</a></li>
          </ul>
        </li>
        <li class="<?php if($first_segment=='broker'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Broker</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='broker'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/broker"><i class="fa fa-circle-o"></i> Manage Broker</a></li>
          </ul>
        </li>

        <li class="<?php if($first_segment=='clients'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Clients</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='clients'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/clients"><i class="fa fa-circle-o"></i> Manage Clients</a></li>
          </ul>
        </li>
        <li class="<?php if($first_segment=='documents'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Document</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='documents'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/documents"><i class="fa fa-circle-o"></i> Manage Documents</a></li>
          </ul>
        </li>
        <li class="<?php if($first_segment=='account'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Account</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='account'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/account"><i class="fa fa-circle-o"></i> Manage Accounts</a></li>
          </ul>
        </li>

        <!-- <li class="<?php if($first_segment=='banners'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Role</span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($first_segment=='roles'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/roles"><i class="fa fa-circle-o"></i> Manage Role</a></li>
            <li class="<?php if($first_segment=='roles'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/roles/roleassign"><i class="fa fa-circle-o"></i> Assign Role</a></li>
          </ul>
        </li> -->

        <li class="<?php if($first_segment=='tools'){ ?> active<?php }?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Tools</span>
            <!-- <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span> -->
          </a>
          <ul class="treeview-menu">
            <!-- <li class="<?php if($second_segment=='changeSettings'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/tools/changeSettings"><i class="fa fa-circle-o"></i> Settings</a></li> -->
            <li class="<?php if($second_segment=='changePassword'){ ?> active<?php }?>"><a href="<?php echo base_url();?>admin/tools/changePassword"><i class="fa fa-circle-o"></i> Change Password</a></li>
            <li><a href="<?php echo base_url();?>admin/home/logout"><i class="fa fa-circle-o"></i> Logout</a></li>
            <!-- <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li> -->
          </ul>
        </li>
        <!-- <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>