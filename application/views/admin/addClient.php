<?php $this->load->view("admin/header");
$page_id=$this->uri->segment(4);
?> 
<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Client
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/clients"><i class="fa fa-dashboard"></i> Manage Clients</a></li>
        <li class="active">create client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Edit CMS</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form name="changepassword" id="main" class="form-horizontal" action="<?php echo base_url()?>admin/clients/submitData" method="post" enctype="multipart/form-data" novalidate>
              <div class="box-body">
                <div class="form-group">
                  <label for="account_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="full_name" id="full_name" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_address" class="col-sm-2 control-label">Work Address :</label>
                  <div class="col-sm-6"> 
                    <textarea name="work_address" class="form-control" id="work_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="residence_address" class="col-sm-2 control-label">Residence Address :</label>
                  <div class="col-sm-6"> 
                    <textarea class="form-control" name="residence_address" id="residence_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                  <div id="phone_div">
				<div class="row form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control" name="phone[]" id="phone" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" id="add_more_phone" onclick="add_phone();">Add more</button>
                  </div>
                </div>
				</div>
              
				<div class="form-group">
                  <label for="firm_type" class="col-sm-2 control-label">Type of firm:</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="firm_type_id" name="firm_type_id" onchange="add_new_firm(this.value)">
                    <option value="">Select</option>
                    
                      <?php foreach ($firm_types as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->type;?></option>
                      <?php }?>
                    
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
				        <div id="firm_type_div"></div>
                <h3 class="box-title">Concerned People:</h3>
                <div id="concern_div">
                <div class="row form-group">
                  
                  <label for="concern_designation" class="col-sm-2 control-label">Designation :</label>
                  <div class="col-xs-3">
                    <select class="form-control" id="concern_designation_id" name="concern_designation_id[]" onchange="add_designation(this.value)">
                      <option value="">Select</option>
                      <?php foreach ($concern_designation as $key => $value) { ?>
                      <option value="<?php echo $value->id;?>"><?php echo $value->designation;?></option>
                      <?php }?>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <div id="concern_sub_div">
                  <label for="concern_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-xs-3">
                    <input class="form-control" placeholder="" type="text" id="concern_name" name="concern_name[]">
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" id="add_more_concern" onclick="add_concern();">Add more</button>
                  </div>
                  </div>
                </div>
                </div>
                <div id="concern_designation_div"></div>

				        <div>&nbsp;</div>	
				        <div class="form-group">
                  <div class="row form-group">
                  
                  <label for="concern_designation" class="col-sm-2 control-label">Introduced By :</label>
                  <div class="col-xs-3">
                     <input class="form-control" placeholder="" type="text" id="introduced_by" name="introduced_by">
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <label for="concern_name" class="col-sm-2 control-label">Contact No :</label>
                  <div class="col-xs-3">
                    <input class="form-control" placeholder="" type="text" id="introduce_contact" name="introduce_contact">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                </div> 
                <div class="form-group">
                  <label for="broker_ids" class="col-sm-2 control-label">Broker :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control" multiple="multiple" id="broker_ids" name="broker_ids[]" onchange="add_new_broker(this.value)">
                      <option value="" selected>Select</option>
                      <?php //$count=1;
                      foreach ($broker_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->full_name;?></option>
                      <?php }?>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
				
				        <div class="form-group">
                  <label for="broker_ids" class="col-sm-2 control-label">Note :</label>
                  <div class="col-sm-6"> 
                     <textarea class="form-control" placeholder="" type="text" id="note" name="note" rows="5"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>

<script type="text/javascript">
  function add_new_broker(){
    var broker_ids = $('#broker_ids').val();
    for (var i = 0; i < broker_ids.length; i++) {
      if (broker_ids[i] == "new") {
        $.post("<?php echo base_url()?>admin/clients/addMoreBroker",{},function(data){
          $("#new_broker_div").html(data);
        }); 
      }else{
        $("#new_broker_div").html('');
      }
    }
  }
</script>

<script type="text/javascript">
  function add_new_firm(type){
    if (type == "4") {
	   $("#firm_type_div").html('');
	  }else if(type=='new'){
	  
	  $("#firm_type_div").html('<div class="form-group"><label for="broker_ids" class="col-sm-2 control-label">Firm Type name :</label><div class="col-sm-6"><input type="text" class="form-control" name="firm_type" id="firm_type" value="" placeholder=""><span class="col-sm-12 messages"></span></div></div>');
	  }else{
       $("#firm_type_div").html('');
	  }
  }
</script>
<script type="text/javascript">
  function add_designation(str){
      if (str == "new") {
	       $('#concern_sub_div').hide();
         $("#concern_designation_div").html('<div id="concern_more_sub_div"><div class="row form-group"><label for="phone" class="col-sm-2 control-label">Designation :</label><div class="col-xs-3"><input type="text" class="form-control" name="concern_designation" id="concern_designation" value="" placeholder=""><span class="col-sm-12 messages"></span></div><label for="concern_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="concern_name_text" name="concern_name_text"><span class="col-sm-12 messages"></span></div></div></div>');
      }else{
        $('#concern_sub_div').show();
	  	$('#concern_designation_div').html('');
	  }
  }

</script>
<script type="text/javascript">
  function add_new_introduced(str){
      if (str == "new") {
        $.post("<?php echo base_url()?>admin/clients/addMoreIntroduced",{},function(data){
          $("#new_introduced_div").html(data);
        }); 
      }else{
        $("#new_introduced_div").html('');
      }
  }
</script>
<script type="text/javascript">
function add_phone()
{
 $("#phone_div").append('<div class="row form-group"><label for="phone" class="col-sm-2 control-label">Phone :</label><div class="col-xs-3"><input type="text" class="form-control" name="phone[]" id="phone" value="" placeholder=""><span class="col-sm-12 messages"></span></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_phone(this);">Remove</button></div</div>');
}
function remove_phone(ele)
{
 $(ele).parent().parent().remove();
}
</script>
<script type="text/javascript">
function add_concern()
{
 $("#concern_div").append('<div class="row form-group"><label for="concern_designation" class="col-sm-2 control-label">Designation :</label><div class="col-xs-3"><select class="form-control" id="concern_designation_id" name="concern_designation_id[]"><option value="">Select</option><option value="1">Proprietor</option><option value="2">Director</option><option value="3">Partner</option><option value="4">Owner</option><option value="5">Renter</option></select><span class="col-sm-12 messages"></span></div><label for="concern_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="concern_name" name="concern_name[]"><span class="col-sm-12 messages"></span></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_concern(this);">Remove</button></div></div>');
 
}
function remove_concern(ele)
{
 $(ele).parent().parent().remove();
}
function remove_draw_div(ele)
{
 $(ele).parent().parent().parent().remove();
}
</script>

  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
  <script type="text/javascript"> 
  $(function () { 
    $("input[type='checkbox']").change(function () { 
      $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
    }); 
  }); 
  </script>
<!-- CK Editor -->
<!-- <script src="<?php //echo ADMIN_ASSETS_PATH;?>plugins/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('cms_description');
    //bootstrap WYSIHTML5 - text editor
    //$(".textarea").wysihtml5();
  });
</script> -->

<script type="text/javascript">
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      // These are the constraints used to validate the form
      var constraints = {
        full_name: {
          presence: true,
          format: {
            pattern: "[a-zA-Z ]*$",
            flags: "i",
            message: "can only contain character"
          }
        },
      };

      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#main");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        // validate the form aainst the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
          showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name],textarea[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it 
        $("#main").submit();
      }
    })();
  </script>

