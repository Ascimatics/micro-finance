<?php $this->load->view("admin/header");
$page_id=$this->uri->segment(4);
?> 
<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Client
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/clients"><i class="fa fa-dashboard"></i> Manage Clients</a></li>
        <li class="active">create client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Edit CMS</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form name="changepassword" id="main" class="form-horizontal" action="<?php echo base_url()?>admin/clients/submitData" method="post" enctype="multipart/form-data" novalidate>
              <div class="box-body">
                <div class="form-group">
                  <label for="account_name" class="col-sm-2 control-label">Client :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="account_name" id="account_name" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  
                  <label for="account_designation" class="col-sm-2 control-label">Designation :</label>
                  <div class="col-xs-6">
                    <select class="form-control select2" id="account_designation" name="account_designation">
                      <option value="">Select</option>
                      <option value="Proprietor">Proprietor</option>
                      <option value="Director">Director</option>
                      <option value="Partner">Partner</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="client_name" class="col-sm-2 control-label">Full Name :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="full_name" id="full_name" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_address" class="col-sm-2 control-label">Work Address :</label>
                  <div class="col-sm-6"> 
                    <textarea name="work_address" class="form-control" id="work_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="residence_address" class="col-sm-2 control-label">Residence Address :</label>
                  <div class="col-sm-6"> 
                    <textarea class="form-control" name="residence_address" id="residence_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_telephone" class="col-sm-2 control-label">Telephone(work) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="work_telephone" id="work_telephone" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="mobile1" class="col-sm-2 control-label">Mobile 1 (personal) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="mobile1" id="mobile1" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="mobile2" class="col-sm-2 control-label">Mobile 2 (home) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="mobile2" id="mobile2" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="introduced_by" class="col-sm-2 control-label">Introduced By :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="introduced_by" name="introduced_by">
                    <option value="">Select</option>
                      <optgroup label="Clients">
                      <?php foreach ($client_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->full_name;?></option>
                      <?php }?>
                      </optgroup>
                      <optgroup label="Collection man">
                      <?php foreach ($collection_man_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->full_name;?></option>
                      <?php }?>
                      </optgroup>
                      <optgroup label="Brokers">
                      <?php foreach ($broker_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->full_name;?></option>
                      <?php }?>
                      </optgroup>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="area_id" class="col-sm-2 control-label">Choose Area :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="area_id" name="area_id" onchange="add_new_area(this.value)">
                    <option value="">Select</option>
                      <?php foreach ($area_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->area_name;?></option>
                      <?php }?>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div id="new_area_div"></div>

                <div class="form-group">
                  <label for="broker_ids" class="col-sm-2 control-label">Broker :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" multiple="multiple" id="broker_ids" name="broker_ids[]" onchange="add_new_broker(this.value)">
                      <?php 
                      foreach ($broker_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->full_name;?></option>
                      <?php }?>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div id="new_broker_div"></div>

                <hr>
                <h3 class="box-title">Draw:</h3>
                <div id="file_div">
                <div class="row form-group">
                  
                  <label for="drawee_designation" class="col-sm-2 control-label">Designation :</label>
                  <div class="col-xs-3">
                    <select class="form-control select2" id="drawee_designation" name="drawee_designation[]">
                      <option value="">Select</option>
                      <option value="Proprietor">Proprietor</option>
                      <option value="Director">Director</option>
                      <option value="Partner">Partner</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <label for="drawee_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-xs-3">
                    <input class="form-control" placeholder="" type="text" id="drawee_name" name="drawee_name[]">
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="add_file();">Add more</button>
                  </div>
                </div>
                </div>

                <hr>
                <h3 class="box-title">guarantor/accept : </h3>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="same_as_draw" name="same_as_draw" onclick="modify_guarantor()" value="yes"> Same as Draw
                      </label>
                    </div>
                  </div>
                </div>
                <div id="guarantor_div">
                <div>
                <div class="row form-group">
                  
                  <label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label>
                  <div class="col-xs-2">
                    <select class="form-control select2" id="guarantor_designation" name="guarantor_designation[]">
                      <option value="">Select</option>
                      <option value="Proprietor">Partner</option>
                      <option value="Owner">Shop Owner</option>
                      <option value="Director">Director</option>
                      <option value="Other">Other</option>
                    </select>
                  </div>
                  <div>
                  <label for="guarantor_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]">
                  </div>
                  <label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]">
                  </div>
                  </div>
                </div><br/>
                <div class="row form-group">
                  <label for="guarantor_address" class="col-sm-2 control-label">Address :</label>
                  <div class="col-xs-2">
                    <textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"></textarea>
                  </div>
                  <label for="tel1" class="col-sm-2 control-label">Tel 1 :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]">
                  </div>
                  <label for="tel2" class="col-sm-2 control-label">Tel 2 :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]">
                  </div>
                </div>
                </div>
                </div>
                <div class="row" id="div3">
                <div class="col-xs-10"></div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" onclick="add_guarantor();">Add more</button>
                  </div>
                </div><hr>
                <h3 class="box-title">Document Details:</h3>
                
                <div id="document_div">
                <div>
                <div class="form-group">
                  <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="document_type" name="document_type[]">
                    <option value="">Select</option>
                      <option value="Hundi">Hundi</option>
                      <option value="Property papers">Property papers</option>
                      <option value="Agreement">Agreement</option>
                      <option value="Commodity">Commodity</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

                
                
                <div class="form-group">
                <div class="col-xs-1"></div>
                <div class="col-xs-4">
                  <label for="exampleInputFile">Upload Document</label>
                  <input type="file" name="client_document[]">
                  </div>
                </div> 
                </div>
                </div>
                <div class="row form-group">
                <div class="col-xs-8"></div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" onclick="add_document();">Add more</button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="purpose" class="col-sm-2 control-label">Purpose :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="purpose" name="purpose">
                    <option value="">Select</option>
                      <option value="Short term">Short term</option>
                      <option value="Fixed">Fixed</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="document_value" class="col-sm-2 control-label">Document value :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="document_value" id="document_value" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-offset-1 col-xs-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="doc_received_status" name="doc_received_status" value="Yes"> : Doc received ? (tick for yes)
                      </label>
                    </div>
                  </div>
                  <label for="received_by" class="col-sm-2 control-label">Received by :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control" name="received_by" id="received_by" value="" placeholder="">
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-offset-1 col-xs-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="validity_status" name="validity_status" value="Yes"> : Doc Valid/Expired ? (tick for valid)
                      </label>
                    </div>
                  </div>
                  <label for="expiry_date" class="col-sm-2 control-label">Date of Expiry :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control pull-right datepick" id="expiry_date" name="expiry_date">
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-offset-1 col-xs-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="return_status" name="return_status" value="Yes"> : Doc Returned ? (tick for yes)
                      </label>
                    </div>
                  </div>
                  <label for="return_date" class="col-sm-2 control-label">Date of Return :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control pull-right datepick" id="return_date" name="return_date">
                  </div>
                </div>

                <hr>

                <div class="form-group">
                  <label for="loan_number" class="col-sm-2 control-label">Loan No :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="loan_number" id="loan_number" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="loan_amount" class="col-sm-2 control-label">Loan Amount :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="loan_amount" id="loan_amount" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="interest_rate" class="col-sm-2 control-label">Rate of Interest :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="interest_rate" id="interest_rate" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_open_date" class="col-sm-2 control-label">Installment Start Date :</label>
                  <div class="col-xs-3">
                     <input type="text" class="form-control datepick" name="account_open_date" id="account_open_date" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_close_date" class="col-sm-2 control-label">Installement close Date :</label>
                  <div class="col-xs-3">
                     <input type="text" class="form-control datepick" name="account_close_date" id="account_close_date" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_of_days" class="col-sm-2 control-label">No of Days :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="no_of_days" id="no_of_days" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="row form-group">
                  <div class="col-sm-offset-1 col-xs-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" id="cheque_received_status" name="cheque_received_status" value="Yes"> : Cheque received ? (tick for Yes)
                      </label>
                    </div>
                  </div>
                </div>
                <div id="cheque_div"></div> 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>
<script type="text/javascript">
  $("#cheque_received_status").on("click", function(){
    check = $("#cheque_received_status").is(":checked");
    
    if(check) {
    
      $.post("<?php echo base_url()?>admin/clients/addCheque",{},function(data){
        $("#cheque_div").append(data);
      }); 
    
    } else {
        $('#cheque_div').html('');
    }
});
</script>
<script type="text/javascript">
  function add_cheque()
{
      $.post("<?php echo base_url()?>admin/clients/addMoreCheque",{},function(data){
        $("#cheque_div").append(data);
      });
}
function remove_cheque(ele)
{
 $(ele).parent().parent().remove();
}
</script>
<script type="text/javascript">
  function add_new_area(val){
    if(val=='new'){
      $('#new_area_div').html('<div class="form-group"><label for="area_name" class="col-sm-2 control-label">Area Name :</label><div class="col-sm-6"> <input type="text" class="form-control" name="area_name" id="area_name" value="" placeholder=""><span class="col-sm-12 messages"></span></div></div>');
    }
  }
</script>
<script type="text/javascript">
  function add_new_broker(){
    var broker_ids = $('#broker_ids').val();
    for (var i = 0; i < broker_ids.length; i++) {
      if (broker_ids[i] == "new") {
        $.post("<?php echo base_url()?>admin/clients/addMoreBroker",{},function(data){
          $("#new_broker_div").html(data);
        }); 
      }else{
        $("#new_broker_div").html('');
      }
    }
  }
</script>
<script type="text/javascript">
  function add_document(){
    $.post("<?php echo base_url()?>admin/clients/addMoreDocument",{},function(data){
      $("#document_div").append(data);
    });  
  }
</script>
<script type="text/javascript">
  function add_guarantor(){
    $.post("<?php echo base_url()?>admin/clients/addMoreGuarantor",{},function(data){
      $("#guarantor_div").append(data);
    });  
  }
</script>
<script type="text/javascript">
function add_file()
{
 $("#file_div").append('<div class="row form-group"><label for="drawee_designation" class="col-sm-2 control-label">Designation :</label><div class="col-xs-3"><select class="form-control select2" id="drawee_designation" name="drawee_designation[]"><option value="">Select</option><option value="Proprietor">Proprietor</option><option value="Director">Director</option><option value="Partner">Partner</option></select><span class="col-sm-12 messages"></span></div><label for="drawee_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="drawee_name" name="drawee_name[]"><span class="col-sm-12 messages"></span></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_file(this);">Remove</button></div></div>');
}
function remove_file(ele)
{
 $(ele).parent().parent().remove();
}
function remove_draw_div(ele)
{
 $(ele).parent().parent().parent().remove();
}
</script>
<script type="text/javascript">
  function modify_guarantor(){ 
    if($('#same_as_draw').is(':checked')){
      $('#guarantor_div').hide();
      $('#div3').hide();
    }else{
      $('#guarantor_div').show();
      $('#div3').show();
    }

  }
</script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
  <script type="text/javascript"> 
  $(function () { 
    $("input[type='checkbox']").change(function () { 
      $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
    }); 
  }); 
  </script>
<!-- CK Editor -->
<!-- <script src="<?php //echo ADMIN_ASSETS_PATH;?>plugins/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('cms_description');
    //bootstrap WYSIHTML5 - text editor
    //$(".textarea").wysihtml5();
  });
</script> -->

<script type="text/javascript">
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      // These are the constraints used to validate the form
      var constraints = {
        full_name: {
          presence: true,
          format: {
            pattern: "[a-zA-Z ]*$",
            flags: "i",
            message: "can only contain character"
          }
        },
        work_telephone: {
          presence: true,
          format: {
            pattern: "[0-9]{10}$",
            flags: "i",
            message: "must be a 10 digit number"
          }
        },
        mobile1: {
          presence: true,
          format: {
            pattern: "[0-9]{10}$",
            flags: "i",
            message: "must be a 10 digit number"
          }
        },
        mobile2: {
          presence: true,
          format: {
            pattern: "[0-9]{10}$",
            flags: "i",
            message: "must be a 10 digit number"
          }
        },
      };

      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#main");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        // validate the form aainst the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
          showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name],textarea[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it 
        $("#main").submit();
      }
    })();
  </script>

