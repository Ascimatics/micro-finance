<?php $this->load->view("admin/header");?>

<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Start Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>
      <!-- End Modal -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Banner
        <small>Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Manage Banner</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Notification -->
      <div id="toast-container" class="toast-top-right" aria-live="polite" role="alert">
        <?php
        if ($this->session->flashdata('message')!='')
        {
          $message = explode("|",$this->session->flashdata('message'));
            ?>           
              <div class="toast toast-<?php echo $message[0];?>" id="toast-container-body">
                <button class="toast-close-button" role="button">×</button>
                <div class="toast-title"><?php echo $message[0];?></div>
                <div class="toast-message"><?php echo $message[1];?></div>
              </div>
            <?php
        }
          ?>
      </div>
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box">
            <div class="box-header">
              <div class="btn-group" >
                <button type="button" class="btn btn-info">Action</button>
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void(0);" onclick="delselected()">Delete</a></li>
                  </ul>
              </div>
              <a class="btn btn-info btn-success" data-toggle="modal" data-target="#myModal" onclick="add()" style="float:right"><i class="fa fa-fw fa-plus"></i>Add New </a>               
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="tablesec">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><input type="checkbox" class="checkboxes" data-set="#sample_1 .checkboxes" id="check" /></th>
                  <th style="width: 181px;">Banner Image</th>
                  <th>Banner Title</th>
                  <th>Status</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php
                foreach($viewData as $key => $val)
                {
                    // Get pic
                      if($val->banner_image== "")
                      {
                        $pic = "nopic.jpg";
                      }
                      
                      else
                      {
                        $pic = $val->banner_image;
                      }
                  ?>
                <tr>
                    <td><input type="checkbox" class="checkboxes"  value="<?php echo $val->id?>" name="data[]"/></td>
                    <td>
                      <div class="col-sm-6">
                        <img class="img-responsive" src="<?php echo base_url()."uploads/images/".$pic?>" alt="User profile picture">
                      </div>
                    </td>
                    <td><?php echo stripslashes($val->banner_title)?></td>              
                    <td>
                      <input type="checkbox" class="make-switch" data-size="small" <?php echo $val->status == 'Yes' ? 'checked' : ''?> id="stat<?php echo $val->id?>" onChange="changestatus(this.value,'<?php echo $val->id?>')">
                    </td>
                    <td>
                    <a class="btn btn-info btn-success" data-toggle="modal" data-target="#myModal" onclick="edit('<?php echo $val->id;?>')"><i class="fa fa-fw fa-edit"></i></a>
                    </td>
                    <td><a class="btn btn-info btn-success" href="#" onclick="deleteone(<?php echo $val->id?>)" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;"><i class="fa fa-fw fa-trash-o"></i></a></td>
                </tr>

                <?php
                }
                  ?>               
                </tbody>
                <tfoot>
                <tr>
                  <th><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                  <th style="width: 181px;">Banner Image</th>
                  <th>Banner Title</th>
                  <th>Status</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>

<script type="text/javascript">
  
  function edit(id)
  {    
    
    $.post('<?php echo base_url();?>admin/banners/editData',{ id : id },
      function(data)
      {
        $('#myModal').html(data);
        $(".chosen").chosen();
      }
    )
  }
  
  function add()
  {
    
    
    $.post('<?php echo base_url();?>admin/banners/addData',
      function(data)
      {
        $('#myModal').html(data);
        $(".chosen").chosen();
      }
    )
  }

</script>
<script type="text/javascript">
  function deleteone(id)
  { 
    var cnf = confirm("Are you sure to delete?");
    
    if(cnf)
    {
      $('.portlet .tools a.reload').click();
      $.post('<?php echo base_url();?>admin/banners/deleteData',{ id : id,mode : 'single' },
        function(data)
        {
          window.location.href = "<?php echo base_url()?>admin/banners";
        }
      );
    }
  }
  
  function delselected()
  {
    var data = document.getElementsByName('data[]');
    
    var ln = data.length;
    
    var flag = 0;
    var str = "";
    
    for(i=0;i<ln;i++)
    {
      if(data[i].checked)
      {
        str = str + data[i].value + ',';
      }
    }
    
    if(str != "")
    {
      var cnf = confirm("Are you sure to delete?");
    
      if(cnf)
      {
        
        $.post('<?php echo base_url();?>admin/banners/deleteData',{ id : str , mode : 'selected' },
          function(data)
          {
            window.location.href = "<?php echo base_url()?>admin/banners";           

          }
        );
      }
    }
    else
    { 
      alert('You must select atleast one item');
    }
  }

  function changestatus(stat,id)
  {
    
    var checked = jQuery('#stat'+id).is(":checked");

    $.post('<?php echo base_url();?>admin/banners/changeStatus',{ stat : checked , id : id },function(data){

      $('#toast-container').html('<div class="toast toast-success" id="toast-container-body"><button class="toast-close-button" role="button">×</button><div class="toast-title">Success</div><div class="toast-message">Status changed successfully.</div></div>');

    });
  }
  </script>


