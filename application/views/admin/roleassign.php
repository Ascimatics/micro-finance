<?php $this->load->view("admin/header");?>

<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Start Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>
      <!-- End Modal -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assign
        <small>Role</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/roleassign"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Assign Roles</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Notification -->
      <div id="toast-container" class="toast-top-right" aria-live="polite" role="alert">
        <?php
        if ($this->session->flashdata('message')!='')
        {
          $message = explode("|",$this->session->flashdata('message'));
            ?>           
              <div class="toast toast-<?php echo $message[0];?>" id="toast-container-body">
                <button class="toast-close-button" role="button">×</button>
                <div class="toast-title"><?php echo $message[0];?></div>
                <div class="toast-message"><?php echo $message[1];?></div>
              </div>
            <?php
        }
          ?>
      </div>
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box">
            <div class="box-header">
              <div class="btn-group" >
                <button type="button" class="btn btn-info">Action</button>
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void(0);" onclick="delselected()">Delete</a></li>
                  </ul>
              </div>
              <a class="btn btn-info btn-success" style="float:right" data-toggle="modal" data-target="#myModal" onclick="add()"><i class="fa fa-fw fa-plus"></i>Add New </a>               
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="tablesec">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><input type="checkbox" class="checkboxes" data-set="#sample_1 .checkboxes" id="check" /></th>
                  <th style="width: 181px;">Username</th>
                  <th>Assigned Roles</th>
                  <th>Date Created</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody id="tbody">
                <?php
                foreach($viewData as $key => $val)
                {
                  ?>
                <tr>
                    <td><input type="checkbox" class="checkboxes"  value="<?php echo $val->id?>" name="data[]"/></td>
                    <td><?php echo stripslashes($val->first_name)." ".stripslashes($val->last_name);?></td>
                    <td><?php echo stripslashes($val->role_name);?></td> 
                    <td><?php echo stripslashes($val->created_date);?></td>             
                    <td>
                    <a class="btn btn-info btn-success" data-toggle="modal" data-target="#myModal" onclick="edit('<?php echo $val->assign_id;?>')"><i class="fa fa-fw fa-edit"></i></a>
                    </td>
                    <td><a class="btn btn-info btn-success" href="#" onclick="deleteone(<?php echo $val->assign_id?>)" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;"><i class="fa fa-fw fa-trash-o"></i></a></td>
                </tr>

                <?php
                }
                  ?>               
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th style="width: 181px;">Username</th>
                  <th>Assigned Roles</th>
                  <th>Date Created</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>

<script type="text/javascript">
  
  function edit(id)
  {    
    
    $.post('<?php echo base_url();?>admin/roles/editRoleAssign',{ id : id },
      function(data)
      {
        $('#myModal').html(data);
        $(".chosen").chosen();
      }
    )
  }
  
  function add()
  {
    
    
    $.post('<?php echo base_url();?>admin/roles/createRoleAssign',
      function(data)
      {
        $('#myModal').html(data);
        $(".chosen").chosen();
      }
    )
  }

</script>
<script type="text/javascript">
  function deleteone(id)
  { 
    var cnf = confirm("Are you sure to delete?");
    
    if(cnf)
    {
      $('.portlet .tools a.reload').click();
      $.post('<?php echo base_url();?>admin/banners/deleteData',{ id : id,mode : 'single' },
        function(data)
        {
          window.location.href = "<?php echo base_url()?>admin/banners";
        }
      );
    }
  }
  
  function delselected()
  {
    var data = document.getElementsByName('data[]');
    
    var ln = data.length;
    
    var flag = 0;
    var str = "";
    
    for(i=0;i<ln;i++)
    {
      if(data[i].checked)
      {
        str = str + data[i].value + ',';
      }
    }
    
    if(str != "")
    {
      var cnf = confirm("Are you sure to delete?");
    
      if(cnf)
      {
        
        $.post('<?php echo base_url();?>admin/banners/deleteData',{ id : str , mode : 'selected' },
          function(data)
          {
            window.location.href = "<?php echo base_url()?>admin/banners";           

          }
        );
      }
    }
    else
    { 
      alert('You must select atleast one item');
    }
  }

  function changestatus(stat,id)
  {
    
    var checked = jQuery('#stat'+id).is(":checked");

    $.post('<?php echo base_url();?>admin/banners/changeStatus',{ stat : checked , id : id },function(data){

      $('#toast-container').html('<div class="toast toast-success" id="toast-container-body"><button class="toast-close-button" role="button">×</button><div class="toast-title">Success</div><div class="toast-message">Status changed successfully.</div></div>');

    });
  }
  </script>


