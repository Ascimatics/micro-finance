<?php $this->load->view("admin/header");
$page_id=$this->uri->segment(4);
?> 
<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create Client
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/clients"><i class="fa fa-dashboard"></i> Manage Clients</a></li>
        <li class="active">create client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Edit CMS</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form name="changepassword" id="main" class="form-horizontal" action="<?php echo base_url()?>admin/clients/submitData" method="post" novalidate>
              <div class="box-body">

                <div class="form-group">
                  <label for="client_name" class="col-sm-2 control-label">Client Name :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="client_name" id="client_name" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_address" class="col-sm-2 control-label">Work Address :</label>
                  <div class="col-sm-6"> 
                    <textarea name="work_address" class="form-control" id="work_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="residence_address" class="col-sm-2 control-label">Residence Address :</label>
                  <div class="col-sm-6"> 
                    <textarea class="form-control" name="residence_address" id="residence_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="work_telephone" class="col-sm-2 control-label">Telephone(work) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="work_telephone" id="work_telephone" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="mobile1" class="col-sm-2 control-label">Mobile 1 (personal) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="mobile1" id="mobile1" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="mobile2" class="col-sm-2 control-label">Mobile 2 (home) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="mobile2" id="mobile2" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

                <hr>
                <h3 class="box-title">Draw:</h3>
                <div id="file_div">
                <div class="row form-group">
                  <label for="drawee_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-xs-3">
                    <input class="form-control" placeholder="" type="text" id="drawee_name" name="drawee_name[]">
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <label for="drawee_designation" class="col-sm-2 control-label">Designation :</label>
                  <div class="col-xs-3">
                    <input class="form-control" placeholder="" type="text" id="drawee_designation" name="drawee_designation[]">
                    <span class="col-sm-12 messages"></span>
                  </div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="add_file();">Add more</button>
                  </div>
                </div>
                </div>

                <hr>
                <h3 class="box-title">guarantor:</h3>
                <div id="guarantor_div">
                <div>
                <div class="row form-group">
                  <label for="guarantor_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]">
                  </div>
                  <label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="guarantor_designation" name="guarantor_designation[]">
                  </div>
                  <label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]">
                  </div>
                </div><br/>
                <div class="row form-group">
                  <label for="guarantor_address" class="col-sm-2 control-label">Address :</label>
                  <div class="col-xs-2">
                    <textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"></textarea>
                  </div>
                  <label for="tel1" class="col-sm-2 control-label">Tel 1 :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]">
                  </div>
                  <label for="tel2" class="col-sm-2 control-label">Tel 2 :</label>
                  <div class="col-xs-2">
                    <input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]">
                  </div>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-xs-10"></div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default" onclick="add_guarantor();">Add more</button>
                  </div>
                </div><hr><br/>
                <div class="form-group">
                  <label for="account_status" class="col-sm-2 control-label">Account status :</label>
                  <div class="col-sm-2"> 
                    <input type="checkbox" class="make-switch" data-size="small"  id="account_status" name="account_status" onChange="changestatus(this.value)">
                  </div>
                </div><br/>

                <div class="form-group">
                  <label for="loan_number" class="col-sm-2 control-label">Loan No :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="loan_number" id="loan_number" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="loan_amount" class="col-sm-2 control-label">Loan Amount :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="loan_amount" id="loan_amount" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_of_days" class="col-sm-2 control-label">No of Days :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="no_of_days" id="no_of_days" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="interest_rate" class="col-sm-2 control-label">Rate of Interest :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="interest_rate" id="interest_rate" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div><hr>
                <h3 class="box-title">Document Details:</h3>
                <div class="form-group">
                  <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="document_type" name="document_type[]">
                      <option value="Hundi">Hundi</option>
                      <option value="Property papers">Property papers</option>
                      <option value="Agreement">Agreement</option>
                      <option value="Commodity">Commodity</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="purpose" class="col-sm-2 control-label">Purpose :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="purpose" name="purpose[]">
                      <option value="Short term">Short term</option>
                      <option value="Fixed">Fixed</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="row form-group">
                  <label for="doc_received_status" class="col-sm-2 control-label">Doc received status :</label>
                  <div class="col-xs-2">
                    <input type="checkbox" class="make-switch" data-size="small"  id="doc_received_status" name="doc_received_status[]" onChange="changestatus(this.value)">
                  </div>
                  <label for="recived_by" class="col-sm-2 control-label">Received by :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control" name="recived_by[]" id="recived_by" value="" placeholder="">
                  </div>
                </div><br/>
                <div class="row form-group">
                  <label for="validity_status" class="col-sm-2 control-label">Doc Validity status :</label>
                  <div class="col-xs-2">
                    <input type="checkbox" class="make-switch" data-size="small"  id="validity_status" name="validity_status[]" onChange="changestatus(this.value)">
                  </div>
                  <label for="expiry_date" class="col-sm-2 control-label">Date of Expiry :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control pull-right" id="expiry_date" name="expiry_date[]">
                  </div>
                </div><br/>
                <div class="row form-group">
                  <label for="return_status" class="col-sm-2 control-label">Doc Return status :</label>
                  <div class="col-xs-2">
                    <input type="checkbox" class="make-switch" data-size="small"  id="return_status" name="return_status[]" onChange="changestatus(this.value)">
                  </div>
                  <label for="return_date" class="col-sm-2 control-label">Date of Return :</label>
                  <div class="col-xs-3">
                    <input type="text" class="form-control pull-right" id="return_date" name="return_date[]">
                  </div>
                </div> <br/> 
                <div class="row form-group">
                <div class="col-xs-7"></div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-block btn-default">Add more</button>
                  </div>
                </div><hr><br/>
                <div class="form-group">
                  <label for="introduced_by" class="col-sm-2 control-label">Preffered By :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="introduced_by" name="introduced_by">
                      <optgroup label="Clients">
                      <?php foreach ($client_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->first_name." ".$value->last_name;?></option>
                      <?php }?>
                      </optgroup>
                      <optgroup label="Collection man">
                      <?php foreach ($collection_man_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->first_name." ".$value->last_name;?></option>
                      <?php }?>
                      </optgroup>
                      <optgroup label="Brokers">
                      <?php foreach ($broker_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->first_name." ".$value->last_name;?></option>
                      <?php }?>
                      </optgroup>
                      <option value="">Add New</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div> 
                <div class="form-group">
                  <label for="collection_man_id" class="col-sm-2 control-label">Collection Man :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="collection_man_id" name="collection_man_id">
                      <?php foreach ($collection_man_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->first_name." ".$value->last_name;?></option>
                      <?php }?>
                      <option value="">Add New</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="area_id" class="col-sm-2 control-label">Area :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="area_id" name="area_id">
                      <option value="1">Saltlake</option>
                      <option value="2">rajarhat</option>
                      <option value="3">Dumdum</option>
                      <option value="4">Taligange</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="broker_ids" class="col-sm-2 control-label">Broker :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" multiple="multiple" id="broker_ids" name="broker_ids[]">
                      <?php 
                      $temp=1;
                      foreach ($broker_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>" <?php if($temp==1){?> selected<?php }?>><?php echo $value->first_name." ".$value->last_name;?></option>
                      <?php $temp++; }?>
                      <option value="">Add New</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="note" class="col-sm-2 control-label">Note :</label>
                  <div class="col-sm-6"> 
                    <textarea name="note" id="note" rows="5" cols="50" class="form-control"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>
<script type="text/javascript">
  function add_guarantor(){
    $("#guarantor_div").append('<div><div class="row form-group"><label for="guarantor_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]"></div><label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label><div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="guarantor_designation" name="guarantor_designation[]"></div><label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label><div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]"></div></div><br/><div class="row form-group"><label for="guarantor_address" class="col-sm-2 control-label">Address :</label><div class="col-xs-2"><textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"></textarea></div><label for="tel1" class="col-sm-2 control-label">Tel 1 :</label><div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]"></div><label for="tel2" class="col-sm-2 control-label">Tel 2 :</label><div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]"></div></div><div class="row form-group"><div class="col-xs-6"></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_draw_div(this);">Remove</button></div></div></div>');
  }
</script>
<script type="text/javascript">
function add_file()
{
 $("#file_div").append('<div class="row form-group"><label for="drawee_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="drawee_name" name="drawee_name[]"><span class="col-sm-12 messages"></span></div><label for="drawee_designation" class="col-sm-2 control-label">Designation :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="drawee_designation" name="drawee_designation[]"><span class="col-sm-12 messages"></span></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_file(this);">Remove</button></div></div>');
}
function remove_file(ele)
{
 $(ele).parent().parent().remove();
}
function remove_draw_div(ele)
{
 $(ele).parent().parent().parent().remove();
}
</script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
  <script type="text/javascript"> 
  $(function () { 
    $("input[type='checkbox']").change(function () { 
      $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
    }); 
  }); 
  </script>
<!-- CK Editor -->
<!-- <script src="<?php //echo ADMIN_ASSETS_PATH;?>plugins/ckeditor/ckeditor.js"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('cms_description');
    //bootstrap WYSIHTML5 - text editor
    //$(".textarea").wysihtml5();
  });
</script> -->

<script type="text/javascript">
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      // These are the constraints used to validate the form
      var constraints = {
        client_name: {
          presence: true,
          format: {
            pattern: "[a-zA-Z ]*$",
            flags: "i",
            message: "can only contain character"
          }
        },
        work_telephone: {
          presence: true,
          format: {
            pattern: "[0-9]{10}$",
            flags: "i",
            message: "must be a 10 digit number"
          }
        },
        mobile1: {
          presence: true,
          format: {
            pattern: "[0-9]{10}$",
            flags: "i",
            message: "must be a 10 digit number"
          }
        },
        mobile2: {
          presence: true,
          format: {
            pattern: "[0-9]{10}$",
            flags: "i",
            message: "must be a 10 digit number"
          }
        },
        "drawee_name[]":{
          presence: true,
        },
        loan_number: {
          presence: false,
          format: {
            pattern: "[0-9]*$",
            flags: "i",
            message: "can only contain numbers"
          }
        },
        loan_amount: {
          presence: false,
          format: {
            pattern: "[0-9]*$",
            flags: "i",
            message: "can only contain numbers"
          }
        },
        no_of_days: {
          presence: false,
          format: {
            pattern: "[0-9]*$",
            flags: "i",
            message: "can only contain numbers"
          }
        },
        interest_rate: {
          presence: false,
          format: {
            pattern: "[0-9.]*$",
            flags: "i",
            message: "can only contain numbers"
          }
        },
      };

      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#main");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        // validate the form aainst the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
          showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name],textarea[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it 
        $("#main").submit();
      }
    })();
  </script>

