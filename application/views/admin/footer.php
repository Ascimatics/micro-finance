 <footer class="main-footer">
    <!-- <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div> -->
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="<?php echo base_url()?>admin/dashboard"><?php echo PROJECT_NAME;?></a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/select2/select2.full.min.js"></script>
<!-- DataTables -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>dist/js/app.min.js"></script>
<!-- Validate -->
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/validate/underscore-min.js"></script>
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/validate/moment.min.js"></script>
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/validate/validate.js"></script>

<!-- Bootstrap FILE UPLOAD -->
<script type="text/javascript" src="<?php echo ADMIN_ASSETS_PATH?>plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>



<!-- page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    //Date picker
    $('.datepick').datepicker({
      autoclose: true,
      format: 'yyyy-mm-d',
    });
    $('#return_date').datepicker({
      autoclose: true,
      format: 'yyyy-mm-d',
    });
  });
</script>

<script type="text/javascript">
	$("#toast-container").click(function(){
		$("#toast-container-body").hide();
	})
</script>
<script type="text/javascript">
        $(function() {
            jQuery('#check').change(function () {
            var set = jQuery(this).attr("class");
            var checked = jQuery(this).is(":checked");
            jQuery('.'+set).each(function () {
              if (checked) {
                $(this).prop("checked", true);
              } else {
                $(this).prop("checked", false);
              }
            });
            jQuery.uniform.update(set);
          });
        });
</script>
<!-- Switch -->
<script src="<?php echo ADMIN_ASSETS_PATH?>plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo ADMIN_ASSETS_PATH;?>plugins/bootstrap-switch/js/app.min.js"></script>

</body>
</html>