<?php $this->load->view("admin/header");?>

<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        CMS
        <small>Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Manage CMS</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Notification -->
      <div id="toast-container" class="toast-top-right" aria-live="polite" role="alert">
        <?php
        if ($this->session->flashdata('message')!='')
        {
          $message = explode("|",$this->session->flashdata('message'));
            ?>           
              <div class="toast toast-<?php echo $message[0];?>" id="toast-container-body">
                <button class="toast-close-button" role="button">×</button>
                <div class="toast-title"><?php echo $message[0];?></div>
                <div class="toast-message"><?php echo $message[1];?></div>
              </div>
            <?php
        }
          ?>
      </div>     
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Page Title</th>
                  <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($viewData as $key => $val)
                {
                  ?>
                <tr>
                  <td><?php echo $val->cms_pagetitle;?></td>
                <td><a class="btn btn-info btn-success" href="<?php echo base_url()?>admin/cms/editData/<?php echo $val->id;?>">EDIT</a></td>
                </tr>

                <?php
                }
                  ?>
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Page Title</th>
                  <th>Edit</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>
