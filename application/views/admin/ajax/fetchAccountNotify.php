<?php if(empty($document_list)){?>
<div class="callout callout-info">
  <h4></h4>

  <p>The client has not submited any documents.</p>
</div>
<?php }else{
?>

<div class="col-md-12">

          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Document Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px"></th>
                  <th>Document Value</th>
                  <th>Expiry Date</th>
                  <th>Loan Name</th>
                  <th>Available</th>
                  <th>Deficit</th>
                </tr>
                <?php foreach ($document_list as $value) { 
                  $account_details = get_row(TABLE_PREFIX."accounts",array('document_id'=>$value->id));
                  $available_amount = 0;
                  if(!empty($account_details)){ 
                    $available_amount = $value->document_value-($account_details->loan_amount+$loan_amount);
                  }else{
                    $available_amount = $value->document_value-$loan_amount;
                  }
                  ?>
                <tr>
                  <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id;?>" name="document_data[]"></td>
                  <td><?php echo $value->document_value;?></td>
                  <td><?php echo $value->expiry_date;?></td>
                  <td><?php if(empty($account_details)){ echo "No Loan yet.";}else{ echo $account_details->account_name; }?></td>
                  <td><?php if($available_amount>=0){ echo $available_amount;}?></td>
                  <td><?php if($available_amount<0){ echo $available_amount;}else{ echo "0"; }?></td>
                </tr>
                <?php }?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

<?php }?>