<h3 class="box-title">Account Details:</h3>
<div class="form-group">
                  <label for="loan_number" class="col-sm-2 control-label">Loan No :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="loan_number" id="loan_number" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="loan_amount" class="col-sm-2 control-label">Loan Amount :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="loan_amount" id="loan_amount" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="interest_rate" class="col-sm-2 control-label">Rate of Interest :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="interest_rate" id="interest_rate" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_open_date" class="col-sm-2 control-label">Installment Start Date :</label>
                  <div class="col-xs-3">
                     <input type="text" class="form-control datepick" name="account_open_date" id="account_open_date" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_close_date" class="col-sm-2 control-label">Installement close Date :</label>
                  <div class="col-xs-3">
                     <input type="text" class="form-control datepick" name="account_close_date" id="account_close_date" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_of_days" class="col-sm-2 control-label">No of Days :</label>
                  <div class="col-sm-4"> 
                    <input type="text" class="form-control" name="no_of_days" id="no_of_days" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>