<div>
                <div class="form-group">
                  <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="document_type" name="document_type[]">
                    <option value="">Select</option>
                      <option value="Hundi">Hundi</option>
                      <option value="Property papers">Property papers</option>
                      <option value="Agreement">Agreement</option>
                      <option value="Commodity">Commodity</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                <div class="col-xs-1"></div>
                <div class="col-xs-4">
                  <label for="exampleInputFile">Upload Document</label>
                  <input type="file" name="client_document[]">
                  </div>
                </div> 
                <div class="row form-group"><div class="col-xs-8"></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_draw_div(this);">Remove</button></div></div>
                </div>

<script type="text/javascript">
  $('.datepick').datepicker({
      autoclose: true,
      format: 'yyyy-mm-d',
    });
</script>