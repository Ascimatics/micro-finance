<div>
   <div class="row form-group">
      <label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label>
      <div class="col-xs-2">
         <select class="form-control select2" id="drawee_designation" name="guarantor_designation[]">
            <option value="">Select</option>
            <option value="Proprietor">Partner</option>
            <option value="Owner">Shop Owner</option>
            <option value="Director">Company Director</option>
            <option value="Other">Other</option>
         </select>
      </div>
      <label for="guarantor_name" class="col-sm-2 control-label">Name :</label>
      <div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]"></div>
      <label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label>
      <div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]"></div>
   </div>
   <br/>
   <div class="row form-group">
      <label for="guarantor_address" class="col-sm-2 control-label">Address :</label>
      <div class="col-xs-2"><textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"></textarea></div>
      <label for="tel1" class="col-sm-2 control-label">Tel 1 :</label>
      <div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]"></div>
      <label for="tel2" class="col-sm-2 control-label">Tel 2 :</label>
      <div class="col-xs-2"><input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]"></div>
   </div>
   <div class="row form-group">
      <div class="col-xs-6"></div>
      <div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_draw_div(this);">Remove</button></div>
   </div>
</div>
