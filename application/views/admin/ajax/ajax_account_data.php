<table class="table table-bordered">
<tr>
  <th><input type="checkbox" class="checkboxes" data-set="#sample_1 .checkboxes" id="check" /></th>
  <th style="width: 181px;">Client</th>
  <th>document value</th>
  <th>loan amount</th>
  <th>no of days</th>
  <th>Status</th>
  <th>Edit</th>
</tr>
<?php
if(!empty($viewData)){
  foreach($viewData as $key => $val)
  {
  ?>
  <tr>
    <td><input type="checkbox" class="checkboxes"  value="<?php echo $val['id']?>" name="data[]"/></td>
    <td><?php echo stripslashes($val['full_name']);?></td>  
    <td><?php echo stripslashes($val['document_value']);?></td>  
    <td><?php echo stripslashes($val['loan_amount']);?></td> 
    <td><?php echo stripslashes($val['no_of_days']);?></td>       
    <td>
      <input type="checkbox" class="make-switch" data-size="small" <?php echo $val['status'] == 'Yes' ? 'checked' : ''?> id="stat<?php echo $val['id']?>" onChange="changestatus(this.value,'<?php echo $val['id']?>')">
    </td>
    <td>
    <a class="btn btn-info btn-success" href="<?php echo base_url()?>admin/account/editData/<?php echo $val['id'];?>"><i class="fa fa-fw fa-edit"></i></a>
    </td>
  </tr>
  <?php
  }
}else{ ?>
  <tr>
    <td colspan="4">No record found</td>
  </tr>
<?php  
  }
?> 
</table>