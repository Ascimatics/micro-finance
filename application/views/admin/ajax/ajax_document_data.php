<table class="table table-bordered">
  <tr>
    <th><input type="checkbox" class="checkboxes" data-set="#sample_1 .checkboxes" id="check" /></th>
    <th style="width: 181px;">Client</th>
    <th>Designation</th>
    <th>Full Name</th>
    <th>Telephone(work)</th>
    <th>Status</th>
    <th>Edit</th>
  </tr>
  <?php
  if(!empty($viewData)){
    foreach($viewData as $key => $val)
    {
      ?>
    <tr>
      <td><input type="checkbox" class="checkboxes"  value="<?php echo $val['id']?>" name="data[]"/></td>
      <td><?php echo stripslashes($val['full_name']);?></td>
      <td><?php echo stripslashes($val['designation']);?></td>
      <td><?php echo stripslashes($val['full_name']);?></td>
      <td><?php echo stripslashes($val['work_telephone']);?></td>             
      <td>
        <div class="col-xs-8">
          <select class="form-control" onChange="changestatus(this.value,'<?php echo $val['id']?>')">
            <option value="Yes" <?php echo $val['status'] == 'Yes' ? 'selected' : ''?>>ON</option>
            <option value="No" <?php echo $val['status'] == 'No' ? 'selected' : ''?>>OFF</option>
          </select>
        </div>
      </td>
      <td>
      <a class="btn btn-info btn-success" href="<?php echo base_url()?>admin/clients/editData/<?php echo $val['id'];?>"><i class="fa fa-fw fa-edit"></i></a>
      </td>
    </tr>
      <?php
    }
  }else{ 
    ?>
    <tr>
      <td colspan="4">No record found</td>
    </tr>
    <?php  
    }
  ?>
</table>
 <?php echo $this->ajax_pagination->create_links(); ?>
<script type="text/javascript">
$(function() {
  jQuery('#check').change(function () {
  var set = jQuery(this).attr("class");
  var checked = jQuery(this).is(":checked");
  jQuery('.'+set).each(function () {
  if (checked) {
    $(this).prop("checked", true);
  } else {
    $(this).prop("checked", false);
  }
  });
  jQuery.uniform.update(set);
  });
});
</script>