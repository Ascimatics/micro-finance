
                   <h3 class="box-title">Draw:</h3>
<div id="file_div">
 <?php 
                              $count=1;
                              foreach ($drawee_list as $key => $value) { ?>
   <div class="row form-group">
      <label for="drawee_designation" class="col-sm-2 control-label">Designation :</label>
      <div class="col-xs-3">
         <select class="form-control select2" name="drawee_designation[]">
            <option value="">Select</option>
            <?php foreach ($concern_designation as $key => $value1) { ?>
                      <option value="<?php echo $value1->id;?>" <?php if($value1->id==$value->drawee_designation){?> selected<?php }?>><?php echo $value1->designation;?></option>
                      <?php }?>
         </select>
         <span class="col-sm-12 messages"></span>
      </div>
      <label for="drawee_name" class="col-sm-2 control-label">Name :</label>
      <div class="col-xs-3">
         <input class="form-control" placeholder="" type="text" id="drawee_name"  name="drawee_name[]" value="<?php echo $value->drawee_name;?>">
         <span class="col-sm-12 messages"></span>
      </div>

         <?php if($count==1){?>
         <div class="col-xs-2">
           <button type="button" class="btn btn-block btn-default" id="add_more_phone" onclick="add_file();">Add more</button>
         </div>
         <?php }else{?>
         <div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_phone" onclick="remove_file(this);">Remove</button></div>
         <?php }?>

   </div>
   <?php $count++; }?>
</div>
<hr>

<h3 class="box-title">guarantor/accept : </h3>
<div class="form-group">
   <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="same_as_draw" name="same_as_draw" onclick="modify_guarantor()" value="yes" <?php if($guarantor_details->same_as_draw=='Yes'){?> checked <?php }?>> Same as Draw
         </label>
      </div>
   </div>
</div>
<div class="form-group" id="guarantor_type_div" <?php if($guarantor_details->same_as_draw=='Yes'){?> style="display: none;" <?php }?>>
         <label for="guarantor_type"  class="col-sm-2 control-label">Guarantor Type :</label>
         <div class="col-sm-6">
            <select class="form-control select2" id="guarantor_type" name="guarantor_type" onchange="get_firm_details(this.value)">
               <option value="">Select</option>
               <option value="firm" <?php if($guarantor_details->guarantor_type=='firm'){?> selected<?php }?>>Firm</option>
               <option value="personal" <?php if($guarantor_details->guarantor_type=='personal'){?> selected<?php }?>>Personal</option>
            </select>
            <span class="col-sm-12 messages"></span>
         </div>
      </div>
      <div id="firm_details_div" <?php if($guarantor_details->same_as_draw=='Yes'){?> style="display: none;" <?php }?>>
      <div class="form-group">
         <label for="document_value" class="col-sm-2 control-label">Firm title :</label>
         <div class="col-sm-6"> 
            <input type="text" class="form-control" value="<?php echo $guarantor_details->firm_title;?>" name="firm_title" id="firm_title" placeholder="">
            <span class="col-sm-12 messages"></span>
         </div>
      </div>    
      <div class="form-group">
                  <label for="firm_type" class="col-sm-2 control-label">Type of firm:</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="firm_type_id" name="firm_type_id" onchange="add_new_firm(this.value)">
                    <option value="">Select</option>
                    
                      <?php foreach ($firm_types as $key => $value) { ?>
                        <option value="<?php echo $value->id;?>" <?php if($guarantor_details->firm_type_id==$value->id){?> selected <?php }?>><?php echo $value->type;?></option>
                      <?php }?>
                    
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div> 
   </div> 
<div id="guarantor_div" <?php if($guarantor_details->same_as_draw=='Yes'){?> style="display: none;" <?php }?>>
<?php $guarantor_list = get_data(TABLE_PREFIX."guarantor_list",array('guarantor_id'=>$guarantor_details->id)); 
 foreach ($guarantor_list as $key => $value) { ?> 
   <div>
      <div class="row form-group">
         <label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label>
         <div class="col-xs-2">
            <select class="form-control select2" id="guarantor_designation" name="guarantor_designation[]">
               <option value="">Select</option>
               <?php foreach ($concern_designation as $key => $value1) { ?>
                      <option value="<?php echo $value1->id;?>" <?php if($value1->id==$value->guarantor_designation){?> selected <?php }?>><?php echo $value1->designation;?></option>
                      <?php }?>
            </select>
         </div>
         <div>
            <label for="guarantor_name" class="col-sm-2 control-label">Name :</label>
            <div class="col-xs-2">
               <input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]" value="<?php echo $value->guarantor_name;?>">
            </div>
            <label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label>
            <div class="col-xs-2">
               <input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]" value="<?php echo $value->blood_relation;?>">
            </div>
         </div>
      </div>
      <br/>
      <div class="row form-group">
         <label for="guarantor_address" class="col-sm-2 control-label">Address :</label>
         <div class="col-xs-2">
            <textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"><?php echo $value->guarantor_address;?></textarea>
         </div>
         <label for="tel1" class="col-sm-2 control-label">Tel 1 :</label>
         <div class="col-xs-2">
            <input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]" value="<?php echo $value->tel1;?>">
         </div>
         <label for="tel2" class="col-sm-2 control-label">Tel 2 :</label>
         <div class="col-xs-2">
            <input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]" value="<?php echo $value->tel2;?>">
         </div>
      </div>
   </div>
   <?php }?>
</div>
<div class="row" id="div3" <?php if($guarantor_details->same_as_draw=='Yes'){?> style="display: none;" <?php }?>>
   <div class="col-xs-10"></div>
   <div class="col-xs-2">
      <button type="button" class="btn btn-block btn-default" onclick="add_guarantor();">Add more</button>
   </div>
</div>
<h3 class="box-title">Document Details:</h3>
      <div class="form-group">
         <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
         <div class="col-sm-6">
            <select class="form-control select2" id="document_type" name="document_type">
               <option value="">Select</option>
               <option value="Hundi" <?php if($document_details->document_type=='Hundi'){?> selected<?php }?>>Hundi</option>
               <option value="Property papers" <?php if($document_details->document_type=='Property papers'){?> selected<?php }?>>Property papers</option>
               <option value="Agreement" <?php if($document_details->document_type=='Agreement'){?> selected<?php }?>>Agreement</option>
               <option value="Commodity" <?php if($document_details->document_type=='Commodity'){?> selected<?php }?>>Commodity</option>
            </select>
            <span class="col-sm-12 messages"></span>
         </div>
      </div>


<div class="form-group">
   <label for="purpose" class="col-sm-2 control-label">Purpose :</label>
   <div class="col-sm-6">
      <select class="form-control select2" id="purpose" name="purpose">
         <option value="">Select</option>
         <option value="Short term" <?php if($document_details->purpose=='Short term'){?> selected<?php }?>>Short term</option>
         <option value="Fixed" <?php if($document_details->purpose=='Fixed'){?> selected<?php }?>>Fixed</option>
      </select>
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Document value :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control" name="document_value" id="document_value" value="<?php echo $document_details->document_value;?>" placeholder="">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="row form-group">
   <div class="col-sm-offset-1 col-xs-4">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="doc_received_status" name="doc_received_status" value="Yes" <?php if($document_details->doc_received_status=='Yes'){?> checked <?php }?>> : Document received ? (tick for Yes)
         </label>
      </div>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Date of Expiry :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control pull-right datepick" id="expiry_date" name="expiry_date" value="<?php echo $document_details->expiry_date;?>">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
      <div class="form-group">
         <div class="col-xs-1"></div>
         <div class="col-xs-4">
            <label for="exampleInputFile">Upload Document</label>
            <input type="file" name="client_document">
         </div>
      </div>
<div class="row form-group">
   <div class="col-sm-offset-1 col-xs-4">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="cheque_received_status" name="cheque_received_status" value="Yes" <?php echo $document_details->cheque_received_status == 'Yes' ? 'checked' : ''?>> : Cheque received ? (tick for Yes)
         </label>
      </div>
   </div>
</div>
<div id="cheque_div">
<?php 
$count=1;
foreach ($cheque_details as $key => $value) { ?>

   <div class="row form-group">
      <label for="cheque_amount" class="col-sm-2 control-label">Amount :</label>
      <div class="col-xs-3">
         <input type="text" class="form-control"  disabled="true"  value="<?php echo $value->cheque_amount;?>" placeholder="">
         <span class="col-sm-12 messages"></span>
      </div>
<?php if($count==1){?>
         <div class="col-xs-2">
           <button type="button" class="btn btn-block btn-default" id="add_more_phone" onclick="add_cheque();">Add more</button>
         </div>
         <?php }else{?>
         <div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_phone" onclick="remove_cheque(this);">Remove</button></div>
         <?php }?>
   </div>
      
       
   <?php $count++;}?>

</div>


<script type="text/javascript">
   function add_file()
   {
    $("#file_div").append('<div class="row form-group"><label for="drawee_designation" class="col-sm-2 control-label">Designation :</label><div class="col-xs-3"><select class="form-control select2" id="drawee_designation" name="drawee_designation[]"><option value="">Select</option><option value="Proprietor">Proprietor</option><option value="Director">Director</option><option value="Partner">Partner</option></select><span class="col-sm-12 messages"></span></div><label for="drawee_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="drawee_name" name="drawee_name[]"><span class="col-sm-12 messages"></span></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_file(this);">Remove</button></div></div>');
   }
   function remove_file(ele)
   {
    $(ele).parent().parent().remove();
   }
   function remove_draw_div(ele)
   {
    $(ele).parent().parent().parent().remove();
   }
</script>

<script type="text/javascript">
   function modify_guarantor(){ 
     if($('#same_as_draw').is(':checked')){
       $('#guarantor_div').hide();
       $('#guarantor_type_div').hide();
       $('#div3').hide();
     }else{
       
        $.post("<?php echo base_url()?>admin/documents/addGuarantor",{},function(data){
          $("#guarantor_div").html(data);
          $('#guarantor_div').show();
          $('#guarantor_type_div').show();
          $('#div3').show();
        });  

       
     }
   
   }
</script>
<script type="text/javascript">
   function add_guarantor(){
     $.post("<?php echo base_url()?>admin/clients/addMoreGuarantor",{},function(data){
       $("#guarantor_div").append(data);
     });  
   }
</script>

<script type="text/javascript">
   function get_firm_details(str){
      if(str=='firm'){
         $('#firm_details_div').show();
      }else{
         $('#firm_details_div').hide();
      }
   }
</script>
<script type="text/javascript">
   $("#cheque_received_status").on("click", function(){
     check = $("#cheque_received_status").is(":checked");
     
     if(check) {
     
       $.post("<?php echo base_url()?>admin/clients/addCheque",{},function(data){
         $("#cheque_div").append(data);
       }); 
     
     } else {
         $('#cheque_div').html('');
     }
   });
</script>
<script type="text/javascript">
   function add_cheque()
   {
       $.post("<?php echo base_url()?>admin/clients/addMoreCheque",{},function(data){
         $("#cheque_div").append(data);
       });
   }
   function remove_cheque(ele)
   {
   $(ele).parent().parent().remove();
   }
</script>

<script>
   $( function() {
     $('.datepick').datepicker({
      autoclose: true,
      format: 'yyyy-mm-d',
    });
   } );
</script>