<h3 class="box-title">Document Details:</h3>
<div id="document_div">
   <div>
      <div class="form-group">
         <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
         <div class="col-sm-6">
            <select class="form-control select2" id="document_type" name="document_type[]">
               <option value="">Select</option>
               <option value="Hundi">Hundi</option>
               <option value="Property papers">Property papers</option>
               <option value="Agreement">Agreement</option>
               <option value="Commodity">Commodity</option>
            </select>
            <span class="col-sm-12 messages"></span>
         </div>
      </div>
      <div class="form-group">
         <div class="col-xs-1"></div>
         <div class="col-xs-4">
            <label for="exampleInputFile">Upload Document</label>
            <input type="file" name="client_document[]">
         </div>
      </div>
   </div>
</div>
<div class="row form-group">
   <div class="col-xs-8"></div>
   <div class="col-xs-2">
      <button type="button" class="btn btn-block btn-default" onclick="add_document();">Add more</button>
   </div>
</div>
<div class="form-group">
   <label for="purpose" class="col-sm-2 control-label">Purpose :</label>
   <div class="col-sm-6">
      <select class="form-control select2" id="purpose" name="purpose">
         <option value="">Select</option>
         <option value="Short term">Short term</option>
         <option value="Fixed">Fixed</option>
      </select>
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Document value :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control" name="document_value" id="document_value" value="" placeholder="">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Received By :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control" name="received_by" id="received_by" value="" placeholder="">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Date of Expiry :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control pull-right datepick" id="expiry_date" name="expiry_date">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="row form-group">
   <div class="col-sm-offset-1 col-xs-4">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="cheque_received_status" name="cheque_received_status" value="Yes"> : Cheque received ? (tick for Yes)
         </label>
      </div>
   </div>
</div>
<div id="cheque_div"></div>
<hr>
<h3 class="box-title">guarantor/accept : </h3>
<div class="form-group">
   <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="same_as_draw" name="same_as_draw" onclick="modify_guarantor()" value="yes"> Same as Draw
         </label>
      </div>
   </div>
</div>
<div id="guarantor_div">
   <div>
      <div class="row form-group">
         <label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label>
         <div class="col-xs-2">
            <select class="form-control select2" id="guarantor_designation" name="guarantor_designation[]">
               <option value="">Select</option>
               <option value="Proprietor">Partner</option>
               <option value="Owner">Shop Owner</option>
               <option value="Director">Director</option>
               <option value="Other">Other</option>
            </select>
         </div>
         <div>
            <label for="guarantor_name" class="col-sm-2 control-label">Name :</label>
            <div class="col-xs-2">
               <input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]">
            </div>
            <label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label>
            <div class="col-xs-2">
               <input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]">
            </div>
         </div>
      </div>
      <br/>
      <div class="row form-group">
         <label for="guarantor_address" class="col-sm-2 control-label">Address :</label>
         <div class="col-xs-2">
            <textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"></textarea>
         </div>
         <label for="tel1" class="col-sm-2 control-label">Tel 1 :</label>
         <div class="col-xs-2">
            <input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]">
         </div>
         <label for="tel2" class="col-sm-2 control-label">Tel 2 :</label>
         <div class="col-xs-2">
            <input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]">
         </div>
      </div>
   </div>
</div>


<script type="text/javascript">
   $("#cheque_received_status").on("click", function(){
     check = $("#cheque_received_status").is(":checked");
     
     if(check) {
     
       $.post("<?php echo base_url()?>admin/clients/addCheque",{},function(data){
         $("#cheque_div").append(data);
       }); 
     
     } else {
         $('#cheque_div').html('');
     }
   });
</script>
<script type="text/javascript">
   function add_cheque()
   {
       $.post("<?php echo base_url()?>admin/clients/addMoreCheque",{},function(data){
         $("#cheque_div").append(data);
       });
   }
   function remove_cheque(ele)
   {
   $(ele).parent().parent().remove();
   }
</script>
