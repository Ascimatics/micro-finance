 <h3 class="box-title">Broker Details:</h3>
<div class="form-group">
                  <label for="broker_full_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="broker_full_name" id="broker_full_name" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="broker_work_address" class="col-sm-2 control-label">Work Address :</label>
                  <div class="col-sm-6"> 
                    <textarea name="broker_work_address" class="form-control" id="broker_work_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="broker_residence_address" class="col-sm-2 control-label">Residence Address :</label>
                  <div class="col-sm-6"> 
                    <textarea class="form-control" name="broker_residence_address" id="broker_residence_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="broker_work_telephone" class="col-sm-2 control-label">Telephone(work) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="broker_work_telephone" id="broker_work_telephone" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="broker_mobile1" class="col-sm-2 control-label">Mobile 1 (personal) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="broker_mobile1" id="broker_mobile1" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="broker_mobile2" class="col-sm-2 control-label">Mobile 2 (home) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="broker_mobile2" id="broker_mobile2" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>