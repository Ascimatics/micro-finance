                 <div class="form-group">
                  <label for="introduced_by" class="col-sm-2 control-label">User type :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="group_id" name="group_id">
                      <option value="">Select</option>
                      <option value="4">Client</option>
                      <option value="3">Broker</option>
                      <option value="5">Collection Man</option> 
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div> 
<div class="form-group">
                  <label for="broker_full_name" class="col-sm-2 control-label">Name :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="introduce_full_name" id="introduce_full_name" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="introduce_work_address" class="col-sm-2 control-label">Work Address :</label>
                  <div class="col-sm-6"> 
                    <textarea name="introduce_work_address" class="form-control" id="introduce_work_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="introduce_residence_address" class="col-sm-2 control-label">Residence Address :</label>
                  <div class="col-sm-6"> 
                    <textarea class="form-control" name="introduce_residence_address" id="introduce_residence_address" rows="5" cols="50"></textarea>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="introduce_work_telephone" class="col-sm-2 control-label">Telephone(work) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="introduce_work_telephone" id="introduce_work_telephone" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="introduce_mobile1" class="col-sm-2 control-label">Mobile 1 (personal) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="introduce_mobile1" id="introduce_mobile1" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="introduce_mobile2" class="col-sm-2 control-label">Mobile 2 (home) :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="introduce_mobile2" id="introduce_mobile2" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>