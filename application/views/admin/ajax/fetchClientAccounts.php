
<h3 class="box-title">Account Details:</h3>
<div class="form-group">
                  <label for="area_id" class="col-sm-3 control-label">Choose Area :</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="area_id" name="area_id" onchange="add_new_area(this.value)">
                    <option value="">Select</option>
                      <?php foreach ($area_list as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->area_name;?></option>
                      <?php }?>
                      <option value="new">Other</option>
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div id="new_area_div"></div>

                <div class="form-group">
                  <label for="loan_amount" class="col-sm-3 control-label">Loan Amount :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="loan_amount" id="loan_amount" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="interest_rate" class="col-sm-3 control-label">Rate of Interest :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="interest_rate" id="interest_rate" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_open_date" class="col-sm-3 control-label">Installment Start Date :</label>
                  <div class="col-xs-6">
                     <input type="text" class="form-control datepick" name="start_date" id="start_date" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="account_close_date" class="col-sm-3 control-label">Installement end Date :</label>
                  <div class="col-xs-6">
                     <input type="text" class="form-control datepick" name="end_date" id="end_date" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="no_of_days" class="col-sm-3 control-label">Loan Duration :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="account_duration" id="account_duration" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="loan_number" class="col-sm-3 control-label">Installement Amount :</label>
                  <div class="col-sm-6"> 
                    <input type="text" class="form-control" name="installment_amount" id="installment_amount" value="" placeholder="">
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div>

<script type="text/javascript">
   function add_new_account(){
      $.post("<?php echo base_url()?>admin/account/addNewAccount",{},function(data){
         $("#new_account").html(data);
         $('#add_new_button').hide();
      });     
   }
</script>
<script type="text/javascript">
  function add_new_area(val){
    if(val=='new'){
      $('#new_area_div').html('<div class="form-group"><label for="area_name" class="col-sm-2 control-label">Area Name :</label><div class="col-sm-4"> <input type="text" class="form-control" name="area_name" id="area_name" value="" placeholder=""><span class="col-sm-12 messages"></span></div></div>');
    }else{
      $('#new_area_div').html('');
    }
  }
</script>
<script>
   $( function() {
     $( "#accordion" ).accordion();
         //Date picker
    $('.datepick').datepicker({
      autoclose: true,
      format: 'yyyy-mm-d',
    });
   });
</script>
<script type="text/javascript"> 
   $(function () { 
     $("input[type='checkbox']").change(function () { 
       $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
     }); 
   }); 
</script>

