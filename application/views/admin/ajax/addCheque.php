
  <div class="row form-group">
    <label for="cheque_no" class="col-sm-2 control-label">Cheque No :</label>
    <div class="col-xs-3">
       <input type="text" class="form-control" name="cheque_no[]" id="cheque_no" value="" placeholder="">
      <span class="col-sm-12 messages"></span>
    </div>
    <label for="cheque_amount" class="col-sm-2 control-label">Cheque amount :</label>
    <div class="col-xs-3">
       <input type="text" class="form-control" name="cheque_amount[]" id="cheque_amount" value="" placeholder="">
      <span class="col-sm-12 messages"></span>
    </div>
    <div class="col-xs-2">
      <button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="add_cheque();">Add more</button>
    </div>
  </div>
