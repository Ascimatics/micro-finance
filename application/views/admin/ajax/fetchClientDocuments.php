<h3 class="box-title">Draw:</h3>
<div id="file_div">

   <div class="row form-group">
      <label for="drawee_designation" class="col-sm-2 control-label">Designation :</label>
      <div class="col-xs-3">
         <select class="form-control select2" name="drawee_designation[]">
            <option value="">Select</option>
            <?php foreach ($concern_designation as $key => $value1) { ?>
                      <option value="<?php echo $value1->id;?>"><?php echo $value1->designation;?></option>
                      <?php }?>
         </select>
         <span class="col-sm-12 messages"></span>
      </div>
      <label for="drawee_name" class="col-sm-2 control-label">Name :</label>
      <div class="col-xs-3">
         <input class="form-control" placeholder="" type="text" id="drawee_name"  name="drawee_name[]" value="">
         <span class="col-sm-12 messages"></span>
      </div>

         <div class="col-xs-2">
           <button type="button" class="btn btn-block btn-default" id="add_more_phone" onclick="add_file();">Add more</button>
         </div>

   </div>
 
</div>
<hr>

<h3 class="box-title">guarantor/accept : </h3>
<div class="form-group">
   <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="same_as_draw" name="same_as_draw" onclick="modify_guarantor()" value="yes"> Same as Draw
         </label>
      </div>
   </div>
</div>
<div class="form-group" id="guarantor_type_div">
         <label for="guarantor_type"  class="col-sm-2 control-label">Guarantor Type :</label>
         <div class="col-sm-6">
            <select class="form-control select2" id="guarantor_type" name="guarantor_type" onchange="get_firm_details(this.value)">
               <option value="">Select</option>
               <option value="firm">Firm</option>
               <option value="personal">Personal</option>
            </select>
            <span class="col-sm-12 messages"></span>
         </div>
      </div>
      <div id="firm_details_div" style="display: none;">
      <div class="form-group">
         <label for="document_value" class="col-sm-2 control-label">Firm title :</label>
         <div class="col-sm-6"> 
            <input type="text" class="form-control" value="" name="firm_title" id="firm_title" placeholder="">
            <span class="col-sm-12 messages"></span>
         </div>
      </div>    
      <div class="form-group">
                  <label for="firm_type" class="col-sm-2 control-label">Type of firm:</label>
                  <div class="col-sm-6"> 
                    <select class="form-control select2" id="firm_type_id" name="firm_type_id" onchange="add_new_firm(this.value)">
                    <option value="">Select</option>
                    
                      <?php foreach ($firm_types as $key => $value) { ?>
                        <option value="<?php echo $value->id?>"><?php echo $value->type;?></option>
                      <?php }?>
                    
                    </select>
                    <span class="col-sm-12 messages"></span>
                  </div>
                </div> 
   </div> 
<div id="guarantor_div">
   <div>
      <div class="row form-group">
         <label for="guarantor_designation" class="col-sm-2 control-label">Designation :</label>
         <div class="col-xs-2">
            <select class="form-control select2" id="guarantor_designation" name="guarantor_designation[]">
               <option value="">Select</option>
               <?php foreach ($concern_designation as $key => $value1) { ?>
                      <option value="<?php echo $value1->id;?>"><?php echo $value1->designation;?></option>
                      <?php }?>
            </select>
         </div>
         <div>
            <label for="guarantor_name" class="col-sm-2 control-label">Name :</label>
            <div class="col-xs-2">
               <input class="form-control" placeholder="" type="text" id="guarantor_name" name="guarantor_name[]">
            </div>
            <label for="blood_group" class="col-sm-2 control-label">Blood Relation :</label>
            <div class="col-xs-2">
               <input class="form-control" placeholder="" type="text" id="blood_relation" name="blood_relation[]">
            </div>
         </div>
      </div>
      <br/>
      <div class="row form-group">
         <label for="guarantor_address" class="col-sm-2 control-label">Address :</label>
         <div class="col-xs-2">
            <textarea class="form-control" rows="3" cols="20" id="guarantor_address" name="guarantor_address[]"></textarea>
         </div>
         <label for="tel1" class="col-sm-2 control-label">Tel 1 :</label>
         <div class="col-xs-2">
            <input class="form-control" placeholder="" type="text" id="tel1" name="tel1[]">
         </div>
         <label for="tel2" class="col-sm-2 control-label">Tel 2 :</label>
         <div class="col-xs-2">
            <input class="form-control" placeholder="" type="text" id="tel2" name="tel2[]">
         </div>
      </div>
   </div>
</div>
<div class="row" id="div3">
   <div class="col-xs-10"></div>
   <div class="col-xs-2">
      <button type="button" class="btn btn-block btn-default" onclick="add_guarantor();">Add more</button>
   </div>
</div>
<h3 class="box-title">Document Details:</h3>
      <div class="form-group">
         <label for="document_type"  class="col-sm-2 control-label">Document Type :</label>
         <div class="col-sm-6">
            <select class="form-control select2" id="document_type" name="document_type">
               <option value="">Select</option>
               <option value="Hundi">Hundi</option>
               <option value="Property papers">Property papers</option>
               <option value="Agreement">Agreement</option>
               <option value="Commodity">Commodity</option>
            </select>
            <span class="col-sm-12 messages"></span>
         </div>
      </div>


<div class="form-group">
   <label for="purpose" class="col-sm-2 control-label">Purpose :</label>
   <div class="col-sm-6">
      <select class="form-control select2" id="purpose" name="purpose">
         <option value="">Select</option>
         <option value="Short term">Short term</option>
         <option value="Fixed">Fixed</option>
      </select>
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Document value :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control" name="document_value" id="document_value" value="" placeholder="">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
<div class="row form-group">
   <div class="col-sm-offset-1 col-xs-4">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="doc_received_status" name="doc_received_status" value="Yes"> : Document received ? (tick for Yes)
         </label>
      </div>
   </div>
</div>
<div class="form-group">
   <label for="document_value" class="col-sm-2 control-label">Date of Expiry :</label>
   <div class="col-sm-6"> 
      <input type="text" class="form-control pull-right datepick" id="expiry_date" name="expiry_date">
      <span class="col-sm-12 messages"></span>
   </div>
</div>
      <div class="form-group">
         <div class="col-xs-1"></div>
         <div class="col-xs-4">
            <label for="exampleInputFile">Upload Document</label>
            <input type="file" name="client_document">
         </div>
      </div>
<div class="row form-group">
   <div class="col-sm-offset-1 col-xs-4">
      <div class="checkbox">
         <label>
         <input type="checkbox" id="cheque_received_status" name="cheque_received_status" value="Yes"> : Cheque received ? (tick for Yes)
         </label>
      </div>
   </div>
</div>
<div id="cheque_div"></div>

<script type="text/javascript">
   function add_new_document(){
      $.post("<?php echo base_url()?>admin/documents/addNewDocument",{},function(data){
         $("#new_document").html(data);
         $('#add_new_button').hide();
      });     
   }
</script>
<script type="text/javascript">
   function get_firm_details(str){
      if(str=='firm'){
         $('#firm_details_div').show();
      }else{
         $('#firm_details_div').hide();
      }
   }
</script>
<script type="text/javascript">
   $("#cheque_received_status").on("click", function(){
     check = $("#cheque_received_status").is(":checked");
     
     if(check) {
     
       $.post("<?php echo base_url()?>admin/clients/addCheque",{},function(data){
         $("#cheque_div").append(data);
       }); 
     
     } else {
         $('#cheque_div').html('');
     }
   });
</script>
<script type="text/javascript">
   function add_cheque()
   {
       $.post("<?php echo base_url()?>admin/clients/addMoreCheque",{},function(data){
         $("#cheque_div").append(data);
       });
   }
   function remove_cheque(ele)
   {
   $(ele).parent().parent().remove();
   }
</script>

<script type="text/javascript">
   function add_new_area(val){
     if(val=='new'){
       $('#new_area_div').html('<div class="form-group"><label for="area_name" class="col-sm-2 control-label">Area Name :</label><div class="col-sm-6"> <input type="text" class="form-control" name="area_name" id="area_name" value="" placeholder=""><span class="col-sm-12 messages"></span></div></div>');
     }else{
       $('#new_area_div').html('');
     }
   }
</script>
<script type="text/javascript">
   function add_new_broker(){
     var broker_ids = $('#broker_ids').val();
     for (var i = 0; i < broker_ids.length; i++) {
       if (broker_ids[i] == "new") {
         $.post("<?php echo base_url()?>admin/clients/addMoreBroker",{},function(data){
           $("#new_broker_div").html(data);
         }); 
       }else{
         $("#new_broker_div").html('');
       }
     }
   }
</script>
<script type="text/javascript">
   function add_document(){
     $.post("<?php echo base_url()?>admin/clients/addMoreDocument",{},function(data){
       $("#document_div").append(data);
     });
     
   }
</script>
<script type="text/javascript">
   function add_guarantor(){
     $.post("<?php echo base_url()?>admin/clients/addMoreGuarantor",{},function(data){
       $("#guarantor_div").append(data);
     });  
   }
</script>
<script type="text/javascript">
   function add_file()
   {
    $("#file_div").append('<div class="row form-group"><label for="drawee_designation" class="col-sm-2 control-label">Designation :</label><div class="col-xs-3"><select class="form-control select2" id="drawee_designation" name="drawee_designation[]"><option value="">Select</option><option value="Proprietor">Proprietor</option><option value="Director">Director</option><option value="Partner">Partner</option></select><span class="col-sm-12 messages"></span></div><label for="drawee_name" class="col-sm-2 control-label">Name :</label><div class="col-xs-3"><input class="form-control" placeholder="" type="text" id="drawee_name" name="drawee_name[]"><span class="col-sm-12 messages"></span></div><div class="col-xs-2"><button type="button" class="btn btn-block btn-default" id="add_more_draw" onclick="remove_file(this);">Remove</button></div></div>');
   }
   function remove_file(ele)
   {
    $(ele).parent().parent().remove();
   }
   function remove_draw_div(ele)
   {
    $(ele).parent().parent().parent().remove();
   }
</script>
<script type="text/javascript">
   function modify_guarantor(){ 
     if($('#same_as_draw').is(':checked')){
       $('#guarantor_div').hide();
       $('#guarantor_type_div').hide();
       $('#div3').hide();
     }else{
       
        $.post("<?php echo base_url()?>admin/documents/addGuarantor",{},function(data){
          $("#guarantor_div").html(data);
          $('#guarantor_div').show();
          $('#guarantor_type_div').show();
          $('#div3').show();
        });  

       
     }
   
   }
</script>
<script>
   $( function() {
     $( "#accordion" ).accordion();
     $('.datepick').datepicker({
      autoclose: true,
      format: 'yyyy-mm-d',
    });
   } );
</script>
<script type="text/javascript"> 
   $(function () { 
     $("input[type='checkbox']").change(function () { 
       $(this).siblings('ul') .find("input[type='checkbox']") .prop('checked', this.checked); 
     }); 
   }); 
</script>
