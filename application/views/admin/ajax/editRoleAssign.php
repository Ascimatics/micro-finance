<div class="modal-dialog" role="document">
  <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Assign Role</h4>
            </div>
            <form name="assignrole" id="main" class="form-horizontal" action="<?php echo base_url()?>admin/roles/submitAssignRoleData" method="post" enctype="multipart/form-data" novalidate>
            <div class="modal-body">

                  <div class="form-group">
                      <label for="role_name" class="col-sm-2 control-label">User Group</label>
                      <div class="col-sm-10">
                        <select name="group_id" id="group_id" class="form-control" onchange="fetch_user(this.value)" >
                            <option value="">Select Group</option>
                            <?php
                            foreach($user_groups as $key=>$groups)
                            { 
                            ?>
                              <option value="<?php echo $groups->id;?>" <?php if($roleassign->group_id==$groups->id){?> selected<?php }?>><?php echo $groups->name;?></option>
                            <?php
                            }
                            ?>
                          </select>   
                      </div>
                  </div>
                  <div class="form-group">
                    <label for="role_name" class="col-sm-2 control-label">Users</label>
                    <div class="col-sm-10">
                      <select name="user_id" id="user_id" class="form-control">
                        <option value="">Select Users</option>
                        <?php
                        foreach($userlist as $key=>$user)
                        { 
                        ?>
                        <option value="<?php echo $user->id;?>" <?php if($roleassign->user_id==$user->id){?> selected<?php }?>><?php echo $user->first_name." ".$user->last_name;?></option>
                        <?php
                        }
                        ?>
                      </select> 
                    </div>
                  </div>

                  <div class="form-group">
                      <label for="role_name" class="col-sm-2 control-label">Roles</label>
                      <div class="col-sm-10">
                        <select name="role_id" id="role_id" class="form-control">
                            <option value="">Select Role</option>
                            <?php
                            foreach($roles as $key=>$role)
                            { 
                            ?>
                              <option value="<?php echo $role->id;?>" <?php if($roleassign->role_id==$role->id){?> selected<?php }?>><?php echo $role->role_name;?></option>
                            <?php
                            }
                            ?>
                          </select>   
                      </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
  </div>
</div>

<script type="text/javascript">
    (function() {
      // Before using it we must add the parse and format functions
      // Here is a sample implementation using moment.js
      validate.extend(validate.validators.datetime, {
        // The value is guaranteed not to be null or undefined but otherwise it
        // could be anything.
        parse: function(value, options) {
          return +moment.utc(value);
        },
        // Input is a unix timestamp
        format: function(value, options) {
          var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
          return moment.utc(value).format(format);
        }
      });

      // These are the constraints used to validate the form
      var constraints = {
        group_id: {
          presence: true,
        },
        user_id: {
          presence: true,
        },
        role_id: {
          presence: true,
        }
      };

      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#main");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        handleFormSubmit(form);
      });

      // Hook up the inputs to validate on the fly
      var inputs = document.querySelectorAll("input, textarea, select")
      for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function(ev) {
          var errors = validate(form, constraints) || {};
          showErrorsForInput(this, errors[this.name])
        });
      }

      function handleFormSubmit(form, input) {
        // validate the form aainst the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
          showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group")
          // Find where the error messages will be insert into
          , messages = formGroup.querySelector(".messages");
        // First we remove any old messages and resets the classes
        resetFormGroup(formGroup);
        // If we have errors
        if (errors) {
          // we first mark the group has having errors
          formGroup.classList.add("has-error");
          // then we append all the errors
          _.each(errors, function(error) {
            addError(messages, error);
          });
        } else {
          // otherwise we simply mark it as success
          formGroup.classList.add("has-success");
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it 
        $("#main").submit();
      }
    })();
  </script>

  <script type="text/javascript">
    function fetch_user(group_id){
      $.post("<?php echo base_url();?>admin/roles/fetchUserByGroup",{group_id:group_id},function(data){ 
        $("#user_id").html(data);
      });
    }
  </script>
