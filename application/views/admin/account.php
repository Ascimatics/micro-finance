<?php $this->load->view("admin/header");?>

<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Start Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>
      <!-- End Modal -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Account
        <small>Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/account"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Manage account</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Notification -->
      <div id="toast-container" class="toast-top-right" aria-live="polite" role="alert">
        <?php
        if ($this->session->flashdata('message')!='')
        {
          $message = explode("|",$this->session->flashdata('message'));
            ?>           
              <div class="toast toast-<?php echo $message[0];?>" id="toast-container-body">
                <button class="toast-close-button" role="button">×</button>
                <div class="toast-title"><?php echo $message[0];?></div>
                <div class="toast-message"><?php echo $message[1];?></div>
              </div>
            <?php
        }
          ?>
      </div>
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box">
            <div class="box-header">
              <!-- <div class="btn-group" >
                <button type="button" class="btn btn-info">Action</button>
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="javascript:void(0);" onclick="delselected()">Delete</a></li>
                  </ul>
              </div> -->
              <a class="btn btn-info btn-success" style="float:right" href="<?php echo base_url()?>admin/account/addData"><i class="fa fa-fw fa-plus"></i>Add New </a>               
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="dataTables_length" id="example1_length">
                    <label>Show 
                      <select name="example1_length" aria-controls="example1" class="form-control input-sm" onchange="setPerPage(this.value)">
                        <option value="10" <?php if($this->session->userdata('perPage')=='10'){?> selected <?php }?>>10</option>
                        <option value="25" <?php if($this->session->userdata('perPage')=='25'){?> selected <?php }?>>25</option>
                        <option value="50" <?php if($this->session->userdata('perPage')=='50'){?> selected <?php }?>>50</option>
                        <option value="100" <?php if($this->session->userdata('perPage')=='100'){?> selected <?php }?>>100</option>
                      </select> 
                      entries</label>
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input class="form-control input-sm" placeholder="" aria-controls="example1" type="search"></label>
                  </div>
                </div>
              </div>
              <div id="client">
              <table class="table table-bordered">
                <tr>
                  <th><input type="checkbox" class="checkboxes" data-set="#sample_1 .checkboxes" id="check" /></th>
                  <th style="width: 181px;">Client</th>
                  <th>document value</th>
                  <th>loan amount</th>
                  <th>no of days</th>
                  <th>Status</th>
                  <th>Edit</th>
                </tr>
                <?php
                if(!empty($viewData)){
                  foreach($viewData as $key => $val)
                  {
                  ?>
                  <tr>
                    <td><input type="checkbox" class="checkboxes"  value="<?php echo $val['id']?>" name="data[]"/></td>
                    <td><?php echo stripslashes($val['full_name']);?></td>  
                    <td><?php echo stripslashes($val['document_value']);?></td>  
                    <td><?php echo stripslashes($val['loan_amount']);?></td> 
                    <td><?php echo stripslashes($val['account_duration']);?></td>       
                    <td>
                      <input type="checkbox" class="make-switch" data-size="small" <?php echo $val['status'] == 'Yes' ? 'checked' : ''?> id="stat<?php echo $val['id']?>" onChange="changestatus(this.value,'<?php echo $val['id']?>')">
                    </td>
                    <td>
                    <a class="btn btn-info btn-success" href="<?php echo base_url()?>admin/account/editData/<?php echo $val['id'];?>"><i class="fa fa-fw fa-edit"></i></a>
                    </td>
                  </tr>
                  <?php
                  }
                }else{ ?>
                  <tr>
                    <td colspan="4">No record found</td>
                  </tr>
                <?php  
                  }
                ?> 
              </table>
              <?php echo $this->ajax_pagination->create_links(); ?>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>
<script type="text/javascript">
  function setPerPage(perPage){
    $.post('<?php echo base_url();?>admin/area/setPerPage',{ perPage : perPage },
      function(data)
      {
        window.location.href="<?php echo base_url()?>admin/area"
      }
    );
  }
</script>
<script type="text/javascript">
  function deleteone(id)
  { 
    var cnf = confirm("Are you sure to delete?");
    
    if(cnf)
    {
      $('.portlet .tools a.reload').click();
      $.post('<?php echo base_url();?>admin/banners/deleteData',{ id : id,mode : 'single' },
        function(data)
        {
          window.location.href = "<?php echo base_url()?>admin/banners";
        }
      );
    }
  }
  
  function delselected()
  {
    var data = document.getElementsByName('data[]');
    
    var ln = data.length;
    
    var flag = 0;
    var str = "";
    
    for(i=0;i<ln;i++)
    {
      if(data[i].checked)
      {
        str = str + data[i].value + ',';
      }
    }
    
    if(str != "")
    {
      var cnf = confirm("Are you sure to delete?");
    
      if(cnf)
      {
        
        $.post('<?php echo base_url();?>admin/banners/deleteData',{ id : str , mode : 'selected' },
          function(data)
          {
            window.location.href = "<?php echo base_url()?>admin/banners";           

          }
        );
      }
    }
    else
    { 
      alert('You must select atleast one item');
    }
  }

  function changestatus(stat,id)
  {
    
    var checked = jQuery('#stat'+id).is(":checked");

    $.post('<?php echo base_url();?>admin/area/changeStatus',{ stat : checked , id : id },function(data){

      $('#toast-container').html('<div class="toast toast-success" id="toast-container-body"><button class="toast-close-button" role="button">×</button><div class="toast-title">Success</div><div class="toast-message">Status changed successfully.</div></div>');

    });
  }
  </script>



