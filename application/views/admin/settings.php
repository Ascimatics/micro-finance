<?php $this->load->view("admin/header");?>

<!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view("admin/left");?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Notification -->
      <div id="toast-container" class="toast-top-right" aria-live="polite" role="alert">
        <?php
        if ($this->session->flashdata('message')!='')
        {
          $message = explode("|",$this->session->flashdata('message'));
            ?>           
              <div class="toast toast-<?php echo $message[0];?>" id="toast-container-body">
                <button class="toast-close-button" role="button">×</button>
                <div class="toast-title"><?php echo $message[0];?></div>
                <div class="toast-message"><?php echo $message[1];?></div>
              </div>
            <?php
        }
          ?>
      </div>
      <div class="row">

        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Settings</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form name="changepassword" id="main" class="form-horizontal" action="<?php echo base_url();?>admin/tools/updateSettings" method="post">
              <div class="box-body">
                
                <?php
                foreach($viewData as $key => $val)
                {
                  ?>
                <div class="form-group">
                  <label for="old_password" class="col-sm-4 control-label"><?php echo $val->config_title;?></label>

                  <div class="col-sm-5"> 
                    <input type="text" class="form-control" id="<?php echo $val->config_type;?>" name="<?php echo $val->config_type;?>" value="<?php echo $val->config_val;?>">
                    
                  </div>
                  <span class="col-sm-5 messages"></span>
                </div>
                <?php
                }
                  ?>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view("admin/footer");?>





  